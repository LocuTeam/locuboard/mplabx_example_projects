EN / [CZ](README.md)

# Real time counter project examples

Here you can find project examples that use real time counter.


## RTC_CALENDAR_AND_LED_example

Easy project, that uses real time counter for LEDs control. More informations about project you can find [here](RTC_CALENDAR_AND_LED_example).