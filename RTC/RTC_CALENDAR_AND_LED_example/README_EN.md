EN / [CZ](README.md)

# [Extern interrupt projects](/EIC) -> EIC_BUTTON_AND_LED_example

Your are at EIC_BUTTON_AND_LED_example project. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### RTC component settings

First choose operation mode as Clock/Calendar with Alarm, next choose Enable Interrupt and set up RTC prescaler to DIV1024.
<img src="img/rtc_config.png" width="500">


### Pin settings

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">