/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

volatile uint8_t alarm_flag = 0; // Calendar alarm flag

// Calendar alarm callback
void RTC_Callback(RTC_CLOCK_INT_MASK int_cause , uintptr_t  context)
{
    if (int_cause & RTC_CLOCK_INT_MASK_ALARM0)
    {
        struct tm next_alarm_time; // Local variable for set the next calendar alarm
        
        RTC_RTCCTimeGet(&next_alarm_time); // Get actual time for set up next alarm
        
        next_alarm_time.tm_sec = next_alarm_time.tm_sec + 1; // Increment seconds
        if(next_alarm_time.tm_sec >= 60){
            next_alarm_time.tm_sec = 0; // If seconds is over 60 -> set seconds to 0
        }
        
        RTC_RTCCAlarmSet(&next_alarm_time, RTC_ALARM_MASK_SS); // Set next calendar alarm
        
        alarm_flag = 1; // Set alarm flag
    }
}


void LED_Tasks(void); // Create function prototype for LED task


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    
    RTC_RTCCCallbackRegister(RTC_Callback, (uintptr_t) NULL); // Register calendar callback function
    
    
    struct tm sys_time;
    
    sys_time.tm_hour = 12;
    sys_time.tm_sec  = 00;
    sys_time.tm_min  = 00;
    sys_time.tm_mon  = 0;
    sys_time.tm_year = 122;
    sys_time.tm_mday = 1;
    sys_time.tm_wday = 6;
    
    RTC_RTCCTimeSet(&sys_time); // Setup actual system time
    
    
    struct tm alarm_time;
    
    alarm_time.tm_hour = 12;
    alarm_time.tm_sec  = 01;
    alarm_time.tm_min  = 00;
    alarm_time.tm_mon  = 0;
    alarm_time.tm_year = 122;
    alarm_time.tm_mday = 1;
    alarm_time.tm_wday = 6;

    RTC_RTCCAlarmSet(&alarm_time, RTC_ALARM_MASK_SS); // Setup first alarm time
    

    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
        
        LED_Tasks(); // LED task
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


void LED_Tasks(void){
    if(alarm_flag == 1){
        USER_LED1_Toggle(); // If alarm_flag is high toggle the LED
        alarm_flag = 0;
    }
}

/*******************************************************************************
 End of File
*/

