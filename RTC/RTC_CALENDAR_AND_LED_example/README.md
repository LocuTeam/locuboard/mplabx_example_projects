CZ / [EN](README_EN.md)

# [Hodiny reálného času příklady projektů](/RTC) -> RTC_CALENDAR_AND_LED_example

Nacházíš se ve složce projektu RTC_CALENDAR_AND_LED_example. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Obrázek níže popisuje jak je nastavená komponenta system.

<img src="img/system_config.png" width="500">


### Nastavení komponenty RTC

Zaprvé vybereme operation mode jako Clock/Calendar with Alarm, dále vybereme Enable Interrupts a nastavíme RTC prescaler na DIV1024.

<img src="img/rtc_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">