CZ / [EN](README_EN.md)

# Hodiny reálného času příklady projektů

Zde se nacházejí projekty používající hodiny reálného času.


## RTC_CALENDAR_AND_LED_example

Jednoduchý projekt, který využívá hodin reálného času pro ovládání LED diod. Více informací o projektu naleznete [zde](RTC_CALENDAR_AND_LED_example).