/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


#define SLAVE_ADDRESS 0x18 // Default slave address

uint16_t global_i2c_slave_address = SLAVE_ADDRESS; // Global slave address set to default value
uint8_t  global_i2c_data_buffer[2] = {0xF1, 0x5F}; // Create data buffer for sending


volatile uint8_t button1_flag = 0; // button1 flag
volatile uint8_t button2_flag = 0; // button2 flag
volatile uint8_t button3_flag = 0; // button3 flag

// Button 1 callback function
void eic_button1_callback(uintptr_t context)
{
    button1_flag = 1;
}

// Button 2 callback function
void eic_button2_callback(uintptr_t context)
{
    button2_flag = 1;
}

// Button 3 callback function
void eic_button3_callback(uintptr_t context)
{
    button3_flag = 1;
}


volatile uint8_t i2c_callback_flag  = 0; // I2C callback flag
volatile uint8_t i2c_callback_value = 0; // I2C callback value

// I2C callback function
void i2c_callback(uintptr_t contextHandle)
{
    
    i2c_callback_flag = 1;
    
    switch(SERCOM5_I2C_ErrorGet()){
        case SERCOM_I2C_ERROR_NONE:
            i2c_callback_value = 0; // If no error set to 0
        break;
            
        case SERCOM_I2C_ERROR_NAK:
            i2c_callback_value = 1; // If no acknowledge set to 1
        break;
            
        case SERCOM_I2C_ERROR_BUS:
            i2c_callback_value = 2; // If bus error set to 2
        break;
    }
}


void USER_BUTTON1_Tasks(void); // Function prototype for USER_BUTTON1_Tasks function
void USER_BUTTON2_Tasks(void); // Function prototype for USER_BUTTON2_Tasks function
void USER_BUTTON3_Tasks(void); // Function prototype for USER_BUTTON3_Tasks function

void I2C_CALLBACK_MESSAGE_Tasks(void); // Function prototype for I2C_CALLBACK_MESSAGE_Tasks function

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    
    SYSTICK_TimerStart(); // Start system timer for delay function
    
    // Register external interrupt callbacks for button pins
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_callback, (uintptr_t)NULL);
    EIC_CallbackRegister(EIC_PIN_7, eic_button2_callback, (uintptr_t)NULL);
    EIC_CallbackRegister(EIC_PIN_15, eic_button3_callback, (uintptr_t)NULL);
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 9600;              // Baudrate set to 9600 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    
    // May be in future here will be implemented I2C Setup function
    
    SERCOM5_I2C_CallbackRegister(i2c_callback, (uintptr_t)NULL); // Register callback for I2C bus
    
    while ( true )
    {   
       
       USER_BUTTON1_Tasks();
       
       USER_BUTTON2_Tasks();
       
       USER_BUTTON3_Tasks();
       
       I2C_CALLBACK_MESSAGE_Tasks();
        
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}

// Function print Callback state message to stdout
void I2C_CALLBACK_MESSAGE_Tasks(void){
    if(i2c_callback_flag){
        switch(i2c_callback_value){
            case 0:
                printf("I2C_CALLBACK_MESSAGE: Everything is OK.\r\n");
            break;

            case 1:
                printf("I2C_CALLBACK_MESSAGE: No acknowledge received.\r\n");
            break;

            case 2:
                printf("I2C_CALLBACK_MESSAGE: Error on the bus.\r\n");
            break;
        }
        i2c_callback_flag = 0; // Clear i2c flag
    }
}

// Function print sends data through I2C bus if the bus is not busy
void USER_BUTTON1_Tasks(void){
    if(button1_flag){
        
        if(!SERCOM5_I2C_IsBusy()){
            
            printf("USER_BUTTON1: Sending data ");
            for(uint8_t i = 0; i < sizeof(global_i2c_data_buffer); i++){ // print out all data
                printf("0x%02X ", global_i2c_data_buffer[i]);
            }
            printf("to slave address 0x%02X.\r\n", global_i2c_slave_address);
            
            SERCOM5_I2C_Write(global_i2c_slave_address, global_i2c_data_buffer, sizeof(global_i2c_data_buffer));
        }
        else{
            printf("USER_BUTTON1: Wait 'till data is all send.\r\n");
        }
        
        button1_flag = 0; // Clear button flag
    }
}

// Function increment global slave address
void USER_BUTTON2_Tasks(void){
    if(button2_flag){
        if(global_i2c_slave_address < 127){
            global_i2c_slave_address++;
            printf("USER_BUTTON2: Slave address changed to 0x%02X.\r\n", global_i2c_slave_address);
        }
        else printf("USER_BUTTON2: Slave address maximum value achieved.\r\n");
        button2_flag = 0; // Clear button flag
    }
}

// Function decrement global slave address
void USER_BUTTON3_Tasks(void){
    if(button3_flag){
        if(global_i2c_slave_address > 0){
            global_i2c_slave_address--;
            printf("USER_BUTTON3: Slave address changed to 0x%02X.\r\n", global_i2c_slave_address);
        }
        else printf("USER_BUTTON3: Slave address minimum value achieved.\r\n");
        button3_flag = 0; // Clear button flag
    }
}


/*******************************************************************************
 End of File
*/

