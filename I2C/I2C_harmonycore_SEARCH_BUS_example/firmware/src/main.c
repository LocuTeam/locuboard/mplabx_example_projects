/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

DRV_HANDLE i2c_handle;
DRV_I2C_TRANSFER_HANDLE i2c_transfer_handle;

#define WRITE_BUFFER_SIZE 1
#define READ_BUFFER_SIZE 1

uint8_t write_buffer[WRITE_BUFFER_SIZE] = {0x00};
uint8_t read_buffer[READ_BUFFER_SIZE]   = {0x00};

typedef enum{
    I2C_SEARCH_BUS_INIT                   = 0,
    I2C_SEARCH_BUS_WAIT_FOR_BUTTON_PRESS  = 1,
    I2C_SEARCH_BUS_SEND_DATA              = 2,
    I2C_SEARCH_BUS_WAIT_FOR_EVENT_HANDLER = 3
} I2C_SEARCH_BUS_STATES_e;

uint16_t global_i2c_slave_address = 0x00;

I2C_SEARCH_BUS_STATES_e i2c_search_bus_state = I2C_SEARCH_BUS_INIT;


volatile uint8_t button1_flag = 0;

void eic_button1_calback(uintptr_t context){
    button1_flag = 1;
}


volatile uint8_t i2c_transfer_complete_flag = 0;
volatile uint8_t i2c_transfer_error_flag    = 0;

void i2c_transfer_event_handler(DRV_I2C_TRANSFER_EVENT event, DRV_I2C_TRANSFER_HANDLE handle, uintptr_t context){
    
    switch(event){
        case DRV_I2C_TRANSFER_EVENT_COMPLETE:
            i2c_transfer_complete_flag = 1;
        break;

        case DRV_I2C_TRANSFER_EVENT_ERROR:
            i2c_transfer_error_flag = 1;
        break;

        default:
            
        break;
    }
}


void I2C_SEARCH_Tasks(void);


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main(void){   
    
    /* Initialize all modules */
    SYS_Initialize ( NULL );

    
    SYSTICK_TimerStart();
    
    
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_calback, (uintptr_t)NULL);
    
    
    USART_SERIAL_SETUP edbg_usart_setup;
    
    edbg_usart_setup.baudRate  = 9600;
    edbg_usart_setup.dataWidth = USART_DATA_8_BIT;
    edbg_usart_setup.parity    = USART_PARITY_NONE;
    edbg_usart_setup.stopBits  = USART_STOP_1_BIT;
    
    SERCOM4_USART_SerialSetup(&edbg_usart_setup, 0);
    
    
    i2c_handle = DRV_I2C_Open(DRV_I2C_INDEX_0, DRV_IO_INTENT_EXCLUSIVE);
    
    DRV_I2C_TRANSFER_SETUP setup;
    setup.clockSpeed = 100000;

    DRV_I2C_TransferSetup(i2c_handle, &setup);
    
    DRV_I2C_TransferEventHandlerSet(i2c_handle, i2c_transfer_event_handler, (uintptr_t)NULL);
    
    
    printf("LocuBoard is ready.\r\n");
    
    
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );

        I2C_SEARCH_Tasks();

    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


void I2C_SEARCH_Tasks(void){
    
    switch(i2c_search_bus_state){
            
        case I2C_SEARCH_BUS_INIT:
                
            printf("\r\nI2C_SEARCH_BUS_INIT: Push button to start I2C searching.\r\n");
            i2c_search_bus_state = I2C_SEARCH_BUS_WAIT_FOR_BUTTON_PRESS;
                
        break;
            
        case I2C_SEARCH_BUS_WAIT_FOR_BUTTON_PRESS:
            
            if(button1_flag){
                    
                i2c_search_bus_state = I2C_SEARCH_BUS_SEND_DATA;
              
                button1_flag = 0;
            }
                
        break;
            
        case I2C_SEARCH_BUS_SEND_DATA:
            
            DRV_I2C_WriteReadTransferAdd(i2c_handle, global_i2c_slave_address, write_buffer, WRITE_BUFFER_SIZE, read_buffer, READ_BUFFER_SIZE, &i2c_transfer_handle);
            
            i2c_search_bus_state = I2C_SEARCH_BUS_WAIT_FOR_EVENT_HANDLER;
            
        break;
        
        
        case I2C_SEARCH_BUS_WAIT_FOR_EVENT_HANDLER:
        
            if(i2c_transfer_complete_flag || i2c_transfer_error_flag){
                if(i2c_transfer_complete_flag) printf("Slave address located on board is: 0x%02X\r\n", global_i2c_slave_address);
                
                global_i2c_slave_address++;
                
                i2c_search_bus_state = I2C_SEARCH_BUS_SEND_DATA;
                
                if(global_i2c_slave_address > 127){
                    global_i2c_slave_address = 0;
                    i2c_search_bus_state = I2C_SEARCH_BUS_INIT;
                }
                
                if(i2c_transfer_complete_flag) i2c_transfer_complete_flag = 0;
                if(i2c_transfer_error_flag) i2c_transfer_error_flag = 0;
            }
            
        break;
        
            
        default:
            printf("I2C_SEARCH_BUS_ERROR\r\n");
            i2c_search_bus_state = I2C_SEARCH_BUS_INIT;
        break;
    }
    
}


/*******************************************************************************
 End of File
*/


