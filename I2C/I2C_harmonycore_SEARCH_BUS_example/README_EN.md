EN / [CZ](README.md)

# [I2C bus](/I2C) -> I2C_harmonycore_SEARCH_BUS_example

Your are at I2C_harmonycore_SEARCH_BUS_example project. Project source file you can find [here](firmware/src/main.c).


## Project settings

**When the Harmony core usart driver is imported in project the promt windows will appear. In the first one choose Yes as an option for import harmony core, but in the second one choose No as an oprion for not import FREE RTOS**

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### Sercom4 component settings

Here we choose USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 9600 bauds, parity set to No Parity, Character size set to 8 bit width, Stop bit set to One Stop Bit.

The most important part are the Receive Pinout and the Transmit Pinout, these two we set to pin on development board where we want to send our data or where we want to read them. Notice there, that we have no pins to choose here, only pads, so we look for the datasheet or we look for the harmony v3 Pin Table, where the pins show their pads.

<img src="img/sercom4_edbg_usart_config.png" width="500">


### Stdio component settings

Choose option buffered mode.

<img src="img/stdio_config.png" width="500">


### Sercom5 component settings

Choose I2C Master, next set SDA hold TIME to 300-600 ns and I2C TRise time set to 215ns.

The most important part is SDA Pinout and SCL Pinout, these pinouts set to pins due to pads for bus communication. Therefore we need to look for the documentation or look for harmony v3 Pin Table.

<img src="img/sercom5_i2c_config.png" width="500">


### Harmony Core component settings

Let all settings of this component on default value.

<img src="img/harmony_core_config.png" width="500">


### Harmony I2C driver component settings

In Harmony I2C driver setting set Driver mode to Asynchronous.

<img src="img/harmony_usart_driver_config.png" width="500">


### Harmony I2C driver instance 0 component settings

In Harmony I2C driver instance 0  let values as defaults. If you want to set more Clients for sending more data "at the same time" ("at the same time" send and read data). The next you can expand Queue size for sending "big" data that may not fit in I2C registers.

<img src="img/harmony_usart_driver_instance_config.png" width="500">


### Nastavení komponenty EIC

First choose as clock input Clocked by ULP32K. The next choose chanel 6, 7 and 15. These channels correspond to button pins. In every chanel choose option Enable Interrupt, next choose Edge detection is clock asynchronously operated, next choose Falling edge detection and at the end choose Enable filter.

<img src="img/eic_config.png" width="500">


### Pin settings

Pin settings for USART bus communication:

<img src="img/pin_edbg_usart_config.png" width="800">

Pin settings for I2C bus communication:

<img src="img/pin_i2c_config.png" width="800">

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">