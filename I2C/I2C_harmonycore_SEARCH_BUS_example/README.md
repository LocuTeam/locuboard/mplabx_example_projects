CZ / [EN](README_EN.md)

# [I2C sběrnice](/I2C) -> I2C_harmonycore_SEARCH_BUS_example

Nacházíš se ve složce projektu I2C_harmonycore_SEARCH_BUS_example. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

**Při vkládání harmony core driveru vybereme v první dialogovém okně možnost ANO pro importování harmony core, ale v druhém dialogovém okně vybereme možnost NE pro nenaimportování FREE RTOS**

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">


### Nastavení komponenty sercom4 jako EDBG USART

Zde vybereme možnost USART with Internal Clock, dále blocking mód, USART frame with parity, baudrate nastavíme na 9600 bauds, paritu na No Parity, Character size nastavíme na 8 bitovou velikost dat, Stop bit nastavíme na One Stop Bit.

Nejdůležitější částí je Receive Pinout a Transmit Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, a proto se musíme podívat buď do datasheetu, který pad odpovídá jakému pinu a nebo se podívat do harmony v3 Pin Table.

<img src="img/sercom4_edbg_usart_config.png" width="500">


### Nastavení komponenty stdio

Zde vybereme možnost buffered mód.

<img src="img/stdio_config.png" width="500">


### Nastavení komponenty sercom5 jako I2C

Zde vybereme možnost I2C Master, dále nastavíme SDA hold TIME na 300-600 ns and I2C TRise time nastavíme na 215ns.

Nejdůležitější částí je SDA Pinout a SCL Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, a proto se musíme podívat buď do datasheetu, který pad odpovídá jakému pinu a nebo se podívat do harmony v3 Pin Table.

<img src="img/sercom5_i2c_config.png" width="500">


### Nastavení komponenty Harmony Core

Při vložení této komponenty do projektu necháme vše na výchozích hodnotách.

<img src="img/harmony_core_config.png" width="500">


### Nastavení komponenty Harmony I2C driver

V nastavení Harmony I2C driveru nastavíme Driver mód na Asynchronous.

<img src="img/harmony_i2c_driver_config.png" width="500">


### Nastavení komponenty Harmony I2C driver instance 0

V nastavení Harmony I2C driver instance 0 necháme výchozí hodnoty. Pokud bychom chtěli můžeme nastavit více Clients, kdybychom chtěli "najednou" posílat více dat (zároveň posílat a číst). Dále můžeme zvětšit Queue size, kdybychom posílali nějaká velká data a věděli bychom, že by se výstupních I2C registrů nevešla.

<img src="img/harmony_i2c_driver_instance_config.png" width="500">


### Nastavení komponenty EIC

Zaprvé vybereme zdroj hodin Clocked by ULP32K. Vybereme kanál 6, 7 a 15, které odpovídají pinům, kde jsou připojena tlačítka. V každém kanálu vybereme možnost Enable Interrupt, dále vybereme Edge detection is clock asynchronously operated, dále vybereme Falling edge detection a vybereme Enable filter.

<img src="img/eic_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro komunikaci s EDBG programátorem na USART sběrnici:

<img src="img/pin_edbg_usart_config.png" width="800">

Nastavení pinů pro komunikaci na I2C sběrnici:

<img src="img/pin_i2c_config.png" width="800">

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro tlačítka:

<img src="img/pin_button_config.png" width="800">