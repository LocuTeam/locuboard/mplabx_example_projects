/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

#define I2C_BUFFER_SIZE 4


typedef enum{
    I2C_TRANSFER_RX_STATE_GET_REGISTER = 0,
    I2C_TRANSFER_RX_STATE_GET_DATA     = 1        
} I2C_TRANSFER_RX_STATE_e;


typedef struct i2c_registers_descriptor_t{
    uint8_t address;
    uint8_t write_index;
    uint8_t read_index;
    uint8_t buffer[I2C_BUFFER_SIZE];
    I2C_TRANSFER_RX_STATE_e transfer_rx_state;
} i2c_registers_t;

i2c_registers_t i2c_registers;

volatile uint8_t i2c_transfer_completed = 1;

bool i2c_callback(SERCOM_I2C_SLAVE_TRANSFER_EVENT event, uintptr_t contextHandle)
{
    bool isSendACK = true;
    
    switch(event)
    {
        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_ADDR_MATCH:

            // Reset the index. MSB address is sent first followed by LSB.
            i2c_registers.address           = 0;
            i2c_registers.write_index       = 0;
            i2c_registers.transfer_rx_state = I2C_TRANSFER_RX_STATE_GET_REGISTER;

        break;

        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_RX_READY:
            
            switch(i2c_registers.transfer_rx_state){
                
                case I2C_TRANSFER_RX_STATE_GET_REGISTER:
                    i2c_registers.address           = SERCOM5_I2C_ReadByte();
                    i2c_registers.write_index       = i2c_registers.address;
                    i2c_registers.read_index        = i2c_registers.address;
                    i2c_registers.transfer_rx_state = I2C_TRANSFER_RX_STATE_GET_DATA;
                break;
                
                case I2C_TRANSFER_RX_STATE_GET_DATA:
                    i2c_registers.buffer[i2c_registers.write_index++] = SERCOM5_I2C_ReadByte();
                    if (i2c_registers.write_index < (I2C_BUFFER_SIZE - 1)) i2c_registers.write_index++;
                break;
                
            }
            
        break;

        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_TX_READY:

            SERCOM5_I2C_WriteByte(i2c_registers.buffer[i2c_registers.read_index]); // Provide the I2C data requested by the I2C Master
            if (i2c_registers.read_index < (I2C_BUFFER_SIZE - 1)) i2c_registers.read_index++;
            
        break;

        case SERCOM_I2C_SLAVE_TRANSFER_EVENT_STOP_BIT_RECEIVED:

            i2c_transfer_completed = 1;

        break;

        default:
        break;
    }

    return isSendACK;
}

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    
    i2c_registers.buffer[0] = 0xFF;
    i2c_registers.buffer[1] = 0x01;
    i2c_registers.buffer[2] = 0x10;
    i2c_registers.buffer[3] = 0xEA;
    
    SERCOM5_REGS->I2CS.SERCOM_ADDR = SERCOM_I2CS_ADDR_ADDR(0x55UL);
    
    SERCOM5_I2C_CallbackRegister(i2c_callback, (uintptr_t)NULL);
    
    while ( true )
    {
        if(i2c_transfer_completed){
            
            printf("I2C SLAVE TRANSFER COMPLETED\r\n");
            
            printf("Data in I2C register ([register address] register data):\r\n");
            
            for(uint16_t i = 0; i < sizeof(i2c_registers.buffer); i++){
                printf("[%d] 0x%02X", i, i2c_registers.buffer[i]);
                if(i != 0 && i % 10 == 0) printf("\r\n");
                else printf("  ");
            }
            
            printf("\r\n");
            
            i2c_transfer_completed = 0;
        }
        
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

