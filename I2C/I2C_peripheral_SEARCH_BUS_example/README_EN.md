EN / [CZ](README.md)

# [USART section](/USART) -> I2C_peripheral_SEARCH_BUS_example

Your are at I2C_peripheral_SEARCH_BUS_example project, that use I2C bus. Periodic sends data to fictional slave addresses, if the slave address sends data back on the address is located device. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### Sercom4 component settings

Here we choose USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 19200 bauds, parity set to No Parity, Character size set to 8 bit width, Stop bit set to One Stop Bit.

The most important part are the Receive Pinout and the Transmit Pinout, these two we set to pin on development board where we want to send our data or where we want to read them. Notice there, that we have no pins to choose here, only pads, so we look for the datasheet or we look for the harmony v3 Pin Table, where the pins show their pads.

<img src="img/sercom4_edbg_usart_config.png" width="500">


### Stdio component settings

Choose option buffered mode.

<img src="img/stdio_config.png" width="500">


### Nastavení komponenty sercom5 jako I2C

Choose I2C Master, next set SDA hold TIME to 300-600 ns and I2C TRise time set to 215ns.

The most important part is SDA Pinout and SCL Pinout, these pinouts set to pins due to pads for bus communication. Therefore we need to look for the documentation or look for harmony v3 Pin Table.

<img src="img/sercom5_i2c_config.png" width="500">


### Pin settings

Pin settings for USART bus communication:

<img src="img/pin_config1.png" width="800">

Pin settings for I2C bus communication:

<img src="img/pin_i2c_config.png" width="800">

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">
