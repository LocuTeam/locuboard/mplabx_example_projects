/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


uint16_t i2c_slave_adress          = 0; // I2C slave address
uint8_t i2c_reg_adress             = 0; // I2C register address
uint8_t i2c_transmission_completed = 0; // I2C transmission completed flag
uint8_t i2c_transmission_corrupted = 0; // I2C transmission corrupted flag
uint8_t i2c_read_buff[128]         = {}; // I2C read data buffer


// I2C callback function
void i2c_callback(uintptr_t contextHandle){
    
    if(SERCOM5_I2C_ErrorGet() == SERCOM_I2C_ERROR_NONE){
        i2c_transmission_completed = 1; // If transmission is ok set transmission completed flag as 1
    }
    else{
        i2c_transmission_corrupted = 1; // If transmission is no ok set transmission corrupted flag as 1
    }
}


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    
    SYSTICK_TimerStart(); // Start system timer for delay function
    
    
    SERCOM5_I2C_CallbackRegister(i2c_callback, 0); // Setup I2C callback function
    
    SERCOM5_I2C_WriteRead(i2c_slave_adress, &i2c_reg_adress, 1, i2c_read_buff, 1); // Start I2C - Write first data and request read
    
    
    printf("LocuBoard is ready.\r\n"); // Print out message
    
    printf("---> I2C peripheral search bus example <---\r\n");
    printf("\r\n");
    
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
        
        if(i2c_transmission_completed || i2c_transmission_corrupted){
            
            if(i2c_transmission_completed){ 
                printf("Slave address located on board is: 0x%02X\r\n", i2c_slave_adress - 1);
                i2c_transmission_completed = 0; // If transmission was ok set flag to 0
            }
            else if(i2c_transmission_corrupted){
                i2c_transmission_corrupted = 0; // If transmission was corrupted set flag to 0
            }
            
            SERCOM5_I2C_WriteRead(i2c_slave_adress, &i2c_reg_adress, 1, i2c_read_buff, 1); // When the I2C transmission is all done, send next data and request next read
            
            if(i2c_slave_adress >= 127){ // If slave address is bigger or equal to 127 wait 500 milliseconds and set I2C slave address to 0.
                printf("\r\n");

                SYSTICK_DelayMs(500); // Wait 500 milliseconds

                i2c_slave_adress = 0;
            }
            else i2c_slave_adress++; // Else increment slave address
        }
    }
    
    return ( EXIT_FAILURE );
}

/*******************************************************************************
 End of File
*/

