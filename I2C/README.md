CZ / [EN](README_EN.md)

# I2C sběrnice

Je základní sběrnice, která používá ke své komunikaci jeden datový vodič a jeden hodinový signálový vodič + referenční zem. Datový vodič se značí SDA, hodinový se značí SCL a referenční zem se značí tradičně GND.

- SDA = serial data
- SCL = serial clock

<br>

I2C sběrnice umožňuje komunikaci v různých rychlostích módech:

- Standard / Fast  - umožňuje frekvenci hodin SCK až 400kHz
- FastPlus         - umožňuje frekvenci hodin SCK až 1MHz
- HighSpeed        - umožňuje frekvenci hodin SCK až 3,4MHz

<br>

I2C sběrnice může být nakonfigurována do dvou komunikačních módů:

- **master** - zařízení ovládající ostatní zařízení na sběrnici
- **slave**  - zařízení, které poslouchá na sběrnici a v případě potřeby přijímá a odesílá data.

Zařízení nakonfigurované jako master komunikuje se slave zařízeními připojenými na I2C sběrnici pomocí tzv. slave adresy. Každé slave zařízení má svou jedinečnou slave adresu. Na jednotlivých slave adresách zařízení naslouchají a pokud master pošle data na sběrnici s určenou slave adresou, pak pouze toto zařízení začne přijímat nebo odesílat data na základě požadavků masteru.

<br>

Základní zapojení sběrnice I2C vypadá takto:
- Sběnice I2C podporuje také tzv. multimaster, což znamená, že na jedné sběrnici může být zapojeno také více master zařízení najednou. Multimaster zapojení popisuje obrázek napravo.
<img src="img/I2C_chip_block_diagram.png" width="35%">
<img src="img/I2C_multimaster_chip_block_diagram.png" align="right" width="35%">

<br>

Slave adresa může být nakonfigurvána do dvou rozsahů:

- 7bitů  - slave adresy mohou nabývat hodnot od 0 až do 127
- 10bitů - slave adresy mohou nabývat hodnot od 0 až do 1023

<br>

Pro komunikaci je důležité nastavení SDA hold time, které určuje minimální dobu, po kterou jsou data čtena (vzorkována druhou stranou)

Další důležitou konfigurací je nastavení doby nástupné hrany pro SDA a SCK signály.

<img src="img/I2C_SDA_hold_time_graph.png" width="70%">

<br>

Popis rozložení I2C komunikace:

<img src="img/I2C_packet.png" width="70%">

Komunikace se zahájí odesláním tzv. Start bitu (SDA - log 0, SCK - log 1). Pokud zahájí komunikaci master, tak na sběrnici odešle slave adresu zařízení se kterým chce komunikovat a tzv. 
R/W bit. R/W bit určuje směr komunikace ve smyslu čtení nebo zápisu dat.

Pokud byl nastaven R/W bit na odesílání dat, data se začnou odesílat.
Před odesíláním každé sekvence dat čeká master na ACK od slave zařízení, aby věděl, že je slave připravený přijímat data. 
Když master již nemá data pro odeslání, nastaví NACK na sběrnici a tím dá najevo slave zařízení, že všechna data byla již odeslána. 
Poslední krok komunikace je tzv. Stop bit, který ukončí komunikaci mezi master a slave zařízením.

Pokud byl nastaven R/W bit na čtení dat, tak master pošle ACK, uvolní SDA linku, ale hodinový signál generuje dál.
Na konci každé sekvence dat master odešle ACK, čímž dává najevo slavu přípravenost na příjem dalších dat.
Jakmile master přijme požadovaná data, odešle NACK, následně stop bit a ukončí komunikaci.

<img src="img/I2C_packet_and_graph.png" width="100%">

<br>


# Příklady projektů

Jsou zde jednoduché projekty, které pracují s jednoduchou peripheral knihovnou a také projekty se sofistikovanější harmonycore knihovnou.


## I2C_peripheral_SEARCH_BUS_example

**Doporučuje se.**
Jednoduchý projekt využívající peripheral I2C knihovnu. Uživatelský program odesílá data vždy s jinou slave adresou a tím vyhledává zařízení umístěná na I2C sběrnici. Více informací najdete [zde](I2C_peripheral_SEARCH_BUS_example).


## I2C_peripheral_MASTER_example

Jednoduchý projekt využívající peripheral I2C knihovnu. Uživatelský program odesílá data na zvolenou slave adresu v uživatelském programu vždy pokud je zmáčknuto uživatelské tlačítko. Více informací najdete [zde](I2C_peripheral_MASTER_example).


## I2C_peripheral_SLAVE_example

**Doporučuje se.**
Jednoduchý projekt využívající peripheral I2C knihovnu. Uživatelský program čeká na příchozí data, má nastavenou slave adresu, na které poslouchá a pokud master pošle po sběrnici data určená pro tuto slave adresu, uživatelský program data zpracuje a buď data zapíše do paměti anebo přečte uložená data v paměti. Více informací najdete [zde](I2C_peripheral_SLAVE_example).


## I2C_harmonycore_SEARCH_BUS_example

Jednoduchý projekt využívající harmonycore I2C knihovnu. Uživatelský program odesílá data vždy s jinou slave adresou a tím vyhledává zařízení umístěná na I2C sběrnici. Více informací najdete [zde](I2C_harmonycore_SEARCH_BUS_example).