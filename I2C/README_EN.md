EN / [CZ](README.md)

# I2C bus

It is the basic communication bus, that uses for its communication single data wire, single clock signal wire + reference ground. Data wire is called SDA, clock signal wire is called SCL and reference ground is called GND.

- SDA = serial data
- SCL = serial clock

<br>

I2C bus allow to communicate in different speed mode:

- Standard / Fast  - allows to 400kHz clock frequency
- FastPlus         - allows to 1MHz clock frequency
- HighSpeed        - allows to 3,4MHz clock frequency

<br>

I2C bus can be configer to two communication modes:

- **master** - master device controls other devices on bus called slave
- **slave**  - slave device listen on the bus and if it is needed the slave device receive or transmit data.

Device configured as master communicates with slave devices connected to I2C bus using so-called slave address. Each slave device has its own unique slave address. On different slave addresses are devices listening and if master sends data on the bus with specific slave address then only this device starts receiving or transmitting data due to master.

<br>

Basic I2C bus connection looks like that:

- I2C bus also allows so-called multimaster, whitch means that on one bus can be conected more master devices at one time. This describes picture on the right side.

<img src="img/I2C_chip_block_diagram.png" width="35%">
<img src="img/I2C_multimaster_chip_block_diagram.png" align="right" width="35%">

<br>

Slave address can be configured to two ranges:

- 7bits  - slave address value can be set from 0 to 127
- 10bits - slave address value can be set from 0 to 1023

<br>

For comunication is important to configure SDA hold time, that determines minimal time for data is read (sampled by the other party).

The next important configuration is to set SDA and SCK rise time.

<img src="img/I2C_SDA_hold_time_graph.png" width="70%">

<br>

Description of I2C communication layout:

<img src="img/I2C_packet.png" width="70%">

Communication starts sending so-called Start bit (SDA - log 0, SCK - log 1). Master starts the communication by sending slave address device with which it wants to communicate and so-called R/W bit. 
R/W bit determines direction of comunication, that means reading or writing.

If the R/W bit is set for writing data, the master starts sending data.
Before each data sequence is sended, master waits for ACK from slave device for knowing that the slave is ready recieve data.
If master do not have data for sending, it sets NACK on the bus and that tells slave device that all data were already sended. 
The last step of communication is so-called Stop bit that end the communication between master and slave device.

If the R/W bit is set for reading data, the master sends ACK and releases SDA line, but clock signal generation continue.
At the end of each data sequence master sends ACK for letting slave device knowt that master is ready for data receive.
If the master receives required amount of data, then master sends NACK and at the edn master sends so-called Stop bit and end the communication.

<img src="img/I2C_packet_and_graph.png" width="100%">

<br>


# I2C section

There are peripheral projects, that work with basic library and also projects with more complex harmonycore library.

## I2C_peripheral_SEARCH_BUS_example

**Recommended.**
Basic project for peripheral I2C library. The user code sends data each time with another slave address and by this search devices connected to I2C bus. For more information about project click [here](I2C_peripheral_SEARCH_BUS_example).


## I2C_peripheral_MASTER_example

Basic project for peripheral I2C library. The user code sends data to chousen slave address in the user code if the user button is pressed. For more information about project click [here](I2C_peripheral_MASTER_example).


## I2C_peripheral_SLAVE_example

**Recommended.**
Basic project for peripheral I2C library. The user code is waiting for data receiving, it has sets slave address and if master sends data through bus for that slave address, the user program data write to memory or reads out saved data in memory. For more information about project click [here](I2C_peripheral_SLAVE_example).


## I2C_harmonycore_SEARCH_BUS_example

Basic project for peripheral I2C library. The user code sends data each time with another slave address and by this search devices connected to I2C bus. For more information about project click [here](I2C_harmonycore_SEARCH_BUS_example).