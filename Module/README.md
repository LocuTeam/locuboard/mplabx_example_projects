CZ / [EN](README_EN.md)

# Příklady projektů modulů

Zde jsou jednoduché projekty, které implementují sběrnice pro komunikaci s moduly, externí přerušení, ADC periferii anebo se používají periferiní funkce pro ovládání modulů. V projektech se většinou objevuje knihovna daného modulu, která usnadňuje používání. Pro více informací se podívejte do jednotlivých příkladů projektů.


## WS2812B_LED_STRIP_MODULE_SPI_example

V tomto příkladu projektu se používá SPI sběrnice repektive pouze MOSI signál pro ovládání programovatelného LED pásku WS2812B. Uživatelský program používá tlačítka na desce LocuBoard pro vybírání světelného efektu. Více informací najdete [zde](WS2812B_LED_STRIP_MODULE_SPI_example).


## SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example

V tomto příkladu projektu se používají gpio write funkce pro ovládání segmentů a digitů Segmentového LED displaye. Uživatelský program používá jedno tlačítko pro přepínání mezi módy (decimal counter, hexadecimal counter a button counter), dále používá zbávající dvě tlačítka pro přičítání nebo odečítání jedničky od hodnoty, která se zorazuje na display v posledním módu. Více informací najdete [zde](SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example).