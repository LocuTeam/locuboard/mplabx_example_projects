CZ / [EN](README_EN.md)

# WS2812B_LED_STRIP_MODULE_SPI_example

Nacházíš se ve složce projektu WS2812B_LED_STRIP_MODULE_SPI_example. Uživatelský program používá pro komunikaci SPI sběrnici, repektive pouze MOSI signál ve kterém jsou zakódovaná data potřebná pro ws2812b LED pásek. Uživatelský program umožňuje pomocí tlačítek přepínat jednotlivé efekty a zrychlovat nebo zpomalovat efekt. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">


### Nastavení komponenty sercom4 jako EDBG USART

Zde vybereme možnost USART with Internal Clock, dále blocking mód, USART frame with parity, baudrate nastavíme na 19200 bauds, paritu na No Parity, Character size nastavíme na 8 bitovou velikost dat, Stop bit nastavíme na One Stop Bit.

Nejdůležitější částí je Receive Pinout a Transmit Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, což nám teď momentálně nic neříká, a proto je dobré se buď podívat do datasheetu, který pad odpovídá jakému pinu a nebo se podívat do harmony v3 Pin Table.

<img src="img/sercom4_edbg_usart_config.png" width="70%">


### Nastavení komponenty stdio

Zde vybereme možnost buffered mód.

<img src="img/stdio_config.png" width="60%">


### Nastavení komponenty sercom1 jako SPI

Zde vybereme možnost SPI Master, dále nastavíme Clock frequency na 2MHz.  
<img src="img/sercom1_spi_config.png" width="100%">

Pro komunikaci je potřeba data o barvách pro ws2812b zakódovat pomocí následnující tabulky:  
<img src="img/ws2812b_data1.png" width="68%"><img src="img/ws2812b_data2.png" width="30%">


### Nastavení komponenty tc0 jako timer

Zde vybereme 32-bit counter mód, Prescaler na 1024 a mód nastavíme na Timer.  
<img src="img/tc0_config.png" width="70%">


### Nastavení hodin
 
<img src="img/clock_config.png" width="100%">


### Nastavení komponenty EIC
 
<img src="img/eic_config.png" width="80%">


### Nastavení pinů

Nastavení pinů pro komunikaci na USART sběrnici:  
<img src="img/pin_edbg_usart_config.png" width="100%">

Nastavení pinů pro komunikaci na SPI sběrnici:  
<img src="img/pin_spi_config.png" width="100%">

Nastavení pinů pro led diody:  
<img src="img/pin_led_config.png" width="100%">

Nastavení pinů pro tlačítka:  
<img src="img/pin_button_config.png" width="100%">

Nastavení power pinů:  
<img src="img/pin_power_5v_enable_config.png" width="100%">
<img src="img/pin_power_m2_enable_config.png" width="100%">