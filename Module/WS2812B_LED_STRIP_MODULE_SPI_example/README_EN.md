EN / [CZ](README.md)

# WS2812B_LED_STRIP_MODULE_SPI_example

You are now at project folder WS2812B_LED_STRIP_MODULE_SPI_example. User code uses for communication SPI bus, respectively only MOSI signal in witch are coded data needed for ws2812b LED strip. User code uses buttons for switching effects and speeding or slowing effects. User code can be found [here](firmware/src/main.c)


## Project settings

Picture below describes main project layout.  
<img src="img/project_config.png" width="500">


### System component settings

Here allow SysTick and turn on interrupt for using delay function in project.  
<img src="img/system_config.png" width="500">


### sercom4 component settings as EDBG USART

Here choose option USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 115200 bauds, parity to No Parity, Character size set to 8 bit data width, Stop bit set to One Stop Bit.  
<img src="img/sercom4_edbg_usart_config.png" width="70%">


### stdio component settings 

Here choose option buffered mode.  
<img src="img/stdio_config.png" width="60%">


### sercom1 component settings as SPI

Here choose option SPI Master, next set Clock frequency to 2MHz.  
<img src="img/sercom1_spi_config.png" width="100%">

For communication it is important to code data about color value using table below:  
<img src="img/ws2812b_data1.png" width="68%"><img src="img/ws2812b_data2.png" width="30%">


### tc0 component settings as timer

Here chose 32-bit counter mode, Prescaler na 1024 and mode set to Timer.  
<img src="img/tc0_config.png" width="70%">


### Clock settings
 
<img src="img/clock_config.png" width="100%">


### EIC settings
 
<img src="img/eic_config.png" width="80%">


### Pin settings

USART bus pin settings:  
<img src="img/pin_edbg_usart_config.png" width="100%">

SPI bus pin settings:  
<img src="img/pin_spi_config.png" width="100%">

LED pin settings:  
<img src="img/pin_led_config.png" width="100%">

Button pin settings:  
<img src="img/pin_button_config.png" width="100%">

Power pin settings:  
<img src="img/pin_power_5v_enable_config.png" width="100%">
<img src="img/pin_power_m2_enable_config.png" width="100%">