/*
 * ws2812b_spi driver � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ws2812b_spi.c

  @Summary
    SPI driver for WS2812B LED strip.
*/


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "ws2812b_spi.h"

#include <stdint.h>
#include <stdlib.h>


/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */


uint8_t ws2812_init(ws2812b_strip_t *me, uint16_t led_num, ws2812b_spi_write_funcptr_t spi_write_funcptr, uint8_t *spi_write_done_flag){
    
    if(led_num > WS2812_LED_NUMBER_MAX) return 0;
    me->num = led_num;
    
    
    if(spi_write_funcptr == NULL) return 0;
    me->spi_write_funcptr = spi_write_funcptr;
    
    
    me->spi_write_done_flag  = spi_write_done_flag;
    *me->spi_write_done_flag = 0;
    
    
    me->spi_write_state = WS2812B_SPI_WRITE_STATE_IDLE;
    
    
    if(me->leds == NULL) return 0;
    
    for(uint16_t i = 0; i < me->num; i++){
        me->leds[i].green = 0;
        me->leds[i].red   = 0;
        me->leds[i].blue  = 0;
    }
    
    return 1;
}

uint8_t ws2812_set_led_color(ws2812b_strip_t *me, uint16_t led_index, uint8_t green, uint8_t red, uint8_t blue){
    
    if(led_index >= me->num) return 0;
    
    me->leds[led_index].green = green;
    me->leds[led_index].red   = red;
    me->leds[led_index].blue  = blue;
    
    return 1;
}


uint8_t ws2812_set_leds_color(ws2812b_strip_t *me, uint16_t led_index_from, uint16_t led_index_to, uint8_t green, uint8_t red, uint8_t blue){
    
    if((led_index_from > led_index_to) || (led_index_from >= me->num) || (led_index_to >= me->num)) return 0;
    
    for(uint16_t i = led_index_from; i <= led_index_to; i++){
        me->leds[i].green = green;
        me->leds[i].red   = red;
        me->leds[i].blue  = blue;
    }
    
    return 1;
}


void ws2812_set_strip_color(ws2812b_strip_t *me, uint8_t green, uint8_t red, uint8_t blue){
    for(uint16_t i = 0; i < me->num; i++){
        me->leds[i].green = green;
        me->leds[i].red   = red;
        me->leds[i].blue  = blue;
    }
}


WS2812B_SPI_WRITE_STATE_e ws2812_write_data(ws2812b_strip_t *me){
    
    static uint8_t _data[9];
    static uint16_t _led_index;
    
    switch(me->spi_write_state){
        case WS2812B_SPI_WRITE_STATE_IDLE:
            
            _led_index = 0;
            
            me->spi_write_state = WS2812B_SPI_WRITE_STATE_WRITING;
        break;
        
        case WS2812B_SPI_WRITE_STATE_WRITING:
            
            _data[0] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 7) & 1) << 5;  //XXX00000
            _data[0] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 6) & 1) << 2; //000XXX00
            _data[0] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 5) & 1) >> 1; //000000XX

            _data[1] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 5) & 1) << 7;  //X0000000
            _data[1] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 4) & 1) << 4; //0XXX0000
            _data[1] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 3) & 1) << 1; //0000XXX0
            _data[1] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 2) & 1) >> 2; //0000000X

            _data[2] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 2) & 1) << 6;  //XX000000
            _data[2] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].green >> 1) & 1) << 3; //00XXX000
            _data[2] |= WS2812_GET_3BIT_HIGHLOW(me->leds[_led_index].green & 1);             //00000XXX


            _data[3] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 7) & 1) << 5;  //XXX00000
            _data[3] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 6) & 1) << 2; //000XXX00
            _data[3] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 5) & 1) >> 1; //000000XX

            _data[4] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 5) & 1) << 7;  //X0000000
            _data[4] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 4) & 1) << 4; //0XXX0000
            _data[4] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 3) & 1) << 1; //0000XXX0
            _data[4] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 2) & 1) >> 2; //0000000X

            _data[5] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 2) & 1) << 6;  //XX000000
            _data[5] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].red >> 1) & 1) << 3; //00XXX000
            _data[5] |= WS2812_GET_3BIT_HIGHLOW(me->leds[_led_index].red & 1);             //00000XXX


            _data[6] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 7) & 1) << 5;  //XXX00000
            _data[6] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 6) & 1) << 2; //000XXX00
            _data[6] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 5) & 1) >> 1; //000000XX

            _data[7] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 5) & 1) << 7;  //X0000000
            _data[7] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 4) & 1) << 4; //0XXX0000
            _data[7] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 3) & 1) << 1; //0000XXX0
            _data[7] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 2) & 1) >> 2; //0000000X

            _data[8] = WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 2) & 1) << 6;  //XX000000
            _data[8] |= WS2812_GET_3BIT_HIGHLOW((me->leds[_led_index].blue >> 1) & 1) << 3; //00XXX000
            _data[8] |= WS2812_GET_3BIT_HIGHLOW(me->leds[_led_index].blue & 1);             //00000XXX
            
            
            me->spi_write_funcptr(me, _data);
            
            
            me->spi_write_state = WS2812B_SPI_WRITE_STATE_WAITING;
        break;
        
        case WS2812B_SPI_WRITE_STATE_WAITING:
            
            if((*me->spi_write_done_flag) == 1){
                if(_led_index < me->num){
                    _led_index++;
                    me->spi_write_state = WS2812B_SPI_WRITE_STATE_WRITING;
                }
                else{
                    me->spi_write_state = WS2812B_SPI_WRITE_STATE_IDLE;
                }
                
                *me->spi_write_done_flag = 0;
            }
            
        break;
        
        default:
            // Nothing
        break;
    }

    return me->spi_write_state;
}


uint8_t ws2812_write_data_is_busy(ws2812b_strip_t *me){
    if(me->spi_write_state != WS2812B_SPI_WRITE_STATE_IDLE) return 1;
    else return 0;
}

uint8_t ws2812_write_data_is_idle(ws2812b_strip_t *me){
    if(me->spi_write_state == WS2812B_SPI_WRITE_STATE_IDLE) return 1;
    else return 0;
}


void ws2812_write_data_polled(ws2812b_strip_t *me){
    do{
        ws2812_write_data(me);
    } while(ws2812_write_data_is_busy(me)); 
}
