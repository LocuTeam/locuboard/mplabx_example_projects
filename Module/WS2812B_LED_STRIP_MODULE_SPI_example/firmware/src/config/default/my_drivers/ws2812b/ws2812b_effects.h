/*
 * ws2812b_effects library � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ws2812b_effects.h

  @Summary
    WS2812B LED strip effects library.
*/


#ifndef _WS2812B_EFFECTS_H    /* Guard against multiple inclusion */
#define _WS2812B_EFFECTS_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "ws2812b_spi.h"

#include <stdint.h>


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    
    #define WS2812B_INCREMENT_CNT_DEFAULT 0
    #define WS2812B_INCREMENT_VALUE_DEFAULT 0
    #define WS2812B_INCREMENT_SIGN_DEFAULT 1
    
    

    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    
    typedef struct ws2812b_effects_descriptor_t ws2812b_effects_t;
    typedef uint16_t (*ws2812b_random_funcptr_t)(uint16_t num);
    
    
    typedef enum{
        WS2812B_FADEIN_FADEOUT_STATE_INIT      = 0,
        WS2812B_FADEIN_FADEOUT_STATE_GREE_UP   = 1,
        WS2812B_FADEIN_FADEOUT_STATE_GREE_DOWN = 2,
        WS2812B_FADEIN_FADEOUT_STATE_RED_UP    = 3,
        WS2812B_FADEIN_FADEOUT_STATE_RED_DOWN  = 4,
        WS2812B_FADEIN_FADEOUT_STATE_BLUE_UP   = 5,
        WS2812B_FADEIN_FADEOUT_STATE_BLUE_DOWN = 6
    } WS2812B_FADEIN_FADEOUT_STATE_e;
    
    typedef enum{
        WS2812B_RAINBOW_CYCLE_STATE_INIT    = 0,
        WS2812B_RAINBOW_CYCLE_STATE_WRITING = 1,
        WS2812B_RAINBOW_CYCLE_STATE_IDLE    = 2
    } WS2812_RAINBOW_CYCLE_STATE_e;
    
    typedef enum{
        WS2812B_COLOR_WIPE_STATE_INIT    = 0,
        WS2812B_COLOR_WIPE_STATE_WRITING = 1,
        WS2812B_COLOR_WIPE_STATE_IDLE    = 2
    } WS2812B_COLOR_WIPE_STATE_e;
    
    typedef enum{
        WS2812B_SPARKLE_STATE_INIT          = 0,
        WS2812B_SPARKLE_STATE_TURNON        = 1,
        WS2812B_SPARKLE_STATE_WAIT          = 2,
        WS2812B_SPARKLE_STATE_TURNOFF       = 3
    } WS2812B_SPARKLE_STATE_e;
    
    
    struct ws2812b_effects_descriptor_t{
        WS2812B_FADEIN_FADEOUT_STATE_e fadein_fadeout_state;
        WS2812_RAINBOW_CYCLE_STATE_e rainbow_cycle_state;
        WS2812B_COLOR_WIPE_STATE_e color_wipe_state;
        WS2812B_SPARKLE_STATE_e sparkle_state;
        
        uint8_t green_inc;
        uint8_t red_inc;
        uint8_t blue_inc;
        
        uint16_t cnt_i;
        int8_t sign_inc;
    };
    

    // *****************************************************************************
    // Section: Function Prototypes
    // *****************************************************************************
    
    WS2812B_FADEIN_FADEOUT_STATE_e WS2812B_FADEIN_FADEOUT_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects);
    
    WS2812_RAINBOW_CYCLE_STATE_e WS2812B_RAINBOW_CYCLE_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects);
    
    WS2812B_COLOR_WIPE_STATE_e WS2812B_COLOR_WIPE_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects, uint8_t red, uint8_t green, uint8_t blue);
    
    WS2812B_SPARKLE_STATE_e WS2812B_SPARKLE_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects, ws2812b_random_funcptr_t random_funcptr, uint8_t red, uint8_t green, uint8_t blue);
    
    void ws2812b_clear_effects(ws2812b_effects_t *effects);
    

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
