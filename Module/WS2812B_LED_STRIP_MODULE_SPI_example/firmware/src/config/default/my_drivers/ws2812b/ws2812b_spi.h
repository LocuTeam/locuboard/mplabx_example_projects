/*
 * ws2812b_spi driver � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ws2812b_spi.h

  @Summary
    SPI driver for WS2812B LED strip.
*/


#ifndef _WS2812B_SPI_H    /* Guard against multiple inclusion */
#define _WS2812B_SPI_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    
    #define WS2812_LED_NUMBER_MAX 300
    #define WS2812_GET_3BIT_HIGHLOW(X) ((X) ? (0b001) : (0b011))
    
    

    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    
    typedef struct ws2812b_led_descriptor_t ws2812b_led_t;
    typedef struct ws2812b_strip_descriptor_t ws2812b_strip_t;
    typedef void (*ws2812b_spi_write_funcptr_t)(ws2812b_strip_t *me, uint8_t *data);
    
    
    typedef enum{
        WS2812B_SPI_WRITE_STATE_IDLE    = 0,
        WS2812B_SPI_WRITE_STATE_WRITING = 1,
        WS2812B_SPI_WRITE_STATE_WAITING = 2
    }WS2812B_SPI_WRITE_STATE_e;
    
    
    struct ws2812b_led_descriptor_t{
        uint8_t green;
        uint8_t red;
        uint8_t blue;
    };
    
    struct ws2812b_strip_descriptor_t{
        ws2812b_led_t leds[WS2812_LED_NUMBER_MAX];
        ws2812b_spi_write_funcptr_t spi_write_funcptr;
        WS2812B_SPI_WRITE_STATE_e spi_write_state;
        uint8_t *spi_write_done_flag;
        uint16_t num;
    };
    
    

    // *****************************************************************************
    // Section: Function Prototypes
    // *****************************************************************************
    
    uint8_t ws2812_init(ws2812b_strip_t *me, uint16_t led_num, ws2812b_spi_write_funcptr_t spi_write_funcptr, uint8_t *spi_write_done_flag);
    
    uint8_t ws2812_set_led_color(ws2812b_strip_t *me, uint16_t led_index, uint8_t green, uint8_t red, uint8_t blue);
    
    uint8_t ws2812_set_leds_color(ws2812b_strip_t *me, uint16_t led_index_from, uint16_t led_index_to, uint8_t green, uint8_t red, uint8_t blue);
    
    void ws2812_set_strip_color(ws2812b_strip_t *me, uint8_t green, uint8_t red, uint8_t blue);
    
    WS2812B_SPI_WRITE_STATE_e ws2812_write_data(ws2812b_strip_t *me);
    
    void ws2812_write_data_polled(ws2812b_strip_t *me);
    
    uint8_t ws2812_write_data_is_busy(ws2812b_strip_t *me);
    
    uint8_t ws2812_write_data_is_idle(ws2812b_strip_t *me);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
