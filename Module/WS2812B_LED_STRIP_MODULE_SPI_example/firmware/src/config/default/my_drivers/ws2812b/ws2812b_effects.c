/*
 * ws2812b_effects library � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ws2812b_effects.c

  @Summary
    WS2812B LED strip effects library.
*/


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "ws2812b_effects.h"

#include "ws2812b_spi.h"

#include <stdint.h>
#include <stdlib.h>


/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */


void ws2812b_clear_effects(ws2812b_effects_t *effects){
    effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_INIT;
    effects->rainbow_cycle_state  = WS2812B_RAINBOW_CYCLE_STATE_INIT;
    effects->color_wipe_state     = WS2812B_COLOR_WIPE_STATE_INIT;
    effects->sparkle_state        = WS2812B_SPARKLE_STATE_INIT;
}


WS2812B_FADEIN_FADEOUT_STATE_e WS2812B_FADEIN_FADEOUT_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects){
    
    switch(effects->fadein_fadeout_state){
        
        case WS2812B_FADEIN_FADEOUT_STATE_INIT:
            
            effects->green_inc = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->red_inc   = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->blue_inc  = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->sign_inc  = WS2812B_INCREMENT_SIGN_DEFAULT;
            
            effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_GREE_UP;
        break;
        
        case WS2812B_FADEIN_FADEOUT_STATE_GREE_UP:
            if(effects->green_inc >= 255){
                effects->sign_inc = -1;

                effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_GREE_DOWN;
            }
            else effects->green_inc += effects->sign_inc;
        break;
            
        case WS2812B_FADEIN_FADEOUT_STATE_GREE_DOWN:
            if(effects->green_inc <= 0){
                effects->sign_inc = 1;

                effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_RED_UP;
            }
            else effects->green_inc += effects->sign_inc;
        break;
            
        case WS2812B_FADEIN_FADEOUT_STATE_RED_UP:
            if(effects->red_inc >= 255){
                effects->sign_inc = -1;

                effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_RED_DOWN;
            }
            else effects->red_inc += effects->sign_inc;
        break;
            
        case WS2812B_FADEIN_FADEOUT_STATE_RED_DOWN:
            if(effects->red_inc <= 0){
                effects->sign_inc = 1;

                effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_BLUE_UP;
            }
            else effects->red_inc += effects->sign_inc;
        break;
            
        case WS2812B_FADEIN_FADEOUT_STATE_BLUE_UP:
            if(effects->blue_inc >= 255){
                effects->sign_inc = -1;

                effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_BLUE_DOWN;
            }
            else effects->blue_inc += effects->sign_inc;
        break;
            
        case WS2812B_FADEIN_FADEOUT_STATE_BLUE_DOWN:
            if(effects->blue_inc <= 0){
                effects->sign_inc = 1;

                effects->fadein_fadeout_state = WS2812B_FADEIN_FADEOUT_STATE_GREE_UP;
            }
            else effects->blue_inc += effects->sign_inc;
        break;
            
        default:
            // Nothing
        break;
    }
    
    ws2812_set_strip_color(me, effects->green_inc, effects->red_inc, effects->blue_inc);
    
    ws2812_write_data_polled(me);
    
    return effects->fadein_fadeout_state;
}



void _ws2812b_rainbow_cycle_wheel(uint8_t wheel_position, uint8_t *green, uint8_t *red, uint8_t *blue){
    
    if(wheel_position < 85) {
        *green = wheel_position * 3;
        *red   = 255 - wheel_position * 3;
        *blue  = 0;
    } 
    else if(wheel_position < 170) {
        wheel_position -= 85;
        *green = 255 - wheel_position * 3;
        *red   = 0;
        *blue  = wheel_position * 3;
    }
    else{
        wheel_position -= 170;
        *green = 0;
        *red   = wheel_position * 3;
        *blue  = 255 - wheel_position * 3;
    }
}

WS2812_RAINBOW_CYCLE_STATE_e WS2812B_RAINBOW_CYCLE_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects){
    
    switch(effects->rainbow_cycle_state){
        
        case WS2812B_RAINBOW_CYCLE_STATE_INIT:
            effects->green_inc = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->red_inc   = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->blue_inc  = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->cnt_i     = WS2812B_INCREMENT_CNT_DEFAULT;
            
            effects->rainbow_cycle_state = WS2812B_RAINBOW_CYCLE_STATE_WRITING;
        break;
        
        case WS2812B_RAINBOW_CYCLE_STATE_WRITING:
            
            if(effects->cnt_i > 256*5){
                effects->cnt_i = 0;
            }
            
            for(uint16_t j = 0; j < me->num; j++) {
                _ws2812b_rainbow_cycle_wheel(((j * 256 / me->num) + effects->cnt_i) & 255, &effects->green_inc, &effects->red_inc, &effects->blue_inc);
                ws2812_set_led_color(me, j, effects->green_inc, effects->red_inc, effects->blue_inc);
            }
            
            ws2812_write_data_polled(me); // 256*5 cycles for all 5 colors on wheel
            
            effects->cnt_i++;
            
            effects->rainbow_cycle_state = WS2812B_RAINBOW_CYCLE_STATE_IDLE;
        break;
        
        case WS2812B_RAINBOW_CYCLE_STATE_IDLE:
            
            // IDLE state
            
            effects->rainbow_cycle_state = WS2812B_RAINBOW_CYCLE_STATE_WRITING;
        break;
        
        default:
            // Nothing
        break;
    }
    
    return effects->rainbow_cycle_state;
}


WS2812B_COLOR_WIPE_STATE_e WS2812B_COLOR_WIPE_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects, uint8_t red, uint8_t green, uint8_t blue){
    
    switch(effects->color_wipe_state){
        
        case WS2812B_COLOR_WIPE_STATE_INIT:

            effects->cnt_i = WS2812B_INCREMENT_CNT_DEFAULT;
            
            effects->color_wipe_state = WS2812B_COLOR_WIPE_STATE_WRITING;
        break;
        
        case WS2812B_COLOR_WIPE_STATE_WRITING:
            
            if(effects->cnt_i >= me->num){
                effects->color_wipe_state = WS2812B_COLOR_WIPE_STATE_IDLE;
                effects->cnt_i = 0;
                break;
            }
            
            ws2812_set_led_color(me, effects->cnt_i, red, green, blue);
            ws2812_write_data_polled(me);
            
            effects->cnt_i++;
            
        break;
        
        case WS2812B_COLOR_WIPE_STATE_IDLE:
            
            // IDLE state
            
            effects->color_wipe_state = WS2812B_COLOR_WIPE_STATE_WRITING;
        break;

        default:
            // Nothing
        break;
    }
    
    return effects->color_wipe_state;
}


WS2812B_SPARKLE_STATE_e WS2812B_SPARKLE_Tasks(ws2812b_strip_t *me, ws2812b_effects_t *effects, ws2812b_random_funcptr_t random_funcptr, uint8_t red, uint8_t green, uint8_t blue){
    static uint16_t led_index;
    
    switch(effects->sparkle_state){
        
        case WS2812B_SPARKLE_STATE_INIT:
            effects->green_inc = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->red_inc   = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->blue_inc  = WS2812B_INCREMENT_VALUE_DEFAULT;
            effects->cnt_i     = WS2812B_INCREMENT_CNT_DEFAULT;
            
            effects->sparkle_state = WS2812B_SPARKLE_STATE_TURNON;
        break;
        
        case WS2812B_SPARKLE_STATE_TURNON:
            led_index = random_funcptr(me->num);
            ws2812_set_led_color(me, led_index, red, green, blue);
            ws2812_write_data_polled(me);
            
            effects->sparkle_state = WS2812B_SPARKLE_STATE_WAIT;
        break;
        
        case WS2812B_SPARKLE_STATE_WAIT:
            
            // Wait state
            
            effects->sparkle_state = WS2812B_SPARKLE_STATE_TURNOFF;
        break;
        
        case WS2812B_SPARKLE_STATE_TURNOFF:
            ws2812_set_led_color(me, led_index, 0, 0, 0);
            
            effects->sparkle_state = WS2812B_SPARKLE_STATE_TURNON;
        break;
        
        default:
            // Nothing
        break;
    }
    
    return effects->sparkle_state;
}
