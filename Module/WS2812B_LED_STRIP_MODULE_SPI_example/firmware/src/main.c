/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <math.h>

#include "config/default/my_drivers/ws2812b/ws2812b_spi.h"
#include "config/default/my_drivers/ws2812b/ws2812b_effects.h"


#define WS2812_LED_NUMBER 30


ws2812b_strip_t ws2812b;

ws2812b_effects_t ws2812b_effects;


typedef enum{
    USER_PROGRAM_STATE_INIT                         = 0,
    USER_PROGRAM_STATE_WS2812_FADEIN_FADEOUT_EFFECT = 1,
    USER_PROGRAM_STATE_WS2812_RAINBOW_CYCLE_EFFECT  = 2,
    USER_PROGRAM_STATE_WS2812_COLOR_WIPE_EFFECT     = 3,
    USER_PROGRAM_STATE_SPARKLE_WIPE_EFFECT          = 4,
    USER_PROGRAM_STATE_IDLE                         = 5
} USER_PROGRAM_STATE_e;

USER_PROGRAM_STATE_e user_program_state = USER_PROGRAM_STATE_INIT;


volatile uint8_t eic_button1_callback_flag = 0;
volatile uint8_t eic_button2_callback_flag = 0;
volatile uint8_t eic_button3_callback_flag = 0;

void eic_button1_callback(uintptr_t context){
    eic_button1_callback_flag = 1;
}

void eic_button2_callback(uintptr_t context){
    eic_button2_callback_flag = 1;
}

void eic_button3_callback(uintptr_t context){
    eic_button3_callback_flag = 1;
}


//void ws2812b_spi_write(ws2812b_strip_t *me, uint8_t *data){
//    SERCOM1_SPI_Write(data, 9);
//    while(SERCOM1_SPI_IsBusy());
//    *me->spi_write_done_flag = 1;
//}

void spi_write_callback(uintptr_t context){
    uint8_t *_pointer = (uint8_t *) context;
    *_pointer = 1;
}

void ws2812b_spi_write(ws2812b_strip_t *me, uint8_t *data){
    SERCOM1_SPI_Write(data, 9);
}



volatile uint8_t timer_flag = 0;

void timer_callback(TC_TIMER_STATUS status, uintptr_t context){
    timer_flag = 1;
}



uint16_t ws2812b_random(uint16_t num){
    return ((uint16_t)(random() % num));
}


uint8_t USER_PROGRAM_Tasks(void);


int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    
    TC0_TimerCallbackRegister(timer_callback, (uintptr_t) NULL);
    TC0_TimerStart();
    
    
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_callback, (uintptr_t) NULL);
    EIC_CallbackRegister(EIC_PIN_7, eic_button2_callback, (uintptr_t) NULL);
    EIC_CallbackRegister(EIC_PIN_15, eic_button3_callback, (uintptr_t) NULL);
    
    
    SPI_TRANSFER_SETUP spi_config;
    spi_config.clockFrequency = 2400000;
    spi_config.clockPhase     = SPI_CLOCK_PHASE_LEADING_EDGE;
    spi_config.clockPolarity  = SPI_CLOCK_POLARITY_IDLE_LOW;
    spi_config.dataBits       = SPI_DATA_BITS_8;
    
    SERCOM1_SPI_TransferSetup(&spi_config, 0);
    
    uint8_t spi_write_flag = 0;
    
    SERCOM1_SPI_CallbackRegister(spi_write_callback, (uintptr_t) &spi_write_flag);
    
    ws2812_init(&ws2812b, WS2812_LED_NUMBER, ws2812b_spi_write, &spi_write_flag); // Init ws2812 led strip
    
    
    printf("LocuBoard is ready.\r\n");
    
    
    while ( true ){
        
        SYS_Tasks(); // Maintain state machines of all polled MPLAB Harmony modules.
        
        if(eic_button2_callback_flag){
            uint32_t _period = TC0_Timer32bitPeriodGet();
            
            if(_period < 1876){ // 100ms MAX
                TC0_TimerStop();
                TC0_Timer32bitCounterSet(0);
                TC0_Timer32bitPeriodSet(_period + 46);
                TC0_TimerStart();
            }
            
            eic_button2_callback_flag = 0;
        }
        
        if(eic_button3_callback_flag){
            uint32_t _period = TC0_Timer32bitPeriodGet();
            
            if(_period > 46){ // 1ms MIN
                TC0_TimerStop();
                TC0_Timer32bitCounterSet(0);
                TC0_Timer32bitPeriodSet(_period - 46);
                TC0_TimerStart();
            }
            
            eic_button3_callback_flag = 0;
        }
        
        if(timer_flag){

            uint8_t _wait = USER_PROGRAM_Tasks();
            if(_wait) timer_flag = 0;
        }
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


uint8_t USER_PROGRAM_Tasks(void){
    
    uint8_t _wait = 0;
    
    switch(user_program_state){
            
        case USER_PROGRAM_STATE_INIT:
            
            ws2812b_clear_effects(&ws2812b_effects);
            ws2812_set_strip_color(&ws2812b, 0, 0, 0);
            ws2812_write_data_polled(&ws2812b);
            
            user_program_state = USER_PROGRAM_STATE_WS2812_FADEIN_FADEOUT_EFFECT;
        break;
            
        case USER_PROGRAM_STATE_WS2812_FADEIN_FADEOUT_EFFECT:
                
            WS2812B_FADEIN_FADEOUT_Tasks(&ws2812b, &ws2812b_effects);
            
            _wait = 1;
            
            if(eic_button1_callback_flag){
                
                ws2812b_clear_effects(&ws2812b_effects);
                
                user_program_state = USER_PROGRAM_STATE_WS2812_RAINBOW_CYCLE_EFFECT;
                eic_button1_callback_flag = 0;
            }   
        break;
        
        case USER_PROGRAM_STATE_WS2812_RAINBOW_CYCLE_EFFECT:
                
            WS2812B_RAINBOW_CYCLE_Tasks(&ws2812b, &ws2812b_effects);
            
            _wait = 1;
            
            if(eic_button1_callback_flag){
                
                ws2812b_clear_effects(&ws2812b_effects);
                
                user_program_state = USER_PROGRAM_STATE_WS2812_COLOR_WIPE_EFFECT; 
                eic_button1_callback_flag = 0;
            }
        break;
        
        case USER_PROGRAM_STATE_WS2812_COLOR_WIPE_EFFECT:
            {
                static uint8_t _color_wipe_updown_state = 0;

                switch(_color_wipe_updown_state){

                    case 0:
                        if(WS2812B_COLOR_WIPE_Tasks(&ws2812b, &ws2812b_effects, 255, 0, 0) == WS2812B_COLOR_WIPE_STATE_IDLE) _color_wipe_updown_state = 1;
                    break;

                    case 1:
                        if(WS2812B_COLOR_WIPE_Tasks(&ws2812b, &ws2812b_effects, 0, 0, 0) == WS2812B_COLOR_WIPE_STATE_IDLE) _color_wipe_updown_state = 0;
                    break;

                    default:
                        // Nothing
                    break;
                }
            }
            
            _wait = 1;
        
            if(eic_button1_callback_flag){
                
                ws2812b_clear_effects(&ws2812b_effects);
                
                user_program_state = USER_PROGRAM_STATE_SPARKLE_WIPE_EFFECT;
                eic_button1_callback_flag = 0;
            }
        break;
        
        case USER_PROGRAM_STATE_SPARKLE_WIPE_EFFECT:
            
            if(WS2812B_SPARKLE_Tasks(&ws2812b, &ws2812b_effects, ws2812b_random, 255, 0, 0) == WS2812B_SPARKLE_STATE_WAIT) _wait = 1;
            else _wait = 0;
                
            if(eic_button1_callback_flag){
                
                ws2812b_clear_effects(&ws2812b_effects);
                ws2812_set_strip_color(&ws2812b, 0, 0, 0);
                ws2812_write_data_polled(&ws2812b);
                
                user_program_state = USER_PROGRAM_STATE_IDLE;
                eic_button1_callback_flag = 0;
            }
        break;
            
        case USER_PROGRAM_STATE_IDLE:
            
            if(eic_button1_callback_flag){
                user_program_state = USER_PROGRAM_STATE_WS2812_FADEIN_FADEOUT_EFFECT; 
                eic_button1_callback_flag = 0;
            }  
        break;
            
        default:
            // Nothing
        break;
    }
    
    return _wait;
}

/*******************************************************************************
 End of File
*/

