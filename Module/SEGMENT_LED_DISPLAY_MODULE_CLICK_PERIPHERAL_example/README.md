CZ / [EN](README_EN.md)

# SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example

Nacházíš se ve složce projektu SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example. Uživatelský program používá jedno tlačítko pro přepínání mezi módy (decimal counter, hexadecimal counter a button counter), dále používá zbávající dvě tlačítka pro přičítání nebo odečítání jedničky od hodnoty, která se zorazuje na display v posledním módu. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">

<br>

### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">

<br>

### Nastavení komponenty sercom4 jako EDBG USART

Zde vybereme možnost USART with Internal Clock, dále blocking mód, USART frame with parity, baudrate nastavíme na 19200 bauds, paritu na No Parity, Character size nastavíme na 8 bitovou velikost dat, Stop bit nastavíme na One Stop Bit.

Nejdůležitější částí je Receive Pinout a Transmit Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, což nám teď momentálně nic neříká, a proto je dobré se buď podívat do datasheetu, který pad odpovídá jakému pinu a nebo se podívat do harmony v3 Pin Table.

<img src="img/sercom4_edbg_usart_config.png" width="70%">

<br>

### Nastavení komponenty stdio

Zde vybereme možnost buffered mód.

<img src="img/stdio_config.png" width="60%">

<br>

### Nastavení komponenty tc0 jako timer

Zde vybereme 16-bit counter mód, Prescaler na 16 a mód nastavíme na Timer. V poslení řadě nastavíme time na 5ms a povolíme timer period interrupt.   
<img src="img/tc0_timer_config.png" width="70%">

<br>

### Nastavení komponenty tc1 jako timer

Zde vybereme 16-bit counter mód, Prescaler na 1024 a mód nastavíme na Timer. V poslení řadě nastavíme time na 1 vteřinu a povolíme timer period interrupt.   
<img src="img/tc1_timer_config.png" width="70%">

<br>

### Nastavení hodin
 
<img src="img/clock_config.png" width="100%">

<br>
V části Peripheral clock selection nastavíme Peripheral clock configuration na následující hodinové vstupy:   
<img src="img/clock_sercom4_config.png" width="100%">
<img src="img/clock_tc0_config.png" width="100%">
<img src="img/clock_eic_config.png" width="100%">

<br>

### Nastavení komponenty EIC
 
<img src="img/eic_config.png" width="80%">

<br>

### Nastavení pinů

Nastavení pinů pro komunikaci na USART sběrnici:  
<img src="img/pin_edbg_usart_config.png" width="100%">

Nastavení pinů pro Segment LED display:  
<img src="img/pin_segment_led_display_1_config.png" width="100%">
<img src="img/pin_segment_led_display_2_config.png" width="100%">
<img src="img/pin_segment_led_display_3_config.png" width="100%">
<img src="img/pin_segment_led_display_4_config.png" width="100%">
<img src="img/pin_segment_led_display_5_config.png" width="100%">
<img src="img/pin_segment_led_display_6_config.png" width="100%">

Nastavení pinů pro led diody:  
<img src="img/pin_led_config.png" width="100%">

Nastavení pinů pro tlačítka:  
<img src="img/pin_button_config.png" width="100%">

Nastavení power pinů:  
<img src="img/pin_power_5v_enable_config.png" width="100%">
<img src="img/pin_power_m2_enable_config.png" width="100%">