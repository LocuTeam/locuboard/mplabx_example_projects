#include <stddef.h>      // Defines NULL
#include <stdbool.h>     // Defines true
#include <stdlib.h>      // Defines EXIT_FAILURE
#include "definitions.h" // SYS function prototypes

#include "config/default/my_drivers/segment_led_display/segment_led_display.h" // Segment LED display library include


segment_led_display_t display; // Create structure for Segment LED display information holding


typedef enum{
    USER_PROGRAM_STATE_INIT           = 0,
    USER_PROGRAM_STATE_NUM_COUNTER    = 1,
    USER_PROGRAM_STATE_HEXNUM_COUNTER = 2,
    USER_PROGRAM_STATE_BUTTON_COUNTER = 3
} USER_PROGRAM_STATE_e;

USER_PROGRAM_STATE_e user_program_state = USER_PROGRAM_STATE_INIT;



volatile uint8_t eic_button1_flag = 0;
volatile uint8_t eic_button2_flag = 0;
volatile uint8_t eic_button3_flag = 0;

void eic_button1_callback(uintptr_t context){
    eic_button1_flag = 1;
}

void eic_button2_callback(uintptr_t context){
    eic_button2_flag = 1;
}

void eic_button3_callback(uintptr_t context){
    eic_button3_flag = 1;
}



volatile uint8_t tc0_timer_flag = 0;
volatile uint8_t tc1_timer_flag = 0;

// 5ms timer for segment switching
void tc0_timer_callback(TC_TIMER_STATUS status, uintptr_t context){
    tc0_timer_flag = 1;
}

// 1s timer for value counter
void tc1_timer_callback(TC_TIMER_STATUS status, uintptr_t context){
    tc1_timer_flag = 1;
}


// User function for gpio control
void user_segment_led_display_gpio_write(uint8_t pin, uint8_t value){
    PORT_PinWrite(pin, (_Bool) value);
}



void USER_PROGRAM_Tasks(void); // user program tasks function prototype



int main(void){
    
    SYS_Initialize ( NULL ); // Initialize all modules
    
    
    SERCOM4_USART_ReceiverDisable();   // Disable receiver 
    SERCOM4_USART_TransmitterEnable(); // Enable transmitter
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    
    // Register EIC callback for button interaction
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_callback, (uintptr_t) NULL);
    EIC_CallbackRegister(EIC_PIN_7, eic_button2_callback, (uintptr_t) NULL);
    EIC_CallbackRegister(EIC_PIN_15, eic_button3_callback, (uintptr_t) NULL);
    
    
    // Register Timer callback for "delay"
    TC0_TimerCallbackRegister(tc0_timer_callback, (uintptr_t) NULL);
    TC1_TimerCallbackRegister(tc1_timer_callback, (uintptr_t) NULL);
    
    
    segment_led_display_pinout_t pinout_config; // Configurate gpio pins for Segment LED display interaction
    pinout_config.digit.d1   = USER_CLICK_RX_PIN;
    pinout_config.digit.d2   = USER_CLICK_TX_PIN;
    pinout_config.segment.A  = USER_CLICK_AN_PIN;
    pinout_config.segment.B  = USER_CLICK_RST_PIN;
    pinout_config.segment.C  = USER_CLICK_CS_PIN;
    pinout_config.segment.D  = USER_CLICK_SCK_PIN;
    pinout_config.segment.E  = USER_CLICK_MISO_PIN;
    pinout_config.segment.F  = USER_CLICK_MOSI_PIN;
    pinout_config.segment.G  = USER_CLICK_PWM_PIN;
    pinout_config.segment.DP = USER_CLICK_INT_PIN;
    
    // Define hexadecimal chars in binary for this segment arrangement -> DP, G, F, E, D, C, B, A
    uint8_t binary_hexchar_config[18] = {   
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_ZERO,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_ONE,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_TWO,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_THREE,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_FOUR,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_FIVE,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_SIX,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_SEVEN,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_EIGHT,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NINE,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_A,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_B,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_C,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_D,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_E,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_F,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_DOT,
                                            SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL
                                        };
    
    // Initialize Segment LED display
    segment_led_display_init(&display, user_segment_led_display_gpio_write, &pinout_config, &binary_hexchar_config[0], 18);
    
    
    while(true){
        
        SYS_Tasks ( ); // Maintain state machines of all polled MPLAB Harmony modules.
        
        USER_PROGRAM_Tasks(); // User program state machine
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


void USER_PROGRAM_Tasks(void){
    
    static int8_t display_num_dec;
    static uint8_t display_num_hex;
    
    switch(user_program_state){
        case USER_PROGRAM_STATE_INIT:
            TC0_TimerStart(); // Start 5ms timer
            TC1_TimerStart(); // Start 1s timer
            
            display_num_dec = -9; // Set default value for decimal numbers
            display_num_hex = 0x00; // Set default value for hexadecimal numbers
            
            segment_led_display_set_num(&display, display_num_dec); // Save number into the Segment LED display structure
            
            user_program_state = USER_PROGRAM_STATE_NUM_COUNTER;
        break;
        
        case USER_PROGRAM_STATE_NUM_COUNTER:
            
            if(tc0_timer_flag){
                tc0_timer_flag = 0;
                SEGMENT_LED_DISPLAY_NUM_Tasks(&display); // Each 5ms go through Segment LED display state machine to apply set number value
            }

            if(tc1_timer_flag){ // Each 1s count from -9 to 99
                tc1_timer_flag = 0;

                if(display_num_dec >= 99) display_num_dec = -9;
                else display_num_dec++;

                segment_led_display_set_num(&display, display_num_dec); // Save number into the Segment LED display structure
            }
            
            if(eic_button2_flag){ // If button S2 is pressed move to the next user program mode
                eic_button2_flag = 0;
                
                display_num_hex = 0x00; // Clear display hexadecimal number variable
                segment_led_display_set_hexnum(&display, display_num_hex); // Save hexnum value into the Segment LED display structure
                
                user_program_state = USER_PROGRAM_STATE_HEXNUM_COUNTER;
            }
        break;
        
        case USER_PROGRAM_STATE_HEXNUM_COUNTER:
            
            if(tc0_timer_flag){
                tc0_timer_flag = 0;
                SEGMENT_LED_DISPLAY_HEXNUM_Tasks(&display); // Each 5ms go through Segment LED display state machine to apply set hexadecimal number value
            }
            
            if(tc1_timer_flag){ // Each 1s count from 0x00 to 0xFF
                tc1_timer_flag = 0;

                if(display_num_hex >= 0xFF) display_num_hex = 0x00;
                else display_num_hex++;

                segment_led_display_set_hexnum(&display, display_num_hex); // Save hexadecimal number value into the Segment LED display structure
            }
            
            if(eic_button2_flag){ // If button S2 is pressed move to the next user program mode
                eic_button2_flag = 0;
                
                display_num_dec = 0; // Clear display number variable
                segment_led_display_set_num(&display, display_num_dec); // Save number into the Segment LED display structure
                TC1_TimerStop(); // Stop do not need 1s timer 
                
                eic_button1_flag = 0; // Clear EIC button S1 flag
                eic_button3_flag = 0; // Clear EIC button S3 flag
                
                user_program_state = USER_PROGRAM_STATE_BUTTON_COUNTER;
            }
        break;
        
        case USER_PROGRAM_STATE_BUTTON_COUNTER:
            
            if(tc0_timer_flag){
                tc0_timer_flag = 0;
                SEGMENT_LED_DISPLAY_NUM_Tasks(&display); // Each 5ms go through Segment LED display state machine to apply set number value
            }
            
            if(eic_button3_flag){ // If button S3 is pressed increment decimal value
                eic_button3_flag = 0;
                if(display_num_dec < 99) segment_led_display_set_num(&display, ++display_num_dec); // Save incremented number into the Segment LED display structure
            }
            
            if(eic_button1_flag){ // If button S1 is pressed decrement decimal value
                eic_button1_flag = 0;
                if(display_num_dec > -9) segment_led_display_set_num(&display, --display_num_dec); // Save decremented number into the Segment LED display structure
            }
            
            if(eic_button2_flag){ // If button S2 is pressed move to the next user program mode
                eic_button2_flag = 0;
                
                display_num_dec = -9; // Set number for decadic values to default value -9
                segment_led_display_set_num(&display, display_num_dec); // Save number into the Segment LED display structure
                
                tc1_timer_flag = 0; // Clear timer 1 (1sec) flag
                TC1_Timer16bitCounterSet(0); // Clear counter for timer 1
                TC1_TimerStart(); // Start Timer 1
                
                user_program_state = USER_PROGRAM_STATE_NUM_COUNTER;
            }
        break;
        
        default:
            // Nothing
        break;
    }
    
}

/*******************************************************************************
 End of File
*/

