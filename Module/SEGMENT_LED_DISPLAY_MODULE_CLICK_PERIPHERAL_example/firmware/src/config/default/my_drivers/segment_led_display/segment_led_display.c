/*
 * Segment LED display library � 2022 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    segment_led_display.c

  @Summary
    Segment LED display library.
*/


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "segment_led_display.h"

#include <stdint.h>
#include <stdlib.h>


/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */


void segment_led_display_init(segment_led_display_t *me, segment_led_display_gpio_write_funcptr_t gpio_write_funcptr, segment_led_display_pinout_t *pinout_config, uint8_t *binary_hexchar_config, uint8_t binary_hexchar_count){
    
    me->pinout.digit.d1 = pinout_config->digit.d1;
    me->pinout.digit.d2 = pinout_config->digit.d2;
    
    me->pinout.segment.A  = pinout_config->segment.A;
    me->pinout.segment.B  = pinout_config->segment.B;
    me->pinout.segment.C  = pinout_config->segment.C;
    me->pinout.segment.D  = pinout_config->segment.D;
    me->pinout.segment.E  = pinout_config->segment.E;
    me->pinout.segment.F  = pinout_config->segment.F;
    me->pinout.segment.G  = pinout_config->segment.G;
    me->pinout.segment.DP = pinout_config->segment.DP;
    
    me->binary_hexchar_count = binary_hexchar_count;
    
    for(uint8_t i = 0; i < me->binary_hexchar_count; i++) me->binary_hexchar[i] = binary_hexchar_config[i];
    
    me->gpio_write = gpio_write_funcptr;
    
    segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_NONE);
    
    segment_led_display_segment_write_binary(me, SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL);
    
    me->num_task_state = SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT1_SET;
    me->hexnum_task_state = SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT1_SET;
}


void segment_led_display_digit_set(segment_led_display_t *me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_e segment_option){
    
    switch(segment_option){
        case SEGMENT_LED_DISPLAY_DIGIT_OPTION_NONE:
            me->gpio_write(me->pinout.digit.d2, 0);
            me->gpio_write(me->pinout.digit.d1, 0);
        break;
        
        case SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT1:
            me->gpio_write(me->pinout.digit.d2, 0);
            me->gpio_write(me->pinout.digit.d1, 1);
        break;
        
        case SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT2:
            me->gpio_write(me->pinout.digit.d1, 0);
            me->gpio_write(me->pinout.digit.d2, 1);
        break;
        
        case SEGMENT_LED_DISPLAY_DIGIT_OPTION_ALL:
            me->gpio_write(me->pinout.digit.d1, 1);
            me->gpio_write(me->pinout.digit.d2, 1);
        break;
        
        default:
            // Nothing
        break;
    }
}


void segment_led_display_segment_write_binary(segment_led_display_t *me, uint8_t binary_data){
    me->gpio_write(me->pinout.segment.A, binary_data & 0b1);
    me->gpio_write(me->pinout.segment.B, (binary_data >> 1) & 0b1);
    me->gpio_write(me->pinout.segment.C, (binary_data >> 2) & 0b1);
    me->gpio_write(me->pinout.segment.D, (binary_data >> 3) & 0b1);
    me->gpio_write(me->pinout.segment.E, (binary_data >> 4) & 0b1);
    me->gpio_write(me->pinout.segment.F, (binary_data >> 5) & 0b1);
    me->gpio_write(me->pinout.segment.G, (binary_data >> 6) & 0b1);
    me->gpio_write(me->pinout.segment.DP, (binary_data >> 7) & 0b1);
}


void segment_led_display_segment_write_hexnum(segment_led_display_t *me, uint8_t hexnum){
    if(hexnum < me->binary_hexchar_count) segment_led_display_segment_write_binary(me, me->binary_hexchar[hexnum]);
}


void segment_led_display_set_num(segment_led_display_t *me, int8_t num){
    if((num >= -9) && (num <= 99)) me->num_val.dec = num;
}

void segment_led_display_set_hexnum(segment_led_display_t *me, uint8_t hexnum){
    me->num_val.hex = hexnum;
}


SEGMENT_LED_DISPLAY_NUM_TASK_STATE_e SEGMENT_LED_DISPLAY_NUM_Tasks(segment_led_display_t *me){
    
    switch(me->num_task_state){        
        case SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT1_SET:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT1);
                    
            if(me->num_val.dec >= 10) segment_led_display_segment_write_hexnum(me, me->num_val.dec / 10);
            else if((me->num_val.dec < 10) && (me->num_val.dec >= 0)) segment_led_display_segment_write_binary(me, SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL);
            else segment_led_display_segment_write_binary(me, SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_DASH);
                    
            me->num_task_state = SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT1_CLEAR;
        break;
        
        case SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT1_CLEAR:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_NONE);
                    
            segment_led_display_segment_write_binary(me, SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL);
                    
            me->num_task_state = SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT2_SET;
        break;
                
        case SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT2_SET:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT2);
                    
            if(me->num_val.dec >= 0) segment_led_display_segment_write_hexnum(me, me->num_val.dec % 10);
            else segment_led_display_segment_write_hexnum(me, (me->num_val.dec * (-1)) % 10);
                    
            me->num_task_state = SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT2_CLEAR;
        break;
        
        case SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT2_CLEAR:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_NONE);
                    
            segment_led_display_segment_write_binary(me, SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL);
                    
            me->num_task_state = SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT1_SET;
        break;
                
        default:
            // Nothing
        break;
    }
    
    return me->num_task_state;
}


SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_e SEGMENT_LED_DISPLAY_HEXNUM_Tasks(segment_led_display_t *me){
    
    switch(me->hexnum_task_state){        
        case SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT1_SET:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT1);
                    
            segment_led_display_segment_write_hexnum(me, me->num_val.hex / 16);
                    
            me->hexnum_task_state = SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT1_CLEAR;
        break;
        
        case SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT1_CLEAR:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_NONE);
                    
            segment_led_display_segment_write_binary(me, SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL);
                    
            me->hexnum_task_state = SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT2_SET;
        break;
                
        case SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT2_SET:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT2);
                    
            segment_led_display_segment_write_hexnum(me, me->num_val.hex % 16);
                    
            me->hexnum_task_state = SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT2_CLEAR;
        break;
        
        case SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT2_CLEAR:
                    
            segment_led_display_digit_set(me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_NONE);
                    
            segment_led_display_segment_write_binary(me, SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL);
                    
            me->hexnum_task_state = SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT1_SET;
        break;
                
        default:
            // Nothing
        break;
    }
    
    return me->hexnum_task_state;
}
