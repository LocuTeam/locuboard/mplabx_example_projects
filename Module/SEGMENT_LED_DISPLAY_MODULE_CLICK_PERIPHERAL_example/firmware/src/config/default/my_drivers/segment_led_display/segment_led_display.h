/*
 * Segment LED display library � 2022 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    segment_led_display.h

  @Summary
    Segment LED display library.
*/


#ifndef _SEGMENT_LED_DISPLAY_H    /* Guard against multiple inclusion */
#define _SEGMENT_LED_DISPLAY_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_ZERO  __UINT8_C(0b00111111)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_ONE   __UINT8_C(0b00000110)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_TWO   __UINT8_C(0b01011011)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_THREE __UINT8_C(0b01001111)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_FOUR  __UINT8_C(0b01100110)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_FIVE  __UINT8_C(0b01101101)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_SIX   __UINT8_C(0b01111101)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_SEVEN __UINT8_C(0b00000111)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_EIGHT __UINT8_C(0b01111111)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NINE  __UINT8_C(0b01101111)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_A     __UINT8_C(0b01110111)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_B     __UINT8_C(0b01111100)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_C     __UINT8_C(0b00111001)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_D     __UINT8_C(0b01011110)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_E     __UINT8_C(0b01111001)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_F     __UINT8_C(0b01110001)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_DOT   __UINT8_C(0b10000000)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_NULL  __UINT8_C(0b00000000)
    #define SEGMENT_LED_DISPLAY_DEFAULT_BINARY_HEXCHAR_DASH  __UINT8_C(0b01000000)

    #define SEGMENT_LED_DISPLAY_MAX_NUM_BINARY_HEXCHARS 32
    
    

    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    
    
    typedef struct segment_led_display_descriptor_t segment_led_display_t;
    typedef struct segment_led_display_pinout_descriptor_t segment_led_display_pinout_t;

    typedef void (*segment_led_display_gpio_write_funcptr_t)(uint8_t pin, uint8_t value);


    typedef enum{
        SEGMENT_LED_DISPLAY_DIGIT_OPTION_NONE   = 0b00,
        SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT1 = 0b01,
        SEGMENT_LED_DISPLAY_DIGIT_OPTION_DIGIT2 = 0b10,
        SEGMENT_LED_DISPLAY_DIGIT_OPTION_ALL    = 0b11
    } SEGMENT_LED_DISPLAY_DIGIT_OPTION_e;


    typedef enum{
        SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT1_SET   = 0,
        SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT1_CLEAR = 1,
        SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT2_SET   = 2,
        SEGMENT_LED_DISPLAY_NUM_TASK_STATE_DIGIT2_CLEAR = 3
    } SEGMENT_LED_DISPLAY_NUM_TASK_STATE_e;


    typedef enum{
        SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT1_SET   = 0,
        SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT1_CLEAR = 1,
        SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT2_SET   = 2,
        SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_DIGIT2_CLEAR = 3
    } SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_e;


    struct segment_led_display_pinout_descriptor_t {
        struct{
            uint8_t A;
            uint8_t B;
            uint8_t C;
            uint8_t D;
            uint8_t E;
            uint8_t F;
            uint8_t G;
            uint8_t DP;
        } segment;

        struct{
            uint8_t d1;
            uint8_t d2;
        } digit;
    };

    struct segment_led_display_descriptor_t{
        segment_led_display_pinout_t pinout;
        segment_led_display_gpio_write_funcptr_t gpio_write;
        uint8_t binary_hexchar[SEGMENT_LED_DISPLAY_MAX_NUM_BINARY_HEXCHARS];
        uint8_t binary_hexchar_count;

        SEGMENT_LED_DISPLAY_NUM_TASK_STATE_e num_task_state;
        SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_e hexnum_task_state;

        union{
            int8_t dec;
            uint8_t hex;
        } num_val; // from -9 to 99
    };

    
    void segment_led_display_init(segment_led_display_t *me, segment_led_display_gpio_write_funcptr_t gpio_write_funcptr, segment_led_display_pinout_t *pinout_config, uint8_t *binary_hexchar_config, uint8_t binary_hexchar_count);
    void segment_led_display_digit_set(segment_led_display_t *me, SEGMENT_LED_DISPLAY_DIGIT_OPTION_e segment_option);
    void segment_led_display_segment_write_binary(segment_led_display_t *me, uint8_t binary_data);
    void segment_led_display_segment_write_hexnum(segment_led_display_t *me, uint8_t hexnum);
    void segment_led_display_set_num(segment_led_display_t *me, int8_t num);
    void segment_led_display_set_hexnum(segment_led_display_t *me, uint8_t hexnum);
    
    SEGMENT_LED_DISPLAY_NUM_TASK_STATE_e SEGMENT_LED_DISPLAY_NUM_Tasks(segment_led_display_t *me);
    SEGMENT_LED_DISPLAY_HEXNUM_TASK_STATE_e SEGMENT_LED_DISPLAY_HEXNUM_Tasks(segment_led_display_t *me);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */