EN / [CZ](README.md)

# SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example

Nacházíš se ve složce projektu SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example. User code uses one button for mode switching (decimal counter, hexadecimal counter a button counter), next left buttons are used for increment or decrement viewed value on display in the last mode. User code can be found [here](firmware/src/main.c).


## Project settings

Picture below describes main project layout.  
<img src="img/project_config.png" width="500">


### System component settings

Here allow SysTick and turn on interrupt for using delay function in project.  
<img src="img/system_config.png" width="500">


### sercom4 component settings as EDBG USART

Here choose option USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 115200 bauds, parity to No Parity, Character size set to 8 bit data width, Stop bit set to One Stop Bit.  
<img src="img/sercom4_edbg_usart_config.png" width="70%">

<br>

### stdio component settings 

Here choose option buffered mode.  
<img src="img/stdio_config.png" width="60%">

<br>

### tc0 component settings as timer

Here chose 16-bit counter mode, Prescaler set to 16 and mode set to Timer. At the end set time to 5 ms and enable timer interrupt.  
<img src="img/tc0_timer_config.png" width="70%">

<br>

### tc1 component settings as timer

Here chose 16-bit counter mode, Prescaler set to 1024 and mode set to Timer. At the end set time to 1 second and enable timer interrupt.   
<img src="img/tc1_timer_config.png" width="70%">

<br>

### Clock settings
 
<img src="img/clock_config.png" width="100%">

<br>
In Peripheral clock selection set Peripheral clock configuration to clock inputs as below:   
<img src="img/clock_sercom4_config.png" width="100%">
<img src="img/clock_tc0_config.png" width="100%">
<img src="img/clock_eic_config.png" width="100%">

<br>

### EIC settings
 
<img src="img/eic_config.png" width="80%">

<br>

### Pin settings

USART bus pin settings:  
<img src="img/pin_edbg_usart_config.png" width="100%">

Segment LED display pin settings:  
<img src="img/pin_segment_led_display_1_config.png" width="100%">
<img src="img/pin_segment_led_display_2_config.png" width="100%">
<img src="img/pin_segment_led_display_3_config.png" width="100%">
<img src="img/pin_segment_led_display_4_config.png" width="100%">
<img src="img/pin_segment_led_display_5_config.png" width="100%">
<img src="img/pin_segment_led_display_6_config.png" width="100%">

LED pin settings:  
<img src="img/pin_led_config.png" width="100%">

Button pin settings:  
<img src="img/pin_button_config.png" width="100%">

Power pin settings:  
<img src="img/pin_power_5v_enable_config.png" width="100%">
<img src="img/pin_power_m2_enable_config.png" width="100%">