CZ / [EN](README_EN.md)

# Module project examples

Here you will find basic projects that implements basic buses for module communication, external interupt, ADC periphery or there is implemented peripherial functions for module control. In projects there is usually a library/driver of module that makes easy usage of module. For more informations look to each module projects.


## WS2812B_LED_STRIP_MODULE_SPI_example

In this project example is used SPI bus respectively only one wire of SPI the MOSI signal for WS2812B control. User code use buttons on the LocuBoard for choosing the light effect. For more informations click [here](WS2812B_LED_STRIP_MODULE_SPI_example).


## SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example

In this program project uses the gpio write function for segment and digit control on Segment LED display. User code uses one button for user program mode switching (decimal counter, hexadecimal counter and button counter), next left buttons are used for increment or decrement value, that is viewed on display in the last mode. For more informations click [here](SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example).