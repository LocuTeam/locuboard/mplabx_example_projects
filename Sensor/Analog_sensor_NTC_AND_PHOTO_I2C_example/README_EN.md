EN / [CZ](README.md)

# Analog_sensor_NTC_AND_PHOTO_I2C_example

You are at the project folder designated for reading analog data from analog sensor using I2C bus. User code reads periodicaly analogdata from analog sensor. User code can be found [here](firmware/src/main.c)


## Project settings

Picture below describes layout of each component used in project.   
<img src="img/project_config.png" width="500">

<br>

### System component settings   
<img src="img/system_config.png" width="500">

<br>

### SERCOM4 set as EDBG USART settings

Here we choose USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 115200 bauds, parity set to No Parity, Character size set to 8 bit width, Stop bit set to One Stop Bit.

The most important part are the Receive Pinout and the Transmit Pinout, these two we set to pin on development board where we want to send our data or where we want to read them. Notice there, that we have no pins to choose here, only pads, so we look for the datasheet or we look for the harmony v3 Pin Table, where the pins show their pads.   
<img src="img/sercom4_edbg_usart_config.png" width="80%">

<br>

### Sercom5 component settings

Choose I2C Master, next set SDA hold TIME to 300-600 ns and I2C TRise time set to 215ns.

The most important part is SDA Pinout and SCL Pinout, these pinouts set to pins due to pads for bus communication. Therefore we need to look for the documentation or look for harmony v3 Pin Table.   
<img src="img/sercom5_i2c_config.png" width="80%">

<br>

### Stdio component settings

Choose option buffered mode.   
<img src="img/stdio_config.png" width="60%">

<br>

### Tc0 set as Timer component settings

Here choose 16-bit counter mode, Prescaler set to 1024 and mode set to Timer.  
<img src="img/tc0_config.png" width="70%">

<br>

### Clock settings

Againts default settings disable DFLL and enable 16 MHz intern oscilator where set division to 4. Next choose in GCLK generator 0 as clock source OSC16M and let division on 1. Main clock let be as default, but in Backup set division to 4.   
Next set clock signal designated for AD converor and other components. For that enable GCLK generator 1 and as clock source choose OSCULP32K (intern 32kHz low power crystal).   
<img src="img/clock_config.png" width="100%">

<br>
In Peripheral Clock selection part set Peripheral clock configuration to following clock inputs:   
<img src="img/clock_sercom4_sercom5_tc0_config.png" width="100%">
<img src="img/clock_eic_config.png" width="100%">

<br>

### EIC component settings

First choose as clock input Clocked by ULP32K. The next choose chanel 6, 7 and 15. These channels correspond to button pins. In every chanel choose option Enable Interrupt, next choose Edge detection is clock asynchronously operated, next choose Falling edge detection and at the end choose Enable filter.    
<img src="img/eic_config.png" width="80%">

<br>

### Pin settings

Pin settings for USART bus communication:   
<img src="img/pin_edbg_usart_config.png" width="100%">

AD convertor pin settings:  
<img src="img/pin_i2c_config.png" width="100%">

Pin settings for LED diods:  
<img src="img/pin_led_config.png" width="100%">

Pin settings for button:   
<img src="img/pin_button_config.png" width="100%">

Power pin settings:  
<img src="img/pin_power_5v_enable_config.png" width="100%">
<img src="img/pin_power_m2_enable_config.png" width="100%">