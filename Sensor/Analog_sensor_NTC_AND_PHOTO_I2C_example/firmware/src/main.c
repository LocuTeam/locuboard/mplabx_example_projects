/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


#define ANALOG_SENSOR_I2C_SLAVE_ADDRESS 0x20 // Analog sensor i2c slave address


typedef enum{
    ANALOG_SENSOR_INIT        = 0,
    ANALOG_SENSOR_INIT_CHECK  = 1,
    ANALOG_SENSOR_READ_NTC    = 2,
    ANALOG_SENSOR_PRINT_NTC   = 3,
    ANALOG_SENSOR_READ_PHOTO  = 4,
    ANALOG_SENSOR_PRINT_PHOTO = 5,
}ANALOG_SENSOR_STATE_e;

ANALOG_SENSOR_STATE_e analog_sensor_state = ANALOG_SENSOR_INIT;


uint8_t i2c_data = 0; // Global i2c data variable
uint8_t i2c_reg  = 0; // Global i2c register variable

volatile uint8_t i2c_flag = 0; // I2C flag

void i2c_callback(uintptr_t contextHandle){
    if(SERCOM5_I2C_ErrorGet() == SERCOM_I2C_ERROR_NONE){
        i2c_flag |= 1; // If transmission is ok set transmission completed flag as 1
    }
    else{
        i2c_flag |= 1 << 1; // If transmission is no ok set transmission corrupted flag as 1
    }
}


volatile uint8_t timer_flag = 0; // Timer flag

void timer_callback(TC_TIMER_STATUS status, uintptr_t context){
    timer_flag = 1; // Set timer_flag
}


void ANALOG_SENSOR_Tasks(uint8_t *analog_sensor_done_reading); // Create function prototype for ANALOG_SENSOR_Tasks() function


int main ( void ){
    
    SYS_Initialize ( NULL ); // Initialize all modules
    
    
    SERCOM5_I2C_CallbackRegister(i2c_callback, (uintptr_t) NULL); // Register i2c callback handler
    
    SERCOM_I2C_TRANSFER_SETUP i2c_setup; // Variable for storing setup information for i2c
    
    i2c_setup.clkSpeed = 100000; // Clock speed set to 100kHz
    
    SERCOM5_I2C_TransferSetup(&i2c_setup, 0); // Apply I2C configuration (0 - default clock frequency)
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)

    
    TC0_TimerCallbackRegister(timer_callback, (uintptr_t) NULL); // Register timer callback handler
    TC0_TimerStart(); // Start timer
    
    
    printf("LocuBoard is ready.\r\n");
    
    while ( true ){
        
        SYS_Tasks(); // Maintain state machines of all polled MPLAB Harmony modules.
        
        if(timer_flag){
            
            uint8_t _analog_reading_done = 0;
            
            ANALOG_SENSOR_Tasks(&_analog_reading_done);
            
            if(_analog_reading_done == true){
                timer_flag = 0;
            }
        }
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


void ANALOG_SENSOR_Tasks(uint8_t *analog_sensor_done_reading){
    switch(analog_sensor_state){
        case ANALOG_SENSOR_INIT:
            {
                uint8_t _data[2];
                _data[0] = 0x01;
                _data[1] = 0b11000000; // config - temperature data shifts ({temp+40}*2), All data stored as Little endian
                SERCOM5_I2C_Write(ANALOG_SENSOR_I2C_SLAVE_ADDRESS, _data, 2);
            } 
            analog_sensor_state = ANALOG_SENSOR_INIT_CHECK;
        break;
        
        case ANALOG_SENSOR_INIT_CHECK:
            if(i2c_flag){
                switch(i2c_flag){
                    case 1:
                        printf("Analog sensor initialization has been successfully completed.\r\n");
                        analog_sensor_state = ANALOG_SENSOR_READ_NTC;
                        i2c_flag &= 0b10;
                    break;

                    case 2:
                        printf("I2C error on the bus\r\n");
                        analog_sensor_state = ANALOG_SENSOR_INIT;
                        i2c_flag &= 0b01;
                    break;

                    default:
                        analog_sensor_state = ANALOG_SENSOR_INIT;
                        i2c_flag = 0;
                    break;
                }
            }
        break;
            
        case ANALOG_SENSOR_READ_NTC:
            
            i2c_reg = 0x04;
            SERCOM5_I2C_WriteRead(ANALOG_SENSOR_I2C_SLAVE_ADDRESS, &i2c_reg, 1, &i2c_data, 1);
                
            analog_sensor_state = ANALOG_SENSOR_PRINT_NTC;
        break;
           
        case ANALOG_SENSOR_PRINT_NTC:
                
            if(i2c_flag){
                switch(i2c_flag){
                    case 1:
                        // Data successfully received
                        printf("Temperature is: %dC\r\n", (i2c_data / 2) - 40);
                            
                        i2c_flag &= 0b10;
                    break;

                    case 2:
                        // I2C Error
                        printf("I2C error on the bus\r\n");

                        i2c_flag &= 0b01;
                    break;

                    default:
                        i2c_flag = 0;
                    break;
                }
                    
                analog_sensor_state = ANALOG_SENSOR_READ_PHOTO;
            }
        break;
            
        case ANALOG_SENSOR_READ_PHOTO:
            
            i2c_reg = 0x06;
            SERCOM5_I2C_WriteRead(ANALOG_SENSOR_I2C_SLAVE_ADDRESS, &i2c_reg, 1, &i2c_data, 1);
            
            analog_sensor_state = ANALOG_SENSOR_PRINT_PHOTO;
        break;
            
        case ANALOG_SENSOR_PRINT_PHOTO:
                
            if(i2c_flag){
                switch(i2c_flag){
                    case 1:
                        // Data successfully received
                        printf("Light intensity is: %d%%\r\n", i2c_data);
                            
                        i2c_flag &= 0b10;
                    break;

                    case 2:
                        // I2C Error
                        printf("I2C error on the bus\r\n");

                        i2c_flag &= 0b01;
                    break;

                    default:
                        i2c_flag = 0;
                    break;
                }
                
                // All reading done
                *analog_sensor_done_reading = 1;
                
                analog_sensor_state = ANALOG_SENSOR_READ_NTC;
            }
        break;
    }
}

/*******************************************************************************
 End of File
*/

