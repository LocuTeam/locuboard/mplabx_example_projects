/*
 * MCP9844 driver © 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    mcp9844.h

  @Summary
    Driver for microchip temperature sensor mcp9844.
*/


#ifndef _MCP9844_H    /* Guard against multiple inclusion */
#define _MCP9844_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    
    #define MCP9844_ERR __UINT8_C(0x00) // mcp9844 returned state of fuction ERROR
    #define MCP9844_OK  __UINT8_C(0x01) // mcp9844 returned state of fuction OK
    
    #define MCP9844_REGISTER_CAPABILITY  __UINT8_C(0x00) // Capibility register (Inform register -> Read only)
    #define MCP9844_REGISTER_CONFIG      __UINT8_C(0x01) // Config register
    #define MCP9844_REGISTER_TUPPER      __UINT8_C(0x02) // Upper temperature register (resolution = 0.5 C)
    #define MCP9844_REGISTER_TLOWER      __UINT8_C(0x03) // Lower temperature register (resolution = 0.5 C)
    #define MCP9844_REGISTER_TCRIT       __UINT8_C(0x04) // Critical temperature register (resolution = 0.5 C)
    #define MCP9844_REGISTER_TEMPERATURE __UINT8_C(0x05) // Ambient temperature Ta register
    #define MCP9844_REGISTER_MID         __UINT8_C(0x06) // Manufacturer ID register
    #define MCP9844_REGISTER_DRID        __UINT8_C(0x07) // Device ID and Revision ID
    #define MCP9844_REGISTER_RESOLUTION  __UINT8_C(0x09) // Resolution conf register (can be rewritten in shutdown mode only)
    
    #define MCP9844_TEMP_MAX __INT16_C(125) // Maximum temperature of mcp9844 temperature sensor
    #define MCP9844_TEMP_MIN __INT16_C(-40) // Minimum temperature of mcp9844 temperature sensor
    
    #define MCP9844_TEMP_LIMIT_DEFAULT_UPPER_VALUE    __INT16_C(35)  // MCP9844 default upper temperature threshold
    #define MCP9844_TEMP_LIMIT_DEFAULT_LOWER_VALUE    __INT16_C(-10) // MCP9844 default lower temperature threshold
    #define MCP9844_TEMP_LIMIT_DEFAULT_CRITICAL_VALUE __INT16_C(50)  // MCP9844 default critical temperature threshold
    
    #define MCP9844_LIMIT_HYSTERESIS_0C  __UINT8_C(0x00) // 0C
    #define MCP9844_LIMIT_HYSTERESIS_1C5 __UINT8_C(0x01) // 1,5C
    #define MCP9844_LIMIT_HYSTERESIS_3C  __UINT8_C(0x02) // 3C
    #define MCP9844_LIMIT_HYSTERESIS_6C  __UINT8_C(0x03) // 6C

    #define MCP9844_MEASURE_MODE_CONTINUOUS __UINT8_C(0x00) // Continuous measure state
    #define MCP9844_MEASURE_MODE_SHUTDOWN   __UINT8_C(0x01) // Shutdown measure state 

    #define MCP9844_LOCK_BIT_UNLOCKED __UINT8_C(0x00) // Lock bit unlocked
    #define MCP9844_LOCK_BIT_LOCKED   __UINT8_C(0x01) // Lock bit locked

    #define MCP9844_INTERRUPT_CLEAR_BIT_NO_EFFECT              __UINT8_C(0x00) // Interrupt no clear
    #define MCP9844_INTERRUPT_CLEAR_BIT_CLEAR_INTERRUPT_OUTPUT __UINT8_C(0x01) // Interrupt clear

    #define MCP9844_EVENT_OUTPUT_STATUS_NOT_ASSERTED_BY_DEVICE __UINT8_C(0x00) // Event status is not asserted by the device
    #define MCP9844_EVENT_OUTPUT_STATUS_ASSERTED_BY_DEVICE     __UINT8_C(0x01) // Event status is asserted by the device

    #define MCP9844_EVENT_OUTPUT_CONTROL_BIT_DISABLED __UINT8_C(0x00) // Event control disabled
    #define MCP9844_EVENT_OUTPUT_CONTROL_BIT_ENABLED  __UINT8_C(0x01) // Event control enabled

    #define MCP9844_EVENT_OUTPUT_SELECT_BIT_INTERRUPT_BY_ALL_TEMP_LIMITS   __UINT8_C(0x00) // event is asserted for all limits exceed
    #define MCP9844_EVENT_OUTPUT_SELECT_BIT_INTERRUPT_BY_CRITIC_TEMP_LIMIT __UINT8_C(0x01) // event is asserted only for critic limits exceed

    #define MCP9844_EVENT_OUTPUT_POLARITY_ACTIVE_LOW  __UINT8_C(0x00) // If interrupt occured event pin is set to low (need pull-up resistor)
    #define MCP9844_EVENT_OUTPUT_POLARITY_ACTIVE_HIGH __UINT8_C(0x01) // If interrupt occured event pin is set to high

    #define MCP9844_EVENT_OUTPUT_MODE_COMPARATOR_OUTPUT __UINT8_C(0x00) // Event is set to comparator mode (output event value, if limit is exceed, is constantly on the active value)
    #define MCP9844_EVENT_OUTPUT_MODE_INTERRUPT_OUTPUT  __UINT8_C(0x01) // Evet is set to timer mode (output event value, if limit is exceed, is triggered active value)

    #define MCP9844_MEASURE_RESOLUTION_0C5    __UINT8_C(0x00) // Measure resolution 0,5C
    #define MCP9844_MEASURE_RESOLUTION_0C25   __UINT8_C(0x01) // Measure resolution 0,25C
    #define MCP9844_MEASURE_RESOLUTION_0C125  __UINT8_C(0x02) // Measure resolution 0,125C
    #define MCP9844_MEASURE_RESOLUTION_0C0625 __UINT8_C(0x03) // Measure resolution 0,0625C



    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************

    typedef struct mcp9844_descriptor_t mcp9844_t;                                                       // Create mcp9844 device descriptor type
    typedef struct mcp9844_temp_descriptor_t mcp9844_temp_t;                                             // Create mcp9844 temp descriptor type
    typedef struct mcp9844_capability_descriptor_t mcp9844_capability_t;                                 // Create mcp9844 capability descriptor type
    typedef struct mcp9844_config_descriptor_t mcp9844_config_t;                                         // Create mcp9844 config descriptor type
    typedef struct mcp9844_IDs_descriptor_t mcp9844_IDs_t;                                               // Create mcp9844 IDs descriptor type     
    typedef struct mcp9844_temp_limits_descriptor_t mcp9844_temp_limits_t;                               // Create mcp9844 temperature limits descriptor type
    typedef struct mcp9844_temp_struct_for_temp_limits_descriptor_t mcp9844_temp_struct_for_temp_limits; // *
    
    typedef uint8_t (*mcp9844_i2c_read_funcptr)(mcp9844_t *me, uint8_t reg, uint8_t *data, uint8_t len);  // Create pointer function prototype as a new type
    typedef uint8_t (*mcp9844_i2c_write_funcptr)(mcp9844_t *me, uint8_t reg, uint8_t *data, uint8_t len); // Create pointer function prototype as a new type
    
    
    struct mcp9844_temp_struct_for_temp_limits_descriptor_t{
        int16_t integer; // Integer part of threshold temperature
        uint8_t decimal; // Decimal part of threshold temperature
    };
    
    struct mcp9844_temp_limits_descriptor_t{
        mcp9844_temp_struct_for_temp_limits critical; // Structure for critical temperature limit
        mcp9844_temp_struct_for_temp_limits lower;    // Structure for lower temperature limit
        mcp9844_temp_struct_for_temp_limits upper;    // Structure for upper temperature limit
    };
    
    struct mcp9844_config_descriptor_t{
        uint8_t limit_hysteresis;          // Config limit hysteresis (0C, 1C5, 3C, 6C)
        uint8_t measure_mode;              // Config measure mode (continue, shutdown)
        uint8_t Tcrit_lock_bit;            // Tcrit lock (unlocked, locked)
        uint8_t Tupp_Tlow_lock_bit;        // Tupp & Tlow lock (unlocked, locked)
        uint8_t interrupt_clear_bit;       // Interrupt clear (no effect, clear)
        uint8_t event_output_status;       // Event output status asserted (not by device, by device)
        uint8_t event_output_control_bit;  // Event output control (disable, enable)
        uint8_t event_output_select_bit;   // Event output select interrupt cause (all limits exceeded, critical limit exceeded)
        uint8_t event_output_polarity;     // Event output polarity (active low, active high)
        uint8_t event_output_mode;         // Event output mode (comparator, interrupt)
    };

    struct mcp9844_capability_descriptor_t{
        uint8_t shdn_status; // Event output status during shutdown
        uint8_t Tout_range;  // I2C bus timeout (0 -> 10ms - 60ms, 1 -> 25ms - 35ms)
        uint8_t resolution;  // Sensor resolution (sets in resolution register)
        uint8_t meas_range;  // If set to 0 -> not measure below the 0, If set to 1 temperature is measured below the 0
        uint8_t accuracy;    // Accuracy
        uint8_t temp_alarm;  // If set to 0 -> no alarm was set, If set to 1 alarm was set
    };

    struct mcp9844_temp_descriptor_t{
        int16_t integer;           // Integer part of temperature for mcp9844
        uint16_t decimal;          // Decimal part of temperature for mcp9844

        uint8_t _TAvsTCRIT_status; // Ta vs Tcrit temperature (If 0 - Ta < Tcrit, If 1 - Ta >= Tcrit) (If 1 -> Critical state of temperature)
        uint8_t _TAvsTUPP_status;  // Ta vs Tupper temperature (If 0 - Ta <= Tupper, If 1 - Ta > Tupper) (If 1 -> Temperature is above the upper limit of temperature)
        uint8_t _TAvsTLOW_status;  // Ta vs Tlower temperature (If 0 - Ta >= Tlower, If 1 - Ta < Tlower) (If 1 -> Temperature is below the lower limit of temperature)
    };

    struct mcp9844_IDs_descriptor_t{
        uint16_t manufacturer; // Manufacturer ID
        uint8_t device;        // Device ID
        uint8_t revision;      // Revision ID
    };

    struct mcp9844_descriptor_t{

        uint16_t slave_address;              // Slave address of I2C device

        mcp9844_IDs_t ids;                   // Device IDs

        mcp9844_i2c_read_funcptr i2c_read;   // Pointer to function for I2C read
        mcp9844_i2c_write_funcptr i2c_write; // Pointer to function for I2C write

        mcp9844_capability_t capability;     // Data of capability register

        mcp9844_temp_t temp;                 // MCP9844 temperature structure   
        
        mcp9844_temp_limits_t temp_limit;    // MCP9844 temperature limits structure

        uint8_t meas_resolution;             // MCP9844 temperature resolution

        mcp9844_config_t config;             // MCP9844 config structure
        uint8_t config_buff[2];              // MCP9844 config buffer
    };


    

    // *****************************************************************************
    // Section: Function Prototypes
    // *****************************************************************************

    
    uint8_t mcp9844_init(mcp9844_t *me, uint16_t slave_address, mcp9844_i2c_write_funcptr i2c_write_funcptr, mcp9844_i2c_read_funcptr i2c_read_funcptr);

    uint8_t mcp9844_read_2B_reg(mcp9844_t *me, uint8_t reg, uint8_t *data);
    
    void mcp9844_measure_resolution_set(mcp9844_t *me, uint8_t resolution);

    uint8_t mcp9844_measure_resolution_apply(mcp9844_t *me);


    uint8_t mcp9844_capability_read(mcp9844_t *me);


    uint8_t mcp9844_read_temperature(mcp9844_t *me);

    uint8_t mcp9844_read_manufacturer_id(mcp9844_t *me);


    uint8_t mcp9844_read_dev_rev_ids(mcp9844_t *me);


    uint8_t mcp9844_config_apply(mcp9844_t *me);

    void mcp9844_config_set_measure_mode(mcp9844_t *me, uint8_t mode);

    void mcp9844_config_set_limit_hysteresis(mcp9844_t *me, uint8_t limit_hysteresis);


    void mcp9844_config_set_interrupt_clear_bit(mcp9844_t *me, uint8_t interrupt_clear_bit);


    void mcp9844_config_set_event_output_status(mcp9844_t *me, uint8_t status);

    void mcp9844_config_set_event_output_control_bit(mcp9844_t *me, uint8_t control_bit);

    void mcp9844_config_set_event_output_select_bit(mcp9844_t *me, uint8_t select_bit);

    void mcp9844_config_set_event_output_polarity(mcp9844_t *me, uint8_t polarity);


    void mcp9844_config_set_event_output_mode(mcp9844_t *me, uint8_t mode);

    void mcp9844_config_set_Tcrit_lock_bit(mcp9844_t *me, uint8_t lock_bit);

    void mcp9844_config_set_Tupp_Tlow_lock_bit(mcp9844_t *me, uint8_t lock_bit);

    void mcp9844_config_get_default(mcp9844_t *me);
    
    
    void mcp9844_temp_threshold_crit_set(mcp9844_t *me, int16_t temp_int, uint8_t temp_dec);

    void mcp9844_temp_threshold_upper_set(mcp9844_t *me, int16_t temp_int, uint8_t temp_dec);

    void mcp9844_temp_threshold_lower_set(mcp9844_t *me, int16_t temp_int, uint8_t temp_dec);

    void mcp9844_temp_threshold_get_default(mcp9844_t *me);

    uint8_t mcp9844_temp_threshold_apply(mcp9844_t *me);
    


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
