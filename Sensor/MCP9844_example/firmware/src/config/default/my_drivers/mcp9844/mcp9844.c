/*
 * MCP9844 driver © 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    mcp9844.c

  @Summary
    Driver for microchip temperature sensor mcp9844.
*/


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "mcp9844.h"

#include <stdint.h>



/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */

/********************************************************* MCP9844 INIT FUNCTION ******************************************************************/
/*
   Description:
                Function for initializing mcp9844 driver.

   Parameters:
                me [in/out]            -> Structure instance of mcp9844_t
                slave_address [in]     -> Contains the slave address of I2C device
                i2c_write_funcptr [in] -> Contains pointer to the function for I2C write
                i2c_read_funcptr [in]  -> Contains pointer to the function for I2C read
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_init(mcp9844_t *me, uint16_t slave_address, mcp9844_i2c_write_funcptr i2c_write_funcptr, mcp9844_i2c_read_funcptr i2c_read_funcptr){
    
    uint8_t _state = MCP9844_ERR;      // If 1 correct post data, if 0 could not post data
    
    me->slave_address = slave_address; // Set slave address to the device
    
    me->i2c_write = i2c_write_funcptr; // Set i2c write function
    me->i2c_read  = i2c_read_funcptr;  // Set i2c read function

    _state = MCP9844_OK;               // Set state to 1 to indicate no error  
    
    return _state;                     // return state
}
/**************************************************************************************************************************************************/



/******************************* MCP9844 READ 2B REG FUNCTION ************************************/
/*
   Description:
                Read out 2 Bytes data from chosen register.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
                reg [in]    -> Contains the chosen register
                data [out]  -> Received data 
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_read_2B_reg(mcp9844_t *me, uint8_t reg, uint8_t *data){
    
    uint8_t _state = MCP9844_ERR;            // If 1 correct post data, if 0 could not post data
    
    _state = me->i2c_read(me, reg, data, 2); // Get data from register
    
    return _state;                           // return state of the bus
}
/*************************************************************************************************/



/************************** MCP9844 TEMP CRITICAL TRESHOLD SET FUNCTION **************************/
/*
   Description:
                Set critical temperature threshold and save it to the structure.
                Must be called mcp9844_temp_threshold_apply() function to apply the configuration.

   Parameters:
                me [in/out]    -> Structure instance of mcp9844_t
                temp_int [in]  -> Contains integer part of temperature
                temp_dec [out] -> Contains decimal part of temperature
   
   Returns:
                Nothing!
*/
void mcp9844_temp_threshold_crit_set(mcp9844_t *me, int16_t temp_int, uint8_t temp_dec){
    if((temp_int >= MCP9844_TEMP_MIN) && (temp_int <= MCP9844_TEMP_MAX)){
        me->temp_limit.critical.integer = temp_int;
        
        if(temp_dec >= 75) me->temp_limit.critical.decimal = 75;
        else if(temp_dec >= 50) me->temp_limit.critical.decimal = 50;
        else if(temp_dec >= 25) me->temp_limit.critical.decimal = 25;
        else if(temp_dec >= 0) me->temp_limit.critical.decimal = 0;
        else me->temp_limit.critical.decimal = 0;
    }
}
/*************************************************************************************************/



/*************************** MCP9844 TEMP UPPER TRESHOLD SET FUNCTION ****************************/
/*
   Description:
                Set upper temperature threshold and save it to the structure.
                Must be called mcp9844_temp_threshold_apply() function to apply the configuration.

   Parameters:
                me [in/out]    -> Structure instance of mcp9844_t
                temp_int [in]  -> Contains integer part of temperature
                temp_dec [out] -> Contains decimal part of temperature
   
   Returns:
                Nothing!
*/
void mcp9844_temp_threshold_upper_set(mcp9844_t *me, int16_t temp_int, uint8_t temp_dec){
    if((temp_int >= MCP9844_TEMP_MIN) && (temp_int <= MCP9844_TEMP_MAX)){
        me->temp_limit.upper.integer = temp_int;
        
        if(temp_dec >= 75) me->temp_limit.upper.decimal = 75;
        else if(temp_dec >= 50) me->temp_limit.upper.decimal = 50;
        else if(temp_dec >= 25) me->temp_limit.upper.decimal = 25;
        else if(temp_dec >= 0) me->temp_limit.upper.decimal = 0;
        else me->temp_limit.upper.decimal = 0;
    }
}
/*************************************************************************************************/



/*************************** MCP9844 TEMP LOWER TRESHOLD SET FUNCTION ****************************/
/*
   Description:
                Set lower temperature threshold and save it to the structure.
                Must be called mcp9844_temp_threshold_apply() function to apply the configuration.

   Parameters:
                me [in/out]    -> Structure instance of mcp9844_t
                temp_int [in]  -> Contains integer part of temperature
                temp_dec [out] -> Contains decimal part of temperature
   
   Returns:
                Nothing!
*/
void mcp9844_temp_threshold_lower_set(mcp9844_t *me, int16_t temp_int, uint8_t temp_dec){
    if((temp_int >= MCP9844_TEMP_MIN) && (temp_int <= MCP9844_TEMP_MAX)){
        me->temp_limit.lower.integer = temp_int;
        
        if(temp_dec >= 75) me->temp_limit.lower.decimal = 75;
        else if(temp_dec >= 50) me->temp_limit.lower.decimal = 50;
        else if(temp_dec >= 25) me->temp_limit.lower.decimal = 25;
        else if(temp_dec >= 0) me->temp_limit.lower.decimal = 0;
        else me->temp_limit.lower.decimal = 0;
    }
}
/*************************************************************************************************/



/************************** MCP9844 TEMP TRESHOLD GET DEFAULT FUNCTION ***************************/
/*
   Description:
                Get default temp threshold and save it to the structure.
                Must be called mcp9844_temp_threshold_apply() function to apply the configuration.

   Parameters:
                me [in/out]    -> Structure instance of mcp9844_t
   
   Returns:
                Nothing!
*/
void mcp9844_temp_threshold_get_default(mcp9844_t *me){
    me->temp_limit.upper.integer    = MCP9844_TEMP_LIMIT_DEFAULT_UPPER_VALUE;
    me->temp_limit.upper.decimal    = 0;
    
    me->temp_limit.lower.integer    = MCP9844_TEMP_LIMIT_DEFAULT_LOWER_VALUE;
    me->temp_limit.lower.decimal    = 0;
    
    me->temp_limit.critical.integer = MCP9844_TEMP_LIMIT_DEFAULT_CRITICAL_VALUE;
    me->temp_limit.critical.decimal = 0;
}
/*************************************************************************************************/



/***************************** MCP9844 TEMP TRESHOLD APPLY FUNCTION ******************************/
/*
   Description:
                Temp threshold apply function for sending configuration to the sensor.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_temp_threshold_apply(mcp9844_t *me){
    
    uint8_t _state = MCP9844_ERR;
    
    if((me->config.Tcrit_lock_bit == MCP9844_LOCK_BIT_UNLOCKED) && (me->config.Tupp_Tlow_lock_bit == MCP9844_LOCK_BIT_UNLOCKED)){
        
        _state = MCP9844_ERR; // Useless perhaps
        
        uint8_t _sign_upper    = ((me->temp_limit.upper.integer >> 15) & 0x01);
        uint8_t _sign_lower    = ((me->temp_limit.lower.integer >> 15) & 0x01);
        uint8_t _sign_critical = ((me->temp_limit.critical.integer >> 15) & 0x01);

        uint8_t _temp_upper    = 0;
        uint8_t _temp_lower    = 0;
        uint8_t _temp_critical = 0;

        uint8_t _i2c_buff[2] = {};

        if(_sign_upper == 1) _temp_upper = ((~ (me->temp_limit.upper.integer - 1)) & 0xFF);
        else _temp_upper = (me->temp_limit.upper.integer & 0xFF);

        if(_sign_lower == 1) _temp_lower = ((~ (me->temp_limit.lower.integer - 1)) & 0xFF);
        else _temp_lower = (me->temp_limit.lower.integer & 0xFF);

        if(_sign_critical == 1) _temp_critical = ((~ (me->temp_limit.critical.integer - 1)) & 0xFF);
        else _temp_critical = (me->temp_limit.critical.integer & 0xFF);

        
        _i2c_buff[0] = ((_sign_upper << 4) & 0x10) | ((_temp_upper >> 4) & 0x0F);
        _i2c_buff[1] = ((_temp_upper << 4) & 0xF0) | (((me->temp_limit.upper.decimal / 25) << 2) & 0b1100);
        
        _state = me->i2c_write(me, MCP9844_REGISTER_TUPPER, _i2c_buff, 2);
        
        
        _i2c_buff[0] = ((_sign_lower << 4) & 0x10) | ((_temp_lower >> 4) & 0x0F);
        _i2c_buff[1] = ((_temp_lower << 4) & 0xF0) | (((me->temp_limit.lower.decimal / 25) << 2) & 0b1100);
        
        if(_state == MCP9844_OK) _state = me->i2c_write(me, MCP9844_REGISTER_TLOWER, _i2c_buff, 2);
        
        
        _i2c_buff[0] = ((_sign_critical << 4) & 0x10) | ((_temp_critical >> 4) & 0x0F);
        _i2c_buff[1] = ((_temp_critical << 4) & 0xF0) | (((me->temp_limit.critical.decimal / 25) << 2) & 0b1100);
        
        if(_state == MCP9844_OK) _state = me->i2c_write(me, MCP9844_REGISTER_TCRIT, _i2c_buff, 2);
    }
    
    return _state;
}
/*************************************************************************************************/



/**************************** MCP9844 MEASURE RESOLUTION SET FUNCTION ****************************/
/*
   Description:
                Set measure resolution (0.5, 0.25, 0.125, 0.0625) and save it to the structure.

   Parameters:
                me [in/out]     -> Structure instance of mcp9844_t
                resolution [in] -> Contains resolution for measuring (sensor ADC conversion)
   
   Returns:
                Nothing!
*/
void mcp9844_measure_resolution_set(mcp9844_t *me, uint8_t resolution){
    me->meas_resolution = resolution & 0b11; // Set resolution from parameter to an internal memory
}
/*************************************************************************************************/



/*************************** MCP9844 MEASURE RESOLUTION APPLY FUNCTION ***************************/
/*
   Description:
                Measure resolution apply function for sending configuration to the sensor.

   Parameters:
                me [in/out]    -> Structure instance of mcp9844_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_measure_resolution_apply(mcp9844_t *me){
    
    uint8_t _state = MCP9844_ERR;                                          // If 1 correct post data, if 0 could not post data
    
    uint8_t _i2c_buff[2] = {};                                             // Create temporary _i2c_buff
    
    _i2c_buff[0] = 0;                                                      // Set first byte of register to 0
    _i2c_buff[1] = me->meas_resolution;                                    // Set second part of the register to the resolution

    _state = me->i2c_write(me, MCP9844_REGISTER_RESOLUTION, _i2c_buff, 2); // Send data through the i2c bus
    
    return _state;                                                         // return state of the bus
}
/*************************************************************************************************/



/******************************** MCP9844 CAPABILITY READ FUNCTION *******************************/
/*
   Description:
                Read capability information about sensor.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_capability_read(mcp9844_t *me){
    
    uint8_t _state = MCP9844_ERR;                                         // If 1 correct post data, if 0 could not post data
    
    uint8_t _i2c_buff[2] = {};                                            // Create temporary _i2c_buff
    
    _state = me->i2c_read(me, MCP9844_REGISTER_CAPABILITY, _i2c_buff, 2); // Read data about capability through the i2c bus
    
    me->capability.shdn_status = (_i2c_buff[1] >> 7) & 0x01;              // Parse SHDN status
    me->capability.Tout_range  = (_i2c_buff[1] >> 6) & 0x01;              // Parse Tout state
    me->capability.resolution  = (_i2c_buff[1] >> 3) & 0b00000011;        // Parse resolution
    me->capability.meas_range  = (_i2c_buff[1] >> 2) & 0x01;              // Parse measure range
    me->capability.accuracy    = (_i2c_buff[1] >> 1) & 0x01;              // Parse accuracy
    me->capability.temp_alarm  = _i2c_buff[1] & 0x01;                     // Parse temperature alarm
    
    return _state;                                                        // return state of the bus communication
}
/*************************************************************************************************/



/******************************* MCP9844 READ TEMPERATURE FUNCTION *******************************/
/*
   Description:
                Read temperature from the sensor.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_read_temperature(mcp9844_t *me){
    
    uint8_t _state = MCP9844_ERR;                                                   // If 1 correct post data, if 0 could not post data
    
    uint8_t _i2c_buff[2] = {};                                                      // Create temporary _i2c_buff 
    
    _state = me->i2c_read(me, MCP9844_REGISTER_TEMPERATURE, _i2c_buff, 2);          // Get data about temperature
    
    uint8_t _temp_sign = (_i2c_buff[0] >> 4) & 0x01;                                // Get value of sign 
        
    me->temp._TAvsTCRIT_status = (_i2c_buff[0] >> 7) & 0x01;
    me->temp._TAvsTUPP_status  = (_i2c_buff[0] >> 6) & 0x01;
    me->temp._TAvsTLOW_status  = (_i2c_buff[0] >> 5) & 0x01;
        
    me->temp.decimal = ((_i2c_buff[1] & 0x0F) * 100) / 16;                          // Compute temperature decimal part
        
    me->temp.integer = ((_i2c_buff[0] << 4) & 0xF0) | ((_i2c_buff[1] >> 4) & 0x0F); // Compute temperature integer part
        
    if(_temp_sign != 0) me->temp.integer = me->temp.integer - 256;                  // Compute temperature if the sign is 1

    return _state;                                                                  // return state of the bus communication
}
/*************************************************************************************************/



/***************************** MCP9844 READ MANUFACTURER ID FUNCTION *****************************/
/*
   Description:
                Read manufacturer id from the sensor.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_read_manufacturer_id(mcp9844_t *me){
    
    uint8_t _state = MCP9844_ERR;                                    // If 1 correct post data, if 0 could not post data
    
    uint8_t _i2c_buff[2] = {};                                       // Create temporary _i2c_buff 
    
    _state = me->i2c_read(me, MCP9844_REGISTER_MID, _i2c_buff, 2);   // Get data about manufacturer ID
    
    if(_state == 1){
        me->ids.manufacturer = ((_i2c_buff[0] << 8) | _i2c_buff[1]); // Parse manufacturer ID
    }
    return _state;                                                   // return state of the bus communication
}
/*************************************************************************************************/



/************************** MCP9844 READ DEVICE & REVISION ID FUNCTION ***************************/
/*
   Description:
                Read device & revision id from the sensor.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_read_dev_rev_ids(mcp9844_t *me){
    
    uint8_t _state = MCP9844_ERR;                                   // If 1 correct post data, if 0 could not post data
    
    uint8_t _i2c_buff[2] = {};                                      // Create temporary _i2c_buff
    
    _state = me->i2c_read(me, MCP9844_REGISTER_DRID, _i2c_buff, 2); // Get data about device ID & revision ID
    
    if(_state == 1){
        me->ids.device   = _i2c_buff[0];                            // Parse device ID
        me->ids.revision = _i2c_buff[1];                            // Parse revision ID
    }
    return _state;                                                  // return state of the bus communication
}
/*************************************************************************************************/



/************************** MCP9844 READ DEVICE & REVISION ID FUNCTION ***************************/
/*
   Description:
                Send configuration saved in the structure to the sensor.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t mcp9844_config_apply(mcp9844_t *me){
    
    uint8_t _state = MCP9844_ERR; // If 1 correct post data, if 0 could not post data
    
    me->config_buff[0] = ((me->config.limit_hysteresis << 1) | me->config.measure_mode) & 0b00000111; // Build first byte of config register
    
    me->config_buff[1] = (me->config.Tcrit_lock_bit << 7) | (me->config.Tupp_Tlow_lock_bit << 6) | (me->config.interrupt_clear_bit << 5) | (me->config.event_output_status << 4) | (me->config.event_output_control_bit << 3) | (me->config.event_output_select_bit << 2) | (me->config.event_output_polarity << 1) | me->config.event_output_mode; // Build second byte of config register
    
    _state = me->i2c_write(me, MCP9844_REGISTER_CONFIG, me->config_buff, 2); // Write configuration to the device
    
    return _state; // return state of the bus communication
}
/*************************************************************************************************/



/*************************** MCP9844 CONFIG SET MEASURE MODE FUNCTION ****************************/
/*
   Description:
                Set measure mode and save it to the structure.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
                mode [in]   -> Consists mode of measuring (continue, shutdown)
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_measure_mode(mcp9844_t *me, uint8_t mode){
    me->config.measure_mode = mode; // Set measure mode from parameter to an internal memory
}
/*************************************************************************************************/



/************************* MCP9844 CONFIG SET LIMIT HYSTERESIS FUNCTION **************************/
/*
   Description:
                Set limit hysteresis and save it to the structure.

   Parameters:
                me [in/out]           -> Structure instance of mcp9844_t
                limit_hysteresis [in] -> Consists limit hysteresis for measuring
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_limit_hysteresis(mcp9844_t *me, uint8_t limit_hysteresis){
    me->config.limit_hysteresis = limit_hysteresis; // Set limit hysteresis from parameter to an internal memory
}
/*************************************************************************************************/



/************************ MCP9844 CONFIG SET INTERRUPT CLEAR BIT FUNCTION ************************/
/*
   Description:
                Set interrupt clear bit and save it to the structure.

   Parameters:
                me [in/out]              -> Structure instance of mcp9844_t
                interrupt_clear_bit [in] -> Consists setting about interrupt clear bit (later for EXTINT)
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_interrupt_clear_bit(mcp9844_t *me, uint8_t interrupt_clear_bit){
    me->config.interrupt_clear_bit = interrupt_clear_bit; // Set interrupt clear bit from parameter to an internal memory
}
/*************************************************************************************************/



/************************ MCP9844 CONFIG SET EVENT OUTPUT STATUS FUNCTION ************************/
/*
   Description:
                Set event output status and save it to the structure.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
                status [in] -> Consists setting about event output status (later for EXTINT)
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_event_output_status(mcp9844_t *me, uint8_t status){
    me->config.event_output_status = status; // Set event output status from parameter to an internal memory
}
/*************************************************************************************************/



/********************* MCP9844 CONFIG SET EVENT OUTPUT CONTROL BIT FUNCTION **********************/
/*
   Description:
                Set event output control bit and save it to the structure.

   Parameters:
                me [in/out]      -> Structure instance of mcp9844_t
                control_bit [in] -> Consists setting about event output control bit (later for EXTINT)
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_event_output_control_bit(mcp9844_t *me, uint8_t control_bit){
    me->config.event_output_control_bit = control_bit; // Set control bit from parameter to an internal memory
}
/*************************************************************************************************/



/********************** MCP9844 CONFIG SET EVENT OUTPUT SELECT BIT FUNCTION **********************/
/*
   Description:
                Set event output select bit and save it to the structure.

   Parameters:
                me [in/out]     -> Structure instance of mcp9844_t
                select_bit [in] -> Consists setting about event output select bit (later for EXTINT)
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_event_output_select_bit(mcp9844_t *me, uint8_t select_bit){
    me->config.event_output_select_bit = select_bit; // Set select bit from parameter to an internal memory
}
/*************************************************************************************************/



/*********************** MCP9844 CONFIG SET EVENT OUTPUT POLARITY FUNCTION ***********************/
/*
   Description:
                Set event output polarity and save it to the structure.

   Parameters:
                me [in/out]   -> Structure instance of mcp9844_t
                polarity [in] -> Consists setting about event output polarity (later for EXTINT)
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_event_output_polarity(mcp9844_t *me, uint8_t polarity){
    me->config.event_output_polarity = polarity; // Set event output polarity from parameter to an internal memory
}
/*************************************************************************************************/



/************************* MCP9844 CONFIG SET EVENT OUTPUT MODE FUNCTION *************************/
/*
   Description:
                Set event output mode and save it to the structure.

   Parameters:
                me [in/out] -> Structure instance of mcp9844_t
                mode [in]   -> Consists setting about event output mode (later for EXTINT)
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_event_output_mode(mcp9844_t *me, uint8_t mode){
    me->config.event_output_mode = mode; // Set event output mode from parameter to an internal memory
}
/*************************************************************************************************/



/*************************** MCP9844 CONFIG SET TCRIT LOCK BIT FUNCTION **************************/
/*
   Description:
                Set Tcrit lock bit and save it to the structure.

   Parameters:
                me [in/out]   -> Structure instance of mcp9844_t
                lock_bit [in] -> Consists Tcrit lock bit
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_Tcrit_lock_bit(mcp9844_t *me, uint8_t lock_bit){
    me->config.Tcrit_lock_bit = lock_bit; // Set Tcrit lock bit from parameter to an internal memory
}
/*************************************************************************************************/



/**************************** MCP9844 CONFIG SET TUPP LOCK BIT FUNCTION **************************/
/*
   Description:
                Set Tupp lock bit and save it to the structure.

   Parameters:
                me [in/out]   -> Structure instance of mcp9844_t
                lock_bit [in] -> Consists Tupp lock bit
   
   Returns:
                Nothing !
*/
void mcp9844_config_set_Tupp_Tlow_lock_bit(mcp9844_t *me, uint8_t lock_bit){
    me->config.Tupp_Tlow_lock_bit = lock_bit; // Set Tupp and Tlow lock bit from parameter to an internal memory
}
/*************************************************************************************************/



/**************************** MCP9844 CONFIG SET TUPP LOCK BIT FUNCTION **************************/
/*
   Description:
                Get default MCP9844 config and save it to the structure.

   Parameters:
                me [in/out]   -> Structure instance of mcp9844_t
   
   Returns:
                Nothing !
*/
void mcp9844_config_get_default(mcp9844_t *me){
    me->config.measure_mode              = MCP9844_MEASURE_MODE_CONTINUOUS;                              // Set default measure mode to continuous
    me->config.limit_hysteresis          = MCP9844_LIMIT_HYSTERESIS_0C;                                  // Set default limit hysteresis to 0C
    me->config.interrupt_clear_bit       = MCP9844_INTERRUPT_CLEAR_BIT_NO_EFFECT;                        // Set default interrupt clear bit to NO EFFECT
            
    me->config.Tcrit_lock_bit            = MCP9844_LOCK_BIT_UNLOCKED;                                    // Set default Tcrit lock bit to UNLOCKED
    me->config.Tupp_Tlow_lock_bit        = MCP9844_LOCK_BIT_UNLOCKED;                                    // Set default Tupp & Tlow bit to UNLOCKED
    
    me->config.event_output_status       = MCP9844_EVENT_OUTPUT_STATUS_NOT_ASSERTED_BY_DEVICE;           // Set default output status to not asserted by device
    me->config.event_output_control_bit  = MCP9844_EVENT_OUTPUT_CONTROL_BIT_DISABLED;                    // Set default output control bit to disabled
    me->config.event_output_select_bit   = MCP9844_EVENT_OUTPUT_SELECT_BIT_INTERRUPT_BY_ALL_TEMP_LIMITS; // Set default output select bit to interrupt by all limits
    me->config.event_output_polarity     = MCP9844_EVENT_OUTPUT_POLARITY_ACTIVE_LOW;                     // Set default output polarity to active low (needs pull-up resistor)
    me->config.event_output_mode         = MCP9844_EVENT_OUTPUT_MODE_COMPARATOR_OUTPUT;                  // Set default output mode to comparator
}
/*************************************************************************************************/
