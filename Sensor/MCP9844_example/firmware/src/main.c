/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

#include "config/default/my_drivers/mcp9844/mcp9844.h" // Include mcp9844 driver


#define MCP9844_I2C_SLAVE_ADDRESS 0x18


mcp9844_t mcp9844; // Create an mcp9844 temperature sensor structure


volatile uint8_t timer_flag = 0; // Timer flag

void timer_callback(TC_TIMER_STATUS status, uintptr_t context){
    timer_flag = 1; // Set timer_flag
}


uint8_t user_i2c_read(mcp9844_t *me, uint8_t reg, uint8_t *data, uint8_t len);
uint8_t user_i2c_write(mcp9844_t *me, uint8_t reg, uint8_t *data, uint8_t len);

void my_read_temperature(void);
void my_read_all_regs(void);


int main ( void ){
    
    SYS_Initialize ( NULL ); // Initialize all modules
    
    
    SERCOM_I2C_TRANSFER_SETUP i2c_setup; // Variable for storing setup information for i2c
    
    i2c_setup.clkSpeed = 100000; // Clock speed set to 100kHz
    
    SERCOM5_I2C_TransferSetup(&i2c_setup, 0); // Apply I2C configuration (0 - default clock frequency)
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)

    
    TC0_TimerCallbackRegister(timer_callback, (uintptr_t) NULL); // Register timer callback handler
    TC0_TimerStart(); // Start timer
    
    
    mcp9844_init(&mcp9844, MCP9844_I2C_SLAVE_ADDRESS, user_i2c_write, user_i2c_read); // Init mcp9844 driver
    
    mcp9844_config_get_default(&mcp9844); // Get mcp9844 default configuration
    mcp9844_config_apply(&mcp9844); // Apply mcp9844 configuration
    
    // Set shutdown mode to write into resolution register
    mcp9844_config_set_measure_mode(&mcp9844, MCP9844_MEASURE_MODE_SHUTDOWN);
    mcp9844_config_apply(&mcp9844);
    
    // Write to the resolution register
    mcp9844_measure_resolution_set(&mcp9844, MCP9844_MEASURE_RESOLUTION_0C0625);
    mcp9844_measure_resolution_apply(&mcp9844);
    
    // Write upper, lower and critical temperature threshold for mcp9844
    mcp9844_temp_threshold_get_default(&mcp9844);
    mcp9844_temp_threshold_apply(&mcp9844);
    
    my_read_all_regs(); // Read all registers
    
    // Set continuous mode for mcp9844
    mcp9844_config_set_measure_mode(&mcp9844, MCP9844_MEASURE_MODE_CONTINUOUS);
    mcp9844_config_apply(&mcp9844);
    
    
    while ( true ){
        
        SYS_Tasks ( ); // Maintain state machines of all polled MPLAB Harmony modules.
        
        if(timer_flag){
            
            my_read_temperature();
            
            timer_flag = 0;
        }
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


uint8_t user_i2c_read(mcp9844_t *me, uint8_t reg, uint8_t *data, uint8_t len){
    
    uint8_t _state = 0;                                                    // If 1 correct post data, if 0 could not post data

    _state = SERCOM5_I2C_WriteRead(me->slave_address, &reg, 1, data, len); // Send register and get data from that register through the I2C bus
    
    while(SERCOM5_I2C_IsBusy() == 1);                                      // Wait for data transmission is done
    
    if(SERCOM5_I2C_ErrorGet() != SERCOM_I2C_ERROR_NONE){
        _state = 0;
        printf("---> I2C read error\r\n");
    }
    
    return _state;                                                        // return state of the bus communication
}

uint8_t user_i2c_write(mcp9844_t *me, uint8_t reg, uint8_t *data, uint8_t len){
    
    uint8_t _state = 0;                                                  // If 1 correct post data, if 0 could not post data
    
    uint8_t _i2c_buff[19] = {};                                          // Create an _i2c_buff
    
    _i2c_buff[0] = reg;                                                  // Set selected register to the first position of _i2c_buff
    
    for(uint8_t i = 1; i < (len + 1); i++) _i2c_buff[i] = data[i-1];     // Copy all data to an _i2c_buffer
    
    _state = SERCOM5_I2C_Write(me->slave_address, _i2c_buff, (len + 1)); // Send data through the I2C bus
    
    while(SERCOM5_I2C_IsBusy() == 1);                                    // Wait for data transmission is done
    
    if(SERCOM5_I2C_ErrorGet() != SERCOM_I2C_ERROR_NONE){
        _state = 0;
        printf("---> I2C write error\r\n");
    }
    
    return _state;                                                      // return state of the bus communication
}


void my_read_temperature(void){
    
    mcp9844_read_temperature(&mcp9844);
        
    printf("Ta %s Tcrit, Ta %s Tupp, Ta %s Tlow --- ", ((mcp9844.temp._TAvsTCRIT_status == 1) ? ">=" : "<"), ((mcp9844.temp._TAvsTUPP_status == 1) ? ">" : "<="), ((mcp9844.temp._TAvsTLOW_status == 1) ? "<" : ">="));
        
    printf("Temperature: %d.%02d\r\n", mcp9844.temp.integer, mcp9844.temp.decimal);
}

void my_read_all_regs(void){
    uint8_t _temp_buff[2] = {};
    
    mcp9844_capability_read(&mcp9844);
    printf("SHDN: %d, Tout Range: %d, Resolution: %d, Meas Range: %d, Accuracy: %d, Temperature Alarm: %d\r\n", mcp9844.capability.shdn_status, mcp9844.capability.Tout_range, mcp9844.capability.resolution, mcp9844.capability.meas_range, mcp9844.capability.accuracy, mcp9844.capability.temp_alarm);
            
    mcp9844_read_2B_reg(&mcp9844, MCP9844_REGISTER_CONFIG, _temp_buff);
    printf("Thyst: %d, SD Mode: %d, Tcrit lock: %d, Tupper and Tlower lock: %d, INT clear: %d, Ev. out. status: %d, Ev. out. controll bit: %d, Ev. out. select bit: %d, Ev. out. polarity: %d, Ev. out. mode: %d\r\n", ((_temp_buff[0] >> 1) & 0b11), (_temp_buff[0] & 1), ((_temp_buff[1] >> 7) & 1), ((_temp_buff[1] >> 6) & 1), ((_temp_buff[1] >> 5) & 1), ((_temp_buff[1] >> 4) & 1), ((_temp_buff[1] >> 3) & 1), ((_temp_buff[1] >> 2) & 1), ((_temp_buff[1] >> 1) & 1), (_temp_buff[1] & 1));
    
    mcp9844_read_2B_reg(&mcp9844, MCP9844_REGISTER_TUPPER, _temp_buff);
    printf("Tupper configuration -> Sign: %s, Temp int: %d, Temp dec: %d\r\n", ((((_temp_buff[0] >> 4) & 1) == 0) ? "+" : "-"), (((_temp_buff[0] << 4) & 0xF0) | ((_temp_buff[1] >> 4) & 0x0F)), (((_temp_buff[1] >> 2) & 0b11) * 100) / 4);
    
    mcp9844_read_2B_reg(&mcp9844, MCP9844_REGISTER_TLOWER, _temp_buff);
    printf("Tlower configuration -> Sign: %s, Temp int: %d, Temp dec: %d\r\n", ((((_temp_buff[0] >> 4) & 1) == 0) ? "+" : "-"), (((_temp_buff[0] << 4) & 0xF0) | ((_temp_buff[1] >> 4) & 0x0F)), (((_temp_buff[1] >> 2) & 0b11) * 100) / 4);
    
    mcp9844_read_2B_reg(&mcp9844, MCP9844_REGISTER_TCRIT, _temp_buff);
    printf("Tcrit configuration -> Sign: %s, Temp int: %d, Temp dec: %d\r\n", ((((_temp_buff[0] >> 4) & 1) == 0) ? "+" : "-"), (((_temp_buff[0] << 4) & 0xF0) | ((_temp_buff[1] >> 4) & 0x0F)), (((_temp_buff[1] >> 2) & 0b11) * 100) / 4);
    
    mcp9844_read_2B_reg(&mcp9844, MCP9844_REGISTER_TEMPERATURE, _temp_buff);
    printf("Ta %s Tcrit --- Ta %s Tupp --- Ta %s Tlow\r\n", ((((_temp_buff[0] >> 7) & 1) == 0) ? "<" : ">="), ((((_temp_buff[0] >> 6) & 1) == 0) ? "<=" : ">"), ((((_temp_buff[0] >> 5) & 1) == 0) ? ">=" : "<"));
    printf("Ta temperature -> Sign: %s, Temp int: %d, Temp dec: %d\r\n", ((((_temp_buff[0] >> 4) & 1) == 0) ? "+" : "-"), (((_temp_buff[0] << 4) & 0xF0) | ((_temp_buff[1] >> 4) & 0x0F)), ((_temp_buff[1] & 0x0F) * 100) / 16);
    
    mcp9844_read_manufacturer_id(&mcp9844);
    mcp9844_read_dev_rev_ids(&mcp9844);
    printf("Manufacturer ID: 0x%04X, Device ID: 0x%02X, Revision ID: 0x%02X\r\n", mcp9844.ids.manufacturer, mcp9844.ids.device, mcp9844.ids.revision);

    mcp9844_read_2B_reg(&mcp9844, MCP9844_REGISTER_RESOLUTION, _temp_buff);
    printf("Resolution = %s\r\n", (_temp_buff[1] == 0b00) ? ("0,5C (tconv = 30 ms)") : ((_temp_buff[0] == 0b01) ? ("0,25C (default, tconv = 65ms)") : ((_temp_buff[1] == 0b10) ? ("0,125C (tconv = 130ms)") : ((_temp_buff[1] == 0b11) ? ("0,0625C (tconv = 260ms)") : (":-(")))));
}

/*******************************************************************************
 End of File
*/

