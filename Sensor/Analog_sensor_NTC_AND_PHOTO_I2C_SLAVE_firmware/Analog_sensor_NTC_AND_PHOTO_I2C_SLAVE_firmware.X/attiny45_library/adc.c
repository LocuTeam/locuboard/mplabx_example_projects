#include "adc.h"

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

void adc_init(adc_config_t *config){
    
    ADCSRA = 0x00;
    
    if(config->adc_enable) ADCSRA |= ADC_SRA_ENABLE;
    
    if(config->adc_autotriger) ADCSRA |= ADC_SRA_ENABLE_AUTOTRIGGER;
    
    if(config->adc_start_conversion) ADCSRA |= ADC_SRA_START_CONVERSION;
    
    if(config->adc_interrupt_enable) ADCSRA |= ADC_SRA_ENABLE_INTERRUPT;
    
    switch(config->adc_prescaler){
        case ADC_PRESCALER_2:
            ADCSRA |= ADC_SRA_PRESCALER_2;
        break;
        
        case ADC_PRESCALER_4:
            ADCSRA |= ADC_SRA_PRESCALER_4;
        break;
        
        case ADC_PRESCALER_8:
            ADCSRA |= ADC_SRA_PRESCALER_8;
        break;
        
        case ADC_PRESCALER_16:
            ADCSRA |= ADC_SRA_PRESCALER_16;
        break;
        
        case ADC_PRESCALER_32:
            ADCSRA |= ADC_SRA_PRESCALER_32;
        break;
        
        case ADC_PRESCALER_64:
            ADCSRA |= ADC_SRA_PRESCALER_64;
        break;
        
        case ADC_PRESCALER_128:
            ADCSRA |= ADC_SRA_PRESCALER_128;
        break;
        
        default:
            // Nothing
        break;
    }
    
    
    ADCSRB = 0x00;
    
    if(config->adc_input_mode == ADC_INPUT_MODE_BIPOLAR) ADCSRB |= ADC_SRB_BIPOLAR_INPUT_MODE;
    
    switch(config->adc_trigger_source){
        
        case ADC_TRIGGER_SOURCE_FREE_RUNNING:
            ADCSRB |= ADC_SRB_TRIGGER_SOURCE_FREE_RUNNING;
        break;
        
        case ADC_TRIGGER_SOURCE_ANALOG_COMPARATOR:
            ADCSRB |= ADC_SRB_TRIGGER_SOURCE_ANALOG_COMPARATOR;
        break;
        
        case ADC_TRIGGER_SOURCE_EXTERN_INTERRUPT_REQUEST:
            ADCSRB |= ADC_SRB_TRIGGER_SOURCE_EXTERN_INTERRUPT_REQUEST;
        break;
        
        case ADC_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_A:
            ADCSRB |= ADC_SRB_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_A;
        break;
        
        case ADC_TRIGGER_SOURCE_COUNTER0_OVERFLOW:
            ADCSRB |= ADC_SRB_TRIGGER_SOURCE_COUNTER0_OVERFLOW;
        break;
        
        case ADC_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_B:
            ADCSRB |= ADC_SRB_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_B;
        break;
        
        case ADC_TRIGGER_SOURCE_PIN_CHANGE_INTERRUPT_REQUEST:
            ADCSRB |= ADC_SRB_TRIGGER_SOURCE_PIN_CHANGE_INTERRUPT_REQUEST;
        break;
        
        default:
            // Nothing
        break;
    }
    
    
    DIDR0 = 0x00;
    
    if(config->adc_chanel0_digital_read_enable == 0) DIDR0 |= ADC_DISABLE_DIGITAL_READ_TO_ADC0_CHANNEL;
    if(config->adc_chanel1_digital_read_enable == 0) DIDR0 |= ADC_DISABLE_DIGITAL_READ_TO_ADC1_CHANNEL;
    if(config->adc_chanel2_digital_read_enable == 0) DIDR0 |= ADC_DISABLE_DIGITAL_READ_TO_ADC2_CHANNEL;
    if(config->adc_chanel3_digital_read_enable == 0) DIDR0 |= ADC_DISABLE_DIGITAL_READ_TO_ADC3_CHANNEL;
    
    
    ADMUX = 0x00;
    
    if(config->adc_data_adjustion == ADC_DATA_ADJUSTION_LEFT) ADMUX |= ADC_ADMUX_RESULT_LEFT_ADJUSTED;
}

void adc_start_conversion(ADC_CHANEL_e adc_chanel, ADC_VOLTAGE_REFERENCE_e voltage_reference){
    
    switch(voltage_reference){
        case ADC_VOLTAGE_REFERENCE_INTERN_2V56:
            ADMUX = (ADMUX & 0b00101111) | ADC_ADMUX_VOLTAGE_REFERENCE_INTERN_2V56;
        break;
        
        case ADC_VOLTAGE_REFERENCE_INTERN_EXTERN_BYPASS_2V56:
            ADMUX = (ADMUX & 0b00101111) | ADC_ADMUX_VOLTAGE_REFERENCE_INTERN_EXTERN_BYPASS_2V56;
        break;
        
        case ADC_VOLTAGE_REFERENCE_INTERN_1V1:
            ADMUX = (ADMUX & 0b00101111) | ADC_ADMUX_VOLTAGE_REFERENCE_INTERN_1V1;
        break;
        
        case ADC_VOLTAGE_REFERENCE_EXTERN_AREF:
            ADMUX = (ADMUX & 0b00101111) | ADC_ADMUX_VOLTAGE_REFERENCE_EXTERN_AREF;
        break;
        
        case ADC_VOLTAGE_REFERENCE_VCC:
            ADMUX = (ADMUX & 0b00101111) | ADC_ADMUX_VOLTAGE_REFERENCE_VCC;
        break;
                    
        default:
            // NOTHING
        break;
    }
    
    switch(adc_chanel){
        case ADC_CHANEL0:
            ADMUX = (ADMUX & 0b11110000) | ADC_ADMUX_SELECT_CHANEL0; 
        break;
            
        case ADC_CHANEL1:
            ADMUX = (ADMUX & 0b11110000) | ADC_ADMUX_SELECT_CHANEL1;
        break;
        
        case ADC_CHANEL2:
            ADMUX = (ADMUX & 0b11110000) | ADC_ADMUX_SELECT_CHANEL2; 
        break;
            
        case ADC_CHANEL3:
            ADMUX = (ADMUX & 0b11110000) | ADC_ADMUX_SELECT_CHANEL3;
        break;
        
        case ADC_CHANEL4:
            ADMUX = (ADMUX & 0b11110000) | ADC_ADMUX_SELECT_CHANEL4;
        break;
        
        default:
            // NOTHING
        break;
    }
    
    ADCSRA |= ADC_SRA_START_CONVERSION; // works only for trigger mod
}

uint8_t adc_is_conversion_busy(void){
    return (uint8_t)((ADCSRA >> 6) & 1);
}

void adc_wait_for_conversion(void){
    do{} while(adc_is_conversion_busy());
}

uint16_t adc_data_read(uint16_t *data){
    *data = ADCL;
    *data |= ADCH << 8;
    return (*data);
}