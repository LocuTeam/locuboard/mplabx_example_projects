#include "peripherials.h"

#include <stdio.h>
#include <stdlib.h>

void pinWrite(uint8_t pin, uint8_t value){
    if(GetPinDirection(pin) == 1){
        
        WritePinValue(pin, value);
    }
}

uint8_t pinRead(uint8_t pin){
    
    uint8_t _value = 0;
    
    if(GetPinDirection(pin) == 0){
        _value = GetPin(pin);
    }
    
    return _value;
}

void pinDirection(uint8_t pin, uint8_t direction){
    switch(direction){
        
        case INPUT:
            WritePinDirection(pin, INPUT);
            WritePinValue(pin, 0);
        break;
            
        case OUTPUT:
            WritePinDirection(pin, OUTPUT);
        break;
            
        case INPUT_PULLUP:
            WritePinDirection(pin, INPUT);
            WritePinValue(pin, 1);
        break;
            
        default:
            WritePinDirection(pin, INPUT);
            WritePinValue(pin, 0);
        break;
    }
}

