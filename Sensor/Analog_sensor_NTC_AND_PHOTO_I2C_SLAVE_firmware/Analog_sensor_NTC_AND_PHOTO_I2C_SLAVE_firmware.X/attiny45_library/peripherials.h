/* 
 * File:   peripherials.h
 * Author: mirda
 *
 * Created on August 8, 2021, 5:52 PM
 */

#ifndef PERIPHERIALS_H
#define	PERIPHERIALS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    /* ************************************************************************** */
    /* Section: Included Files                                                    */
    /* ************************************************************************** */

    #include <xc.h>
    #include <stdio.h>
    #include <stdlib.h>


    #define OUTPUT 1 // Definice vstupu pro pouziti ve funkcich ovladajici periferie
    #define INPUT 0 // Definice vstupu pro pouziti ve funkcich ovladajici periferie
    #define INPUT_PULLUP 2

    /* Definitions of LOW and HIGH, false and true value */
    #define LOW 0 // Definice logicke nuly pro pouziti ve funkcich ovladajici periferie
    #define HIGH 1 // Definice logicke jednicky pro pouziti ve funkcich ovladajici periferie
    
    #define SetPinOutput(Pin) (DDRB |= 1 << Pin) // Funkce, ktera nastavy dany PIN v rozsahu 0 - 7 jako vystup
    #define SetPinInput(Pin) (DDRB &= ~(1 << Pin)) // Funkce, ktera nastavy dany PIN v rozsahu 0 - 7 jako vstup
    #define GetPinDirection(Pin) ((DDRB >> Pin) & 0b1)
    
    #define SetPin(Pin) (PORTB |= (1 << Pin)) // Funkce, ktere nastavy logickou jednicku na dany PIN v rozsahu 0 - 7
    #define ClearPin(Pin) (PORTB &= ~(1 << Pin)) // Funkce, ktere nastavy logickou nulu na dany PIN v rozsahu 0 - 7
    #define TogglePin(Pin) ((((PORTB >> Pin) & 0b1) == 1) ? (PORTB &= ~(1 << Pin)) : (PORTB |= 1 << Pin))  // Funkce, ktere nastavy opacnou hodnotu na dany PIN v rozsahu 0 - 7
    #define GetPin(Pin) ((PINB >> Pin) & 0b1) // Funkce, ktere ziska hodnotu z daneho PINu v rozsahu 0 - 7
    
    #define WritePinValue(Pin, Value) ((Value == 1) ? (PORTB |= 1 << Pin) : (PORTB &= ~(1 << Pin)))
    #define WritePinDirection(Pin, Direction) ((Direction == 1) ? (DDRB |= 1 << Pin) : (DDRB &= ~(1 << Pin)))
    
    #define WritePortValue(Value) PORTB = Value // Funkce pro nastaveni portu B
    #define GetPortValue() PINB 
    
    
    typedef enum{
        PIN_PB00 = 0,
        PIN_PB01 = 1,
        PIN_PB02 = 2,
        PIN_PB03 = 3,
        PIN_PB04 = 4,
        PIN_PB05 = 5
    } PORT_PIN; // Vytvoreni datoveho typu enum PORT_PIN, kde jsou definovany vsechny piny daneho mikrokontroleru
    
    
    
    void pinWrite(uint8_t pin, uint8_t value);
    uint8_t pinRead(uint8_t pin);
    void pinDirection(uint8_t pin, uint8_t direction);
    

#ifdef	__cplusplus
}
#endif

#endif	/* PERIPHERIALS_H */

