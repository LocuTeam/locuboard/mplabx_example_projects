/* 
 * File:   peripherials.h
 * Author: mirda
 *
 * Created on August 8, 2021, 5:52 PM
 */

#ifndef ADC_H
#define	ADC_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    /* ************************************************************************** */
    /* Section: Included Files                                                    */
    /* ************************************************************************** */

    #include <xc.h>
    #include <stdlib.h>
    
    
    #define ADC_ADMUX_VOLTAGE_REFERENCE_INTERN_2V56 0b10010000
    #define ADC_ADMUX_VOLTAGE_REFERENCE_INTERN_EXTERN_BYPASS_2V56 0b11010000
    #define ADC_ADMUX_VOLTAGE_REFERENCE_INTERN_1V1 0b10000000
    #define ADC_ADMUX_VOLTAGE_REFERENCE_EXTERN_AREF 0b01000000
    #define ADC_ADMUX_VOLTAGE_REFERENCE_VCC 0b00000000
    #define ADC_ADMUX_RESULT_LEFT_ADJUSTED 0b00100000
    #define ADC_ADMUX_RESULT_RIGHT_ADJUSTED 0b00000000
    #define ADC_ADMUX_SELECT_CHANEL0 0b00000000
    #define ADC_ADMUX_SELECT_CHANEL1 0b00000001
    #define ADC_ADMUX_SELECT_CHANEL2 0b00000010
    #define ADC_ADMUX_SELECT_CHANEL3 0b00000011
    #define ADC_ADMUX_SELECT_CHANEL4 0b00001111

    #define ADC_SRA_ENABLE 0b10000000
    #define ADC_SRA_START_CONVERSION 0b01000000
    #define ADC_SRA_ENABLE_AUTOTRIGGER 0b00100000
    #define ADC_SRA_ENABLE_INTERRUPT 0b00001000
    #define ADC_SRA_PRESCALER_2 0b00000000
    #define ADC_SRA_PRESCALER_4 0b00000010
    #define ADC_SRA_PRESCALER_8 0b00000011
    #define ADC_SRA_PRESCALER_16 0b00000100
    #define ADC_SRA_PRESCALER_32 0b00000101
    #define ADC_SRA_PRESCALER_64 0b00000110
    #define ADC_SRA_PRESCALER_128 0b00000111

    #define ADC_SRB_BIPOLAR_INPUT_MODE 0b10000000
    #define ADC_SRB_TRIGGER_SOURCE_FREE_RUNNING 0b00000000
    #define ADC_SRB_TRIGGER_SOURCE_ANALOG_COMPARATOR 0b00000001
    #define ADC_SRB_TRIGGER_SOURCE_EXTERN_INTERRUPT_REQUEST 0b00000010
    #define ADC_SRB_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_A 0b00000011
    #define ADC_SRB_TRIGGER_SOURCE_COUNTER0_OVERFLOW 0b00000100
    #define ADC_SRB_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_B 0b00000101
    #define ADC_SRB_TRIGGER_SOURCE_PIN_CHANGE_INTERRUPT_REQUEST 0b00000110

    #define ADC_DISABLE_DIGITAL_READ_TO_ADC0_CHANNEL 0b00100000
    #define ADC_DISABLE_DIGITAL_READ_TO_ADC2_CHANNEL 0b00010000
    #define ADC_DISABLE_DIGITAL_READ_TO_ADC3_CHANNEL 0b00001000
    #define ADC_DISABLE_DIGITAL_READ_TO_ADC1_CHANNEL 0b00000100

    
    typedef struct adc_config_descriptor_t adc_config_t;
    
    typedef enum{
        ADC_CHANEL0 = 0,
        ADC_CHANEL1 = 1,
        ADC_CHANEL2 = 2,
        ADC_CHANEL3 = 3,
        ADC_CHANEL4 = 4
    }ADC_CHANEL_e;

    typedef enum{
        ADC_VOLTAGE_REFERENCE_INTERN_2V56               = 0,
        ADC_VOLTAGE_REFERENCE_INTERN_EXTERN_BYPASS_2V56 = 1,
        ADC_VOLTAGE_REFERENCE_INTERN_1V1                = 2,
        ADC_VOLTAGE_REFERENCE_EXTERN_AREF               = 3,
        ADC_VOLTAGE_REFERENCE_VCC                       = 4
    }ADC_VOLTAGE_REFERENCE_e;

    typedef enum{
        ADC_TRIGGER_SOURCE_FREE_RUNNING                 = 0,
        ADC_TRIGGER_SOURCE_ANALOG_COMPARATOR            = 1,
        ADC_TRIGGER_SOURCE_EXTERN_INTERRUPT_REQUEST     = 2,
        ADC_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_A     = 3,
        ADC_TRIGGER_SOURCE_COUNTER0_OVERFLOW            = 4,
        ADC_TRIGGER_SOURCE_COUNTER0_COMPARE_MATCH_B     = 5,
        ADC_TRIGGER_SOURCE_PIN_CHANGE_INTERRUPT_REQUEST = 6
    }ADC_TRIGGER_SOURCE_e;

    typedef enum{
        ADC_PRESCALER_2   = 0,
        ADC_PRESCALER_4   = 1,
        ADC_PRESCALER_8   = 2,
        ADC_PRESCALER_16  = 3,
        ADC_PRESCALER_32  = 4,
        ADC_PRESCALER_64  = 5,
        ADC_PRESCALER_128 = 6
    }ADC_PRESCALER_e;

    typedef enum{
        ADC_INPUT_MODE_UNIPOLAR = 0,
        ADC_INPUT_MODE_BIPOLAR  = 1
    }ADC_INPUT_MODE_e;

    typedef enum{
        ADC_DATA_ADJUSTION_RIGHT = 0,
        ADC_DATA_ADJUSTION_LEFT  = 1
    }ADC_DATA_ADJUSTION_e;
    
    
    struct adc_config_descriptor_t{
        unsigned adc_enable           :1;
        unsigned adc_autotriger       :1;
        unsigned adc_start_conversion :1;
        unsigned adc_interrupt_enable :1;
        unsigned adc_prescaler        :3;
        unsigned adc_input_mode       :1;
        unsigned adc_trigger_source   :3;
        unsigned adc_data_adjustion   :1;
        
        unsigned adc_chanel0_digital_read_enable :1;
        unsigned adc_chanel1_digital_read_enable :1;
        unsigned adc_chanel2_digital_read_enable :1;
        unsigned adc_chanel3_digital_read_enable :1;
    };
    
    
    void adc_init(adc_config_t *config);
    void adc_start_conversion(ADC_CHANEL_e adc_chanel, ADC_VOLTAGE_REFERENCE_e voltage_reference);
    uint8_t adc_is_conversion_busy(void);
    void adc_wait_for_conversion(void);
    uint16_t adc_data_read(uint16_t *data);
    

#ifdef	__cplusplus
}
#endif

#endif	/* PERIPHERIALS_H */

