#define F_CPU 1000000UL

#include <xc.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <avr/cpufunc.h>

#include "attiny45_library/peripherials.h"
#include "attiny45_library/adc.h"
#include "attiny45_library/usi_i2c_slave.h"


#define I2C_SLAVE_ADDRESS 0x20
#define I2C_SLAVE_REGISTER_COUNT 7

#define NTC_NOMINAL_RESISTANCE 10000
#define NTC_NOMINAL_TEMPERATURE 25
#define NTC_BCOEFFICIENT 3950

#define EEPROM_ANALOG_SENSOR_ID_ADDRESS 0x00
#define EEPROM_ANALOG_SENSOR_CONFIG_ADDRESS 0x01



user_usi_i2c_t my_i2c;


// Tracks the current register position
volatile uint8_t reg_position;


// I2C registers
uint8_t i2c_regs[I2C_SLAVE_REGISTER_COUNT] =
{
/*0*/ 0b00000000, // Analog sensor ID
/*1*/ 0b11000000, // Analog sensor config byte  -> [2b][1b] -> [ONLY TEMP] [ALL] ->[doplnkove kodovani / prime kodovani / posunuti (temp+40) / posunuti ({temp+40}*2) ] [Little / Big endian]
/*2*/ 0b00000000, // Analog sensor status byte
/*3*/ 0b00000000, // Analog sensor NTC temperature raw register
/*4*/ 0b00000000, // Analog sensor NTC temperature register
/*5*/ 0b00000000, // Analog sensor PHOTO raw register
/*6*/ 0b00000000  // Analog sensor PHOTO intensity register
};


void usi_onRequest(void){
    usiTwiTransmitByte((uint8_t) i2c_regs[reg_position]);
}


volatile uint8_t receive_flag        = 0;
volatile int8_t receive_reg_position = -1;

void usi_onReceiver(uint8_t count){
    
    if (count < 1)
    {
        // Sanity-check
        return;
    }
    if (count > TWI_RX_BUFFER_SIZE)
    {
        receive_reg_position = -1;
        // Insane amount of input numbers
        return;
    }
    
    reg_position = usiTwiReceiveByte();
    count--;
    
    if (!count)
    {
        // This write was only to set the buffer for next read
        return;
    }
    
    receive_reg_position = reg_position;
    
    while(count--)
    {
        if(reg_position < I2C_SLAVE_REGISTER_COUNT) i2c_regs[reg_position++] = usiTwiReceiveByte();
    }
    
    receive_flag = 1;
}


uint8_t convert_analog_to_temp(uint16_t data){
    
    double temp = (33000 / ((data / 1023.0) * 1.1)) - 30000;
    
    // Steinhart's equality
    temp  = temp / NTC_NOMINAL_RESISTANCE;            // (R/Ro)
    temp  = log(temp);                                // ln(R/Ro)
    temp /= NTC_BCOEFFICIENT;                         // 1/B * ln(R/Ro)
    temp += 1.0 / (NTC_NOMINAL_TEMPERATURE + 273.15); // + (1/To)
    temp  = 1.0 / temp;                               // Invert
    temp -= 273.15;                                   // convert to C degrees
    
    uint8_t res = (temp + 40.0) * 2;
    
    if((res < 0) || (res > 240)) return 0;
        
    return res; // Value from (-40C to 80C -> 0 to 240)
}

uint8_t byte_reverse(uint8_t byte){
    byte = (byte & 0b11110000) >> 4 | (byte & 0x00001111) << 4;
    byte = (byte & 0b11001100) >> 2 | (byte & 0b00110011) << 2;
    byte = (byte & 0b10101010) >> 1 | (byte & 0b01010101) << 1;
    return byte;
}


void EEPROM_Tasks(void);
void ADC_Tasks(void);


int main(void) {
    
    usiTwiSlaveRequestCallbackRegister(&my_i2c, usi_onRequest);   // Register I2C request callback
    usiTwiSlaveReceiverCallbackRegister(&my_i2c, usi_onReceiver); // Register I2C receive callback
    
	usiTwiSlaveInit(&my_i2c, I2C_SLAVE_ADDRESS); // Init I2C slave for chousen slave address
    
    pinDirection(PIN_PB03, INPUT); // Configure PB03 pin as INPUT for ADC chanel 3
    pinDirection(PIN_PB04, INPUT); // Configure PB04 pin as INPUT for ADC chanel 2
    
    
    adc_config_t adc_config;
    
    adc_config.adc_enable           = true;
    adc_config.adc_autotriger       = false;
    adc_config.adc_start_conversion = false;
    adc_config.adc_interrupt_enable = false;
    adc_config.adc_prescaler        = ADC_PRESCALER_128;
    adc_config.adc_input_mode       = ADC_INPUT_MODE_UNIPOLAR;
    adc_config.adc_trigger_source   = ADC_TRIGGER_SOURCE_FREE_RUNNING;
    adc_config.adc_data_adjustion   = ADC_DATA_ADJUSTION_RIGHT;
    
    adc_config.adc_chanel0_digital_read_enable = true;
    adc_config.adc_chanel1_digital_read_enable = true;
    adc_config.adc_chanel2_digital_read_enable = false; // For more low power
    adc_config.adc_chanel3_digital_read_enable = false; // For more low power
    
    adc_init(&adc_config); // Configure the ADC
    
    
    eeprom_busy_wait(); // Wait till eeprom is ready
    
    _EEGET(i2c_regs[0], EEPROM_ANALOG_SENSOR_ID_ADDRESS); // Get saved data about device ID
    eeprom_busy_wait();
    
    _EEGET(i2c_regs[1], EEPROM_ANALOG_SENSOR_CONFIG_ADDRESS); // Get saved data about device configuration
    eeprom_busy_wait();
    
    
    sei(); // Enable global interrupt
    
    
    while (1){
        
        
        EEPROM_Tasks();
        
        ADC_Tasks();
        
    }
    
    return 0;
}


void EEPROM_Tasks(void){
    if(receive_flag){
        if(eeprom_is_ready()){
            switch(receive_reg_position){

                case 0:
                    _EEPUT(EEPROM_ANALOG_SENSOR_ID_ADDRESS, i2c_regs[0]); // save register data
                break;

                case 1:
                    _EEPUT(EEPROM_ANALOG_SENSOR_CONFIG_ADDRESS, i2c_regs[1]); // save register data
                break;

                default:
                    // NOTHING
                break;
            }
                
            receive_flag = 0;
        }
    }
}


void ADC_Tasks(void){
    
    uint16_t analog_data; // 10 bit adc data variable
    
    // NTC
    adc_start_conversion(ADC_CHANEL2, ADC_VOLTAGE_REFERENCE_INTERN_1V1); // Start conversion
                
    adc_wait_for_conversion(); // Wait for adc conversion is done
    
    adc_data_read(&analog_data); // read data from ADC registers
    
    uint8_t _ntc_temp = convert_analog_to_temp(analog_data); // Get NTC temp
    
    switch((i2c_regs[1] >> 6) & 0b11){
        case 0:
            _ntc_temp = (_ntc_temp / 2) - 40; // Default behavior -> underflow
        break;
            
        case 1:       
            _ntc_temp = _ntc_temp / 2;
               
            if(_ntc_temp < 40) _ntc_temp |= 0b10000000; // Insert 1 to most significant bit
            else _ntc_temp &= 0b01111111;
               
        break;
            
        case 2:
            _ntc_temp = _ntc_temp / 2; // Value from (-40C to 80C -> 0 to 120)
        break;
            
        case 3:
            // Do nothing - Value from (-40C to 80C -> 0 to 240)
        break;
            
        default:
            //Nothing
        break;
    }
    
    if((i2c_regs[1] >> 5) & 1) { // BIG endian - LSB
        i2c_regs[3] = byte_reverse((uint8_t)(analog_data >> 2)); // Reversed raw data (0 - 255)
        i2c_regs[4] = byte_reverse((uint8_t)_ntc_temp); // Reversed value from (-40C to 80C -> 0 to 240)
    }
    else{
        i2c_regs[3] = (uint8_t)(analog_data >> 2); // Raw data (0 - 255)
        i2c_regs[4] = _ntc_temp; // Value from (-40C to 80C -> 0 to 240)
    }
    

    // PHOTO
    adc_start_conversion(ADC_CHANEL3, ADC_VOLTAGE_REFERENCE_INTERN_1V1); // Start conversion
    
    adc_wait_for_conversion(); // Wait for adc conversion is done
    
    adc_data_read(&analog_data); // read data from ADC registers
    
    if((i2c_regs[1] >> 5) & 1){ // BIG endian - LSB
        i2c_regs[5] = byte_reverse((uint8_t)(analog_data >> 2));    // Raw data (0 - 255)
        i2c_regs[6] = byte_reverse((analog_data / 1023.0) * 100.0); // Intensity (0 - 100%)
    }
    else{
        i2c_regs[5] = (uint8_t)(analog_data >> 2);    // Raw data (0 - 255)
        i2c_regs[6] = (analog_data / 1023.0) * 100.0; // Intensity (0 - 100%)
    }
    
}