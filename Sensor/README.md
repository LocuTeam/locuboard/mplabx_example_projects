CZ / [EN](README_EN.md)

# Příklady projektů senzorů

Zde jsou jednoduché projekty, které implementují sběrnice pro komunikaci s moduly, externí přerušení, ADC periferii anebo se používají periferiní funkce pro ovládání senzorů. V projektech se většinou objevuje knihovna daného senzoru, která usnadňuje používání. Pro více informací se podívejte do jednotlivých příkladů projektů.


## Analog_sensor_NTC_AND_PHOTO_ADC_example

Příklad projektu desky LocuBoard pro analogový senzor. V tomto příkladu projektu se používá přímé čtení analogové hodnoty pomocí ADC periferie vývojové desky LocuBoard. Uživatelský program periodicky čte analogová data z analogového senzoru a vypisuje tato data na terminál. Více informací najdete [zde](Analog_sensor_NTC_AND_PHOTO_ADC_example).


## Analog_sensor_NTC_AND_PHOTO_I2C_example

Příklad projektu desky LocuBoard pro analogový senzor. V tomto příkladu projektu se používá čtení skrze I2C sběrnici z analogového modulu. Analogový modul má na sobě implementovaný Attiny45 mikrokontroler, který čte analogové hodnoty a ukládá je do virtuálníh I2C registrů, které můžeme díky I2C sběrnici číst. Uživatelský program periodicky čte přes I2C sběrnici data a vypisuje tato data na terminál. Více informací najdete [zde](Analog_sensor_NTC_AND_PHOTO_I2C_example).


## Analog_sensor_NTC_AND_PHOTO_I2C_SLAVE_firmware

**Upozornění: Nejde o harmony v3 projekt.**
Firmware analogového modulu pro mikrokontroler Attiny45. Uživatelský program periodicky čte analogová data a ukládá je do virtuálních I2C registrů. Tento firmware umožňuje přistupovat k analogovým datům sezoru skrze I2C sběrnici. Virtuální registry obsahují ID zařízení, konfigurační registr a také registry dat. Více informací najdete [zde](Analog_sensor_NTC_AND_PHOTO_I2C_SLAVE_firmware).


## MCP9844_example

Příklad projektu desky LocuBoard pro teplotní I2C senzor MCP9844. Uživatelský program periodicky čte hodnotu teploty a vypisuje ji na terminál. Více informací najdete [zde](MCP9844_example).