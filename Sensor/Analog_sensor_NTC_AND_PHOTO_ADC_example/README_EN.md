EN / [CZ](README.md)

# Analog_sensor_NTC_AND_PHOTO_ADC_example

You are at the project folder designated for reading analog data using AD convertor from analog module. User code reads periodicaly analog value and prints that value to terminal. User code can be found [here](firmware/src/main.c)


## Project settings

Picture bellow describes layout of each component used in project.   
<img src="img/project_config.png" width="500">

<br>

### System component settings

Here allow SysTick and enable interrupt for delay function usage in project.   
<img src="img/system_config.png" width="500">

<br>

### SERCOM4 set as EDBG USART settings

Here we choose USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 9600 bauds, parity set to No Parity, Character size set to 8 bit width, Stop bit set to One Stop Bit.

The most important part are the Receive Pinout and the Transmit Pinout, these two we set to pin on development board where we want to send our data or where we want to read them. Notice there, that we have no pins to choose here, only pads, so we look for the datasheet or we look for the harmony v3 Pin Table, where the pins show their pads.   
<img src="img/sercom4_edbg_usart_config.png" width="80%">

<br>

### Stdio component settings

Choose option buffered mode.   
<img src="img/stdio_config.png" width="60%">

<br>

### ADC component settings

Clock source for ADC we will set in next settings in Clock configuraton, now it is important for us set so-called prescaler, that divide clock signal. It is important to set sample lenght (how manny cycles has to be done for measurement -> more, better results). Next choose Reference as Internal Bandgap (typicaly 1.1V), Conversion Trigger choose as SW trigger, disable Differetial mode and choose any input (AIN14 Pin), later it will be changed in user code. Choose 12-bit conversion mode and enable Ready interrupt.   
<img src="img/adc_config.png" width="80%">

#### Graphic ADC settings:
<img src="img/adc_graphic_config.png" width="100%">

<br>

### TC0 set as Timer component settings

Here choose 16-bit counter mode, Prescaler sets to 1024, operation mode set to Timer. Next Set the timer period unit as milisecond, time set as 500ms and enable Timer period interrupt.   
<img src="img/tc0_config.png" width="70%">


### Clock settings

Againts default settings disable DFLL and enable 16 MHz intern oscilator where set division to 4. Next choose in GCLK generator 0 as clock source OSC16M and let division on 1. Main clock let be as default, but in Backup set division to 4.   
Next set clock signal designated for AD converor and other components. For that enable GCLK generator 1 and as clock source choose OSCULP32K (intern 32kHz low power crystal).
<img src="img/clock_config.png" width="100%">

<br>
In Peripheral Clock selection part set Peripheral clock configuration to following clock inputs:   
<img src="img/clock_adc_config.png" width="100%">
<img src="img/clock_eic_config.png" width="100%">
<img src="img/clock_sercom4_config.png" width="100%">
<img src="img/clock_tc0_config.png" width="100%">


<br>

### EIC component settings

First choose as clock input Clocked by ULP32K. The next choose chanel 6, 7 and 15. These channels correspond to button pins. In every chanel choose option Enable Interrupt, next choose Edge detection is clock asynchronously operated, next choose Falling edge detection and at the end choose Enable filter.   
<img src="img/eic_config.png" width="80%">

<br>

### Pin settings

Pin settings for USART bus communication:  
<img src="img/pin_edbg_usart_config.png" width="100%">

Pin settings for AD conversion:  
<img src="img/pin_adc_header1_config.png" width="100%">
<img src="img/pin_adc_header2_config.png" width="100%">

Pin settings for led diods:  
<img src="img/pin_led_config.png" width="100%">

Pin settings for buttons:  
<img src="img/pin_button_config.png" width="100%">

Power pin settings:  
<img src="img/pin_power_5v_enable_config.png" width="100%">