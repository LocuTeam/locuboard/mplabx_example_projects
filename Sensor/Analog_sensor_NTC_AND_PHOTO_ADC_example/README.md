CZ / [EN](README_EN.md)

# Analog_sensor_NTC_AND_PHOTO_ADC_example

Nacházíš se ve složce projektu určeného pro čtení analogových hodnot pomocí AD převodníku z analogového modulu. Uživatelský program periodicky čte  Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">

<br>

### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">

<br>

### Nastavení komponenty sercom4 jako EDBG USART

Zde vybereme možnost USART with Internal Clock, dále blocking mód, USART frame with parity, baudrate nastavíme na 19200 bauds, paritu na No Parity, Character size nastavíme na 8 bitovou velikost dat, Stop bit nastavíme na One Stop Bit.

Nejdůležitější částí je Receive Pinout a Transmit Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, což nám teď momentálně nic neříká, a proto je dobré se buď podívat do datasheetu, který pad odpovídá jakému pinu a nebo se podívat do harmony v3 Pin Table.

<img src="img/sercom4_edbg_usart_config.png" width="80%">

<br>

### Nastavení komponenty stdio

Zde vybereme možnost buffered mód.

<img src="img/stdio_config.png" width="60%">

<br>

### Nastavení komponenty ADC

Clock source pro ADC budeme nastavovat dále v Clock configuraton, teď je pro nás duležité nastavit tzv. prescaler, který vydělí hodinový signál. Je potřeba nastavit smaple lenght (kolik cyklů se má provést meření -> čím víc, tím přesnější). Zvolíme dále Referenci jako Internal Bandgap (typicky 1.1V), Conversion Trigger zvolíme jako SW trigger, vypneme Differetial mode a vybereme nějaký vstup (AIN14 Pin), později budeme nastavovat v uživatelském kódu. Vybereme 12 bitový převod a zapneme Ready interrupt.   
<img src="img/adc_config.png" width="80%">

#### Grafické nastavení:
<img src="img/adc_graphic_config.png" width="100%">

<br>

### Nastavení komponenty tc0 jako timer

Zde vybereme 32-bit counter mód, Prescaler na 1024 a mód nastavíme na Timer.  
<img src="img/tc0_config.png" width="70%">


### Nastavení hodin

Oproti výchozímu nastavení vypneme DFLL a zapneme 16 MHz interní oscilátor kde nastavíme division 4. Dále vybereme v GCLK generátoru 0 jako zdroj hodin OSC16M a necháme division na 1. Main clock necháme na výchozí hodnotě a v Backupu nastavíme division na 4.   
Dále nastavíme hodinový signál určený pro AD převodník a jiné komponenty tím, že povolíme GCLK generátor 1 a jako vstup hodinového signálu vybereme OSCULP32K (interní 32kHz low power krystal).
<img src="img/clock_config.png" width="100%">

<br>
V části Peripheral clock selection nastavíme Peripheral clock configuration na následující hodinové vstupy:   
<img src="img/clock_adc_config.png" width="100%">
<img src="img/clock_eic_config.png" width="100%">
<img src="img/clock_sercom4_config.png" width="100%">
<img src="img/clock_tc0_config.png" width="100%">


<br>

### Nastavení komponenty EIC

Nastavíme Clock source na ULP32K, dále edge detection nastavíme na asynchronous (nepotřebujeme čekat na synchronizaci -> potřeba pro vícevláknovou aplikaci), nastavíme Edge selection na Falling edge (sestupná hrana) a v poslední řadě zapneme Enable filter.   
<img src="img/eic_config.png" width="80%">

<br>

### Nastavení pinů

Nastavení pinů pro komunikaci na USART sběrnici:  
<img src="img/pin_edbg_usart_config.png" width="100%">

Nastavení pinů pro AD převodník:  
<img src="img/pin_adc_header1_config.png" width="100%">
<img src="img/pin_adc_header2_config.png" width="100%">

Nastavení pinů pro led diody:  
<img src="img/pin_led_config.png" width="100%">

Nastavení pinů pro tlačítka:  
<img src="img/pin_button_config.png" width="100%">

Nastavení power pinů:  
<img src="img/pin_power_5v_enable_config.png" width="100%">