/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

#define LOCUBOARD_ADC_CHANNEL_AN1 ADC_POSINPUT_AIN12
#define LOCUBOARD_ADC_CHANNEL_AN2 ADC_POSINPUT_AIN13
#define LOCUBOARD_ADC_CHANNEL_AN3 ADC_POSINPUT_AIN14
#define LOCUBOARD_ADC_CHANNEL_AN4 ADC_POSINPUT_AIN15


typedef enum{
    ANALOG_SENSOR_STATE_NTC_READ    = 0,
    ANALOG_SENSOR_STATE_NTC_PRINT   = 1,
    ANALOG_SENSOR_STATE_PHOTO_READ  = 2,
    ANALOG_SENSOR_STATE_PHOTO_PRINT = 3
}ANALOG_SENSOR_STATE_e;

ANALOG_SENSOR_STATE_e analog_sensor_state = ANALOG_SENSOR_STATE_NTC_READ;


volatile uint8_t adc_flag = 0;

void adc_callback(ADC_STATUS status, uintptr_t context){
    adc_flag = 1;
}


volatile uint8_t timer_flag = 0; // Timer flag

void timer_callback(TC_TIMER_STATUS status, uintptr_t context){
    timer_flag = 1; // Set timer_flag
}


int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)

    
    TC0_TimerCallbackRegister(timer_callback, (uintptr_t) NULL); // Register timer callback handler
    TC0_TimerStart(); // Start timer
    
    
    ADC_Enable();
    
    ADC_CallbackRegister(adc_callback, (uintptr_t)NULL);
    
    //ADC_ChannelSelect(LOCUBOARD_ADC_CHANNEL_AN2, ADC_NEGINPUT_GND);

    
    while ( true ){
        
        SYS_Tasks ( ); // Maintain state machines of all polled MPLAB Harmony modules.
        
        if(timer_flag){
            
            switch(analog_sensor_state){
                case ANALOG_SENSOR_STATE_NTC_READ:
                    
                    ADC_ChannelSelect(LOCUBOARD_ADC_CHANNEL_AN2, ADC_NEGINPUT_GND);
                    ADC_ConversionStart();
                    
                    analog_sensor_state = ANALOG_SENSOR_STATE_NTC_PRINT;
                break;
                
                case ANALOG_SENSOR_STATE_NTC_PRINT:
                    
                    if(adc_flag){
            
                        printf("ADC NTC value is: %d%%.\r\n", (uint8_t)((ADC_ConversionResultGet() / 4096.0) * 100));
                        
                        analog_sensor_state = ANALOG_SENSOR_STATE_PHOTO_READ;
                        
                        adc_flag = 0;
                    }
                    
                break;
                
                case ANALOG_SENSOR_STATE_PHOTO_READ:
                    
                    ADC_ChannelSelect(LOCUBOARD_ADC_CHANNEL_AN1, ADC_NEGINPUT_GND);
                    ADC_ConversionStart();
                    
                    analog_sensor_state = ANALOG_SENSOR_STATE_PHOTO_PRINT;
                break;
                
                case ANALOG_SENSOR_STATE_PHOTO_PRINT:
                    
                    if(adc_flag){
            
                        printf("ADC PHOTO value is: %d%%.\r\n", (uint8_t)((ADC_ConversionResultGet() / 4096.0) * 100));
                        
                        analog_sensor_state = ANALOG_SENSOR_STATE_NTC_READ;
                        
                        timer_flag = 0; // Clear timer flag
                        
                        adc_flag = 0;
                    }
                    
                break;
            }

        }        
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

