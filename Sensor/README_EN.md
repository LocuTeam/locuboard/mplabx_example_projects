EN / [CZ](README.md)

# Sensor project examples

Here you will find basic projects that implements basic buses for sensor communication, external interupt, ADC periphery or there is implemented peripherial functions for sensor control. In projects there is usually a library/driver of sensor that makes easy usage of sensor. For more informations look to each sensor projects.


## Analog_sensor_NTC_AND_PHOTO_ADC_example

LocuBoard project example for analog sensor. In this example is used direct reading analog value using ADC periphery. User code periodicaly reads analog data from analog sensor and prints data to terminal. For more informations click [here](Analog_sensor_NTC_AND_PHOTO_ADC_example).


## Analog_sensor_NTC_AND_PHOTO_I2C_example

LocuBoard project example for analog sensor. In this project example is used direct reading through I2C bus from analog sensor. Analog module has implemented Attiny45 microcontroller that reads analog values and saves them to virtual I2C registers. User code periodicaly reads data through I2C bus and prints them to terminal. For more informations click [here](Analog_sensor_NTC_AND_PHOTO_I2C_example).


## Analog_sensor_NTC_AND_PHOTO_I2C_SLAVE_firmware

**Notice: It is not a harmony v3 project.**
Firmware of analog sensor for Attiny45 microcontroller. User code periodicaly reads analog data and saves them to virtual I2C registers. This firmware allow to access to analog data of sensor through I2C bus. Virtual registers contain ID of the device, config registr and also data registers. For more informations click [here](Analog_sensor_NTC_AND_PHOTO_I2C_SLAVE_firmware).


## MCP9844_example

PLocuBoard project example for temperature I2C sensor MCP9844. User code peridicaly reads temperature value and prints it to terminal. For more informations click [here](MCP9844_example).