EN / [CZ](README.md)

# [Timer controller project examples](/TCC) -> TCC_PWM_AND_LED_example

Your are at TCC_PWM_AND_LED_example project. This project shows how the TCC periphery can be used for PWM signal generation. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### TCC component settings

First choose prescaller Divided by 16, next set operating mode to PWM, next set PWM type as NPWM, next set Period Value to 1024, turn on period interrupt and output matrix let default.

<img src="img/tcc_config.png" width="500">


### Pin settings

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">