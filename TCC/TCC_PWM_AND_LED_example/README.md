CZ / [EN](README_EN.md)

# [Časovač kontroler příklady projektů](/TCC) -> TCC_PWM_AND_LED_example

Nacházíš se ve složce projektu TCC_PWM_AND_LED_example. Tento projekt ukazuje, jak se dá použít TCC pro generování PWM signálu. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c).


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Obrázek níže popisuje jak je nastavená komponenta system.

<img src="img/system_config.png" width="500">


### Nastavení komponenty TCC

Zaprvé vybereme prescaler Divided by 16, dále nastavíme operationg mód na PWM, dále nastavíme PWM type jako NPWM, dále nastavíme Period Value na 1024, zapneme period interrupt a output matrix necháme výchozí.

<img src="img/tcc_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro tlačítka:

<img src="img/pin_button_config.png" width="800">