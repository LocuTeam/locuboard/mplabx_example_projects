EN / [CZ](README.md)

# Timer controller project examples

Here you can find project examples that use timer for PWM, timer, compare a capture mode.


## TCC_PWM_AND_LED_example

Easy project, that uses timer for pulse width modulation. User LED 1 is set to output of PWM signal that can be controlled by duty cycle. More informations about project you can find [here](TCC_PWM_AND_LED_example).