CZ / [EN](README_EN.md)

# Časovač kontroler příklady projektů

Zde se nacházejí projekty používající časovač pro PWM, timer, compare a capture mód.


## TCC_PWM_AND_LED_example

Jednoduchý projekt, který využívá timeru vytvoření pulzně šířkové modulace. User LED 1 je nastavena na výstup PWM signálu, který můžeme ovládat duty cyclem. Více informací o projektu naleznete [zde](TCC_PWM_AND_LED_example).