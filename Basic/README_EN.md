EN / [CZ](README.md)

# Basic projects

Here you can find basic projects, that use basic periphery.


## LED_AND_DELAY_example

Easy project, that control pins of microcontroller (writes logical one or zero to pins). The next is in project used system timer (SYSTICK), that is used for delay function. User program toggle LED in intervals. More informations about project you can find [here](LED_AND_DELAY_example).


## LED_AND_BUTTON_example

Easy projetc, that control pins of microcontroller (writes logical one or zero to pins) and read out logical value from pins. User program reads out logical values on buttons and write the logical value to the LEDs due to buttons. More informations about project you can find [here](LED_AND_BUTTON_example).