CZ / [EN](README_EN.md)

# Základní projekty

Zde se nacházejí jednoduché projekty se základními periferiemi.


## LED_AND_DELAY_example

Jednoduchý projekt, který ovládá piny mikrokontroleru (zapisuje logickou jedničku anebo logickou nulu na piny). Dále se v projektu používá systémový časovač (SYSTICK), který se používá kvůli funkci delay. Uživatelský program periodicky rozsvěcuje led diodu připojenou na pin mikrokontroleru. Více informací o projektu naleznete [zde](LED_AND_DELAY_example).


## LED_AND_BUTTON_example

Jednoduchý projekt, který ovládá piny mikrokontroleru (zapisuje logickou jedničku anebo logickou nulu na piny) a z jednotlivých pinů čte logickou hodnotu podle napětí na daném pinu. Uživatelský program periodicky čte z tlačítek logickou hodnotu a rozsvěcuje LED diody podle stisknutí tlačítek. Více informací o projektu naleznete [zde](LED_AND_BUTTON_example).