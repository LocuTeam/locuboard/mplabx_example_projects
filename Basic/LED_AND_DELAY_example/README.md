CZ / [EN](README_EN.md)

# [Basic projekty](/Basic) -> LED_AND_DELAY_example

Nacházíš se ve složce projektu LED_AND_DELAY_example. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">