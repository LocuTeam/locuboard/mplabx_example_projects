EN / [CZ](README.md)

# [Basic section](/Basic) -> LED_AND_DELAY_example

Your are at LED_AND_DELAY_example project. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### Pin settings

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">