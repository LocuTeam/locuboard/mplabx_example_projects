EN / [CZ](README.md)

# [Basic section](/Basic) -> LED_AND_BUTTON_example

Your are at LED_AND_BUTTON_example project. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Picture below describes how the system component is setup.

<img src="img/system_config.png" width="500">


### Pin settings

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">