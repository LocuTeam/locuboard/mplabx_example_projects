CZ / [EN](README_EN.md)

# [Basic projekty](/Basic) -> LED_AND_BUTTON_example

Nacházíš se ve složce projektu LED_AND_BUTTON_example. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Obrázek níže popisuje jak je nastavená komponenta system.

<img src="img/system_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro tlačítka:

<img src="img/pin_button_config.png" width="800">