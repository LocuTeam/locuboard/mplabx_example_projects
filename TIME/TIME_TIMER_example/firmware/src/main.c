/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


void timer_led1_callback(uintptr_t context){
    uint8_t *pointer = (uint8_t *) context;
    *pointer = 1;
}

void timer_led2_callback(uintptr_t context){
    uint8_t *pointer = (uint8_t *) context;
    *pointer = 1;
}

void timer_led3_callback(uintptr_t context){
    uint8_t *pointer = (uint8_t *) context;
    *pointer = 1;
}

void timer_led4_callback(uintptr_t context){
    uint8_t *pointer = (uint8_t *) context;
    *pointer = 1;
}


int main(void){
    
    SYS_Initialize(NULL); // Initialize all modules
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    
    volatile uint8_t timer_led1_flag = 0;
    
    SYS_TIME_HANDLE timer_led1_handle = SYS_TIME_TimerCreate(0, SYS_TIME_MSToCount(4000), &timer_led1_callback, (uintptr_t) &timer_led1_flag, SYS_TIME_PERIODIC);
    
    if(timer_led1_handle == SYS_TIME_HANDLE_INVALID){
        printf("SYS_TIME_TimerCreate for timer_led1_handle ERROR\r\n");
        // Error handle
    }
    
    
    volatile uint8_t timer_led2_flag = 0;
    
    SYS_TIME_HANDLE timer_led2_handle = SYS_TIME_TimerCreate(0, SYS_TIME_MSToCount(2000), &timer_led2_callback, (uintptr_t) &timer_led2_flag, SYS_TIME_PERIODIC);
    
    if(timer_led2_handle == SYS_TIME_HANDLE_INVALID){
        printf("SYS_TIME_TimerCreate for timer_led2_handle ERROR\r\n");
        // Error handle
    }
    
            
    volatile uint8_t timer_led3_flag = 0;
    
    SYS_TIME_HANDLE timer_led3_handle = SYS_TIME_TimerCreate(0, SYS_TIME_MSToCount(1000), &timer_led3_callback, (uintptr_t) &timer_led3_flag, SYS_TIME_PERIODIC);
    
    if(timer_led3_handle == SYS_TIME_HANDLE_INVALID){
        printf("SYS_TIME_TimerCreate for timer_led3_handle ERROR\r\n");
        // Error handle
    }
    
    
    volatile uint8_t timer_led4_flag = 0;
    
    SYS_TIME_HANDLE timer_led4_handle = SYS_TIME_TimerCreate(0, SYS_TIME_MSToCount(500), &timer_led4_callback, (uintptr_t) &timer_led4_flag, SYS_TIME_PERIODIC);
    
    if(timer_led4_handle == SYS_TIME_HANDLE_INVALID){
        printf("SYS_TIME_TimerCreate for timer_led4_handle ERROR\r\n");
        // Error handle
    }
    
    
    SYS_TIME_TimerStart(timer_led1_handle);
    SYS_TIME_TimerStart(timer_led2_handle);
    SYS_TIME_TimerStart(timer_led3_handle);
    SYS_TIME_TimerStart(timer_led4_handle);
    
    
    printf("LocuBoard is ready.\r\n");
    
    
    while(true){
        
        SYS_Tasks(); // Maintain state machines of all polled MPLAB Harmony modules.
        
        if(timer_led1_flag){
            timer_led1_flag = 0;
            USER_LED1_Toggle();
        }
        
        if(timer_led2_flag){
            timer_led2_flag = 0;
            USER_LED2_Toggle();
        }
        
        if(timer_led3_flag){
            timer_led3_flag = 0;
            USER_LED3_Toggle();
        }
        
        if(timer_led4_flag){
            timer_led4_flag = 0;
            USER_LED4_Toggle();
        }
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

