/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


//#define PULLED_DELAY // Uncomment to get pulled delay


#ifndef PULLED_DELAY
    typedef enum{
        PROGRAM_STATE_INIT = 0,
        PROGRAM_STATE_RUN  = 1,
        PROGRAM_STATE_WAIT = 2
    } PROGRAM_STATE_e;
#endif


int main(void){
    
    SYS_Initialize(NULL); // Initialize all modules
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    
    #ifndef PULLED_DELAY
        
        SYS_TIME_HANDLE timer_delay = SYS_TIME_HANDLE_INVALID;
    
        PROGRAM_STATE_e program_state = PROGRAM_STATE_INIT;
        
    #endif
    
    
    printf("LocuBoard is ready.\r\n");
    
    
    while(true){
        
        SYS_Tasks(); // Maintain state machines of all polled MPLAB Harmony modules.
        
        
        #ifdef PULLED_DELAY

            SYS_TIME_HANDLE timer = SYS_TIME_HANDLE_INVALID;

            if (SYS_TIME_DelayMS(500, &timer) != SYS_TIME_SUCCESS){
                // Handle error
            }
            else if(SYS_TIME_DelayIsComplete(timer) != true){
                while (SYS_TIME_DelayIsComplete(timer) == false); // Wait till the delay has not expired
            }

            USER_LED1_Toggle();
            
        #else
            
            switch(program_state){
                case PROGRAM_STATE_INIT:
                    
                    program_state = PROGRAM_STATE_RUN;
                break;
                
                case PROGRAM_STATE_RUN:
                    
                    USER_LED1_Toggle();
                    
                    if (SYS_TIME_DelayMS(250, &timer_delay) == SYS_TIME_SUCCESS){
                        program_state = PROGRAM_STATE_WAIT;
                    }
                    else{
                        // Handle error
                    }
                    
                break;
                
                case PROGRAM_STATE_WAIT:
                    
                    if(SYS_TIME_DelayIsComplete(timer_delay) == true){
                        program_state = PROGRAM_STATE_RUN;
                    }
                    
                break;
                
                default:
                    // Nothing
                break;
            }
            
        #endif

    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/
