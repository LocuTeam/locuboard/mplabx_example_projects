/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes



uint8_t message[13] = "I love IoT\r\n";

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    
    SYSTICK_TimerStart(); // Start system timer for delay function
    
    
    DRV_HANDLE usart_handle; // USART handle, holds information about usart
    
    usart_handle = DRV_USART_Open(DRV_USART_INDEX_0, DRV_IO_INTENT_EXCLUSIVE); // Open (get usart handle) USART for begin communication
    if (usart_handle == DRV_HANDLE_INVALID){ // Check if the handle is valid
        // Error handling here
    }
    
    DRV_USART_SERIAL_SETUP usart_setup; // Create structure that holds usart config information
    
    usart_setup.baudRate  = 19200;                 // Baudrate set to 19200 bauds
    usart_setup.dataWidth = DRV_USART_DATA_8_BIT;  // Data width set to 8 bit
    usart_setup.parity    = DRV_USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = DRV_USART_STOP_1_BIT;  // Stop bits set to 1 bit

    DRV_USART_SerialSetup(usart_handle, &usart_setup); // Apply configuration

    
    while ( true )
    {
        
        if (DRV_USART_WriteBuffer(usart_handle, message, 12) == false) // Print message (polled method)
        {
            // Error handling here
        }
        
        SYSTICK_DelayMs(500); // Delay 500 ms
        
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

