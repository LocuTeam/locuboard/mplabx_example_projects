#include <stddef.h>      // Defines NULL
#include <stdbool.h>     // Defines true
#include <stdlib.h>      // Defines EXIT_FAILURE
#include "definitions.h" // SYS function prototypes

#include "config/default/ltdrivers/ring_buffer/ring_buffer.h"


ring_buffer_t edbg_ring_buffer;
uint8_t edbg_received_char;

void edbg_rx_callback(uintptr_t context){
    ring_buffer_put(&edbg_ring_buffer, edbg_received_char); // Put received data
    SERCOM4_USART_Read(&edbg_received_char, 1);             // Begin reading next char
}


ring_buffer_t m2_ring_buffer;
uint8_t m2_received_char;

void m2_rx_callback(uintptr_t context){
    ring_buffer_put(&m2_ring_buffer, m2_received_char); // Put received data
    SERCOM0_USART_Read(&m2_received_char, 1);           // Begin reading next char
}


int main(void){
    
    SYS_Initialize(NULL); // Initialize all modules
    
    
    uint8_t edbg_data[128]; // Ring buffer "local" in main variable for data
    ring_buffer_init(&edbg_ring_buffer, edbg_data, 128); // Init ring_buffer
    
    USART_SERIAL_SETUP edbg_usart_setup; // Variable for storing setup information for usart
    
    edbg_usart_setup.baudRate  = 9600;              // Baudrate set to 9600 bauds
    edbg_usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    edbg_usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    edbg_usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&edbg_usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    SERCOM4_USART_ReadCallbackRegister(edbg_rx_callback, (uintptr_t) NULL);
    
    SERCOM4_USART_TransmitterEnable();
    SERCOM4_USART_ReceiverEnable();
    
    SERCOM4_USART_Read(&edbg_received_char, 1);
    
    
    
    uint8_t m2_data[128]; // Ring buffer "local" in main variable for data
    ring_buffer_init(&m2_ring_buffer, m2_data, 128); // Init ring_buffer
    
    USART_SERIAL_SETUP m2_usart_setup;            // Variable for storing setup information for usart
    
    m2_usart_setup.baudRate  = 19200;             // Baudrate set to 19200 bauds
    m2_usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    m2_usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    m2_usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM0_USART_SerialSetup(&m2_usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    SERCOM0_USART_ReadCallbackRegister(m2_rx_callback, (uintptr_t) NULL);
    
    SERCOM0_USART_Read(&m2_received_char, 1);
    
    
    POWER_M2_ENABLE_Set(); // Turn on M2 card
    
    
    while(true){
        
        SYS_Tasks(); // Maintain state machines of all polled MPLAB Harmony modules.
        
        
        if(ring_buffer_get_awaiting_count(&edbg_ring_buffer) > 0){
            uint8_t c = ring_buffer_get_value(&edbg_ring_buffer);
            SERCOM0_USART_Write(&c, 1);
            while(SERCOM0_USART_WriteIsBusy());
        }
        
        if(ring_buffer_get_awaiting_count(&m2_ring_buffer) > 0){
            uint8_t c = ring_buffer_get_value(&m2_ring_buffer);
            SERCOM4_USART_Write(&c, 1);
            while(SERCOM4_USART_WriteIsBusy());
        }
        
        
        if(USER_M2_GPIO1_Get()) USER_LED1_Set();
        else USER_LED1_Clear();
        
        if(USER_M2_GPIO2_Get()) USER_LED2_Set();
        else USER_LED2_Clear();
        
        if(USER_M2_GPIO3_Get()) USER_LED3_Set();
        else USER_LED3_Clear();
        
        if(USER_M2_GPIO4_Get()) USER_LED4_Set();
        else USER_LED4_Clear();
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

