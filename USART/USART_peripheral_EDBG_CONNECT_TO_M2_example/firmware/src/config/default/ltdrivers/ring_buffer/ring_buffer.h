/*
 * ring_buffer library � 2022 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ring_buffer.h

  @Version
    01.01

  @Summary
    Library of ring buffer for receiving messages and so on.
*/


#ifndef _RING_BUFFER_H    /* Guard against multiple inclusion */
#define _RING_BUFFER_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>



/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */

    const char GLOBAL_RING_BUFFER_VERSION[] = "01.01";



    /* ************************************************************************** */
    /* Section: Data Types                                                        */
    /* ************************************************************************** */

    typedef struct ring_buffer_descriptor_t ring_buffer_t;

    struct ring_buffer_descriptor_t{
        uint8_t *buffer;

        uint16_t capacity;
        
        // volatile need to be here cause of callback
        volatile uint16_t count; 
        volatile uint16_t write_index;
        volatile uint16_t read_index;
    };



    /* ************************************************************************** */
    /* Section: Function Prototypes                                               */
    /* ************************************************************************** */

    static inline void ring_buffer_init(ring_buffer_t *me, uint8_t *data_buff, const uint16_t capacity){
        me->buffer      = data_buff;
        me->capacity    = capacity;
        me->count       = 0;
        me->write_index = 0;
        me->read_index  = 0;
    }

    static inline uint8_t ring_buffer_get_awaiting_count(const ring_buffer_t *me){
        return me->count;
    }

    static inline uint8_t ring_buffer_is_empty(const ring_buffer_t *me){
        return ring_buffer_get_awaiting_count(me) == 0;
    }

    static inline uint8_t ring_buffer_put(ring_buffer_t *me, uint8_t data){
        if(me->count >= me->capacity) return 0;
        me->buffer[me->write_index] = data;
        ++(me->write_index);
        me->write_index = (me->write_index >= me->capacity) ? 0 : me->write_index;
        ++(me->count);
        return 1;
    }

    static inline uint8_t ring_buffer_get_value(ring_buffer_t *me){
        if(ring_buffer_is_empty(me)) return 0;
        uint8_t data = me->buffer[me->read_index];
        ++(me->read_index);
        me->read_index = (me->read_index >= me->capacity) ? 0 : me->read_index;
        --(me->count);
        return data;
    }

    static inline uint8_t ring_buffer_get_value_p(ring_buffer_t *me, uint8_t *data){
        if(ring_buffer_is_empty(me)) return 0;
        *data = me->buffer[me->read_index];
        ++(me->read_index);
        me->read_index = (me->read_index >= me->capacity) ? 0 : me->read_index;
        --(me->count);
        return 1;
    }

    static inline uint8_t ring_buffer_get_data(ring_buffer_t *me, uint8_t *buffer, const uint16_t count){
        uint16_t _cnt = 0;
        while(!ring_buffer_is_empty(me) && _cnt < count){
          buffer[_cnt] = ring_buffer_get_value(me);
          ++_cnt;
        }
        return _cnt;
    }

    static inline const char *ring_buffer_get_version(void){
        return GLOBAL_RING_BUFFER_VERSION;
    }


/* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _RING_BUFFER_H */

