/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );

    
    SERCOM2_USART_ReceiverEnable();   // Disable receiver 
    SERCOM2_USART_TransmitterEnable(); // Enable transmitter
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 19200;             // Baudrate set to 19200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM2_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    SERCOM2_USART_Write((uint8_t *)"LocuBoard is ready.\r\n", 21); // Print start message

    
    while ( true )
    {
        
        SERCOM2_USART_Write((uint8_t *)"Insert four chars: ", 19); // Print out asking message
        while(SERCOM2_USART_WriteIsBusy()); // Wait 'till the end of the transmission
        
        uint8_t buff[4] = {}; // Array for received data
        
        SERCOM2_USART_Read(buff, 4);       // Get 4 bytes data
        while(SERCOM2_USART_ReadIsBusy()); // Wait 'till four bytes are not get
        
        SERCOM2_USART_Write((uint8_t *)"Four inserted chars was: ", 25); // Print out message
        while(SERCOM2_USART_WriteIsBusy()); // Wait 'till the end of the transmission
        
        SERCOM2_USART_Write(buff, 4);       // Print out received data
        while(SERCOM2_USART_WriteIsBusy()); // Wait 'till the end of the transmission
        
        SERCOM2_USART_Write((uint8_t *)"\r\n", 2); // Print out courier return and line feed
        while(SERCOM2_USART_WriteIsBusy());        // Wait 'till the end of the transmission
        
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

