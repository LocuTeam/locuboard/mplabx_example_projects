EN / [CZ](README.md)

# [USART section](/USART) -> USART_peripheral_CLICK_example

Your are at USART_peripheral_CLICK_example project. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### Sercom2 component settings

Here we choose USART with Internal Clock, next Non-blocking mode, USART frame with parity, baudrate set to 19200 bauds, parity set to No Parity, Character size set to 8 bit width, Stop bit set to One Stop Bit.

The most important part are the Receive Pinout and the Transmit Pinout, these two we set to pin on development board where we want to send our data or where we want to read them. Notice there, that we have no pins to choose here, only pads, so we look for the datasheet or we look for the harmony v3 Pin Table, where the pins show their pads.

<img src="img/sercom2_usart_config.png" width="500">


### Pin settings

Pin settings for USART bus communication:

<img src="img/pin_usart_config.png" width="800">

Pin settings for les:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">

