/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


#define ONLY_SEND_DATA // If you want to see how to read data from USART comment this line.


#ifdef ONLY_SEND_DATA

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    SYSTICK_TimerStart(); // Start system timer for delay function
    
    SERCOM4_USART_ReceiverDisable();   // Disable receiver 
    SERCOM4_USART_TransmitterEnable(); // Enable transmitter
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 19200;             // Baudrate set to 19200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    SERCOM4_USART_Write((uint8_t *)"LocuBoard is ready.\r\n", 21); // Print start message
    
    while ( true )
    {
        
        SERCOM4_USART_Write((uint8_t *)"I love IoT.\r\n", 13); // Print message
        SYSTICK_DelayMs(500);                                  // Delay 500 ms
        
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}

#endif



#ifndef ONLY_SEND_DATA

#define RING_BUFF_SIZE 128 // Maximum bytes for recieve data

char ring_buff[RING_BUFF_SIZE] = {}; // Array for received data
uint8_t ring_buff_rindex       = 0;  // Array index for read
uint8_t ring_buff_windex       = 0;  // Array index for write


/*************** Ring buffer write byte ***************/

/*
 * description:
 *				Function that writes byte to ring buffer
 *
 * params:
 *				[in] byte(char) - Input parameter for one character
 *
 * returns: No return value.
*/
void ring_buffer_write_byte(char byte){
    ring_buff_windex = ring_buff_windex & 127;
    ring_buff[ring_buff_windex] = byte;
    ring_buff_windex++;
}


/****************** Ring buffer read byte ******************/

/*
 * description:
 *				Function that read out one byte from ring buffer
 *
 * params:
 *				No param
 *
 * returns: Returns one character value
*/
char ring_buffer_read_byte(void){
    if(ring_buff_rindex != ring_buff_windex){
        ring_buff_rindex = ring_buff_rindex & 127;
        char _tmp = ring_buff[ring_buff_rindex];
        ring_buff[ring_buff_rindex++] = 0;
        return _tmp;
    }
    else return 0;
}


volatile uint8_t rx_flag = 0;    // Flag for receive data
char rx_char             = '\0'; // Receive char value     

void usart_rx_callback( uintptr_t context ){
    ring_buffer_write_byte(rx_char);
    SERCOM4_USART_Read(&rx_char, 1);
    rx_flag = 1;
}


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    SERCOM4_USART_ReceiverEnable();    // Enable receiver 
    SERCOM4_USART_TransmitterEnable(); // Enable transmitter
    
    
    SERCOM4_USART_ReadCallbackRegister(&usart_rx_callback, (uintptr_t) NULL); // Register RX callback for catching receive data
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 19200;             // Baudrate set to 19200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, CPU_CLOCK_FREQUENCY); // Apply USART configuration
    
    
    SERCOM4_USART_Read(&rx_char, 1); // Begin reading
    
    
    SERCOM4_USART_Write((uint8_t *)"LocuBoard is ready.\r\n", 21); // Print start message
    
    while ( true )
    {
        char _char = ring_buffer_read_byte(); // Read out received data
        
        if(_char) SERCOM4_USART_Write(&_char, 1); // Send read data back
        
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}

#endif

/*******************************************************************************
 End of File
*/

