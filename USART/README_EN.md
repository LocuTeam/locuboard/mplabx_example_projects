EN / [CZ](README.md)


# USART bus

It is basic bus, that uses two communication wires + reference ground. Communication cables are called TX and RX.
- TX = transmit
- RX = receive
- GND = reference ground

<br>

Two devices can be connected like that:

<img src="img/USART_chip_block_diagram.png" width="80%">

For special causes can be used two other wires so-called CTS and RTS, for "safe" bus control:
- RTS = request to send
- CTS = clear to send

<br>

More generally it is possible to connect USART bus between two devices like that:

<img src="img/USART_block_diagram.png" width="80%">


USART bus doesn't use clock for data transmision, therefore for communication it has to be known communication speed so-called **baudrate**, that says how many bits are transmit per time unit.

Typical values of baudrate are:
- 9600
- 19200
- 115200

On the bus can be communication done only with one other device no more, for example with one communication card or with one sensor.

<br>

Transmitted messages is called **packets** and raw data in it we call **data frame**.
<img src="img/USART_packet.png" width="80%">

Each packet contains:
- **Start bit** = begining of the communication - for start is logical one on the bus changed to logical zero
- **Data frame** = our data - count of transmit bits
- **Parity bit** = bit for pasive error check. It can be configured to **odd** or **even** parity. If the bus is configured for odd parity and the count of the ones in data frame is odd number, parity bit will be set to logical 0, otherwise the parity bit will be set to logical 1. The same it will be if the usart will be configuret for even parity.

<br>

USART bus can be used in two modes:
- Asynchronous (UART) - data can be in one moment transmitted and received
- Synchronous - easier for use, but allow only transmit or receive in one moment

<br>

Transmission on teh USART bus can look like that:

- Data wire is in stand by connected to logical 1.
- When the communication starts, the transmitting device set data wire to logical 0 for one bit transmit time.
- Data width is optional from 5 bits to 9 bits, but both sides (transmitter, receiver) must be configured the same.
- Parity bit is fully optional and can be confgured in to two modes
- Stop bit stops the transmission of one packet for bus (optional from 1 to 2 stop bits) setting the data wire to logical 1.


<img src="img/USART_packet_and_graph.png" width="100%">


<br>

# Project examples

There are project for peripheral library a also projects that use sofisticated harmonycore library.

## USART_peripheral_EDBG_example

**Recommended.**
Basic project for peripheral USART library. USART is set to EDBG programmer, that is represented as comport in computer. In user program there is an example how to send data through bus and also how to data properly read and save. For more information about project click [here](USART_peripheral_EDBG_example).


## USART_peripheral_CLICK_example

Basic project for peripheral USART library.  USART is set to CLICK header pins. In user program is used for data reading the polled method (wait 'till there is no other data). For more information about project click [here](USART_peripheral_CLICK_example).


## USART_peripheral_STDIO_EDBG_example

Basic project for peripheral USART library and the standard input output library (stdio.h). USART is set to EDBG programmer, that is represented as comport in computer. In user program you can see periodical data sending each 500 ms. For more information about project click [here](USART_peripheral_STDIO_EDBG_example).


## USART_peripheral_STDIO_CLICK_example

Basic project for peripheral USART library and the standard input output library (stdio.h). USART is set to CLICK header pins. In user program you can see periodical data sending each 500 ms. For more information about project click [here](USART_peripheral_STDIO_CLICK_example).


## USART_harmonycore_EDBG_example

Project for Harmony v3 core USART library usage. USART is set as async and is also set to EDBG programmer, that is represented as comport in computer.  In user program you can see periodical data sending each 500 ms. For more information about project click [here](USART_harmonycore_EDBG_example).


## USART_harmonycore_CLICK_example

Project for Harmony v3 core USART library usage. USART is set as sync and is set to CLICK header pins. In user program you can see periodical data sending each 500 ms. For more information about project click [here](USART_harmonycore_CLICK_example).