/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <stdio.h>

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    SYSTICK_TimerStart(); // Start system timer for delay function
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 19200;             // Baudrate set to 19200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)

    printf("LocuBoard is ready.\r\n"); // Print start message
    
    while ( true )
    {
        
        printf("I love IoT\r\n"); // Print out message
        
        SYSTICK_DelayMs(500); // Delay for 500 miliseconds
        
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

