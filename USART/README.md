CZ / [EN](README_EN.md)

# USART sběrnice

Je základní sběrnice, která používá dva komunikační vodiče + referenční zem. Přenosové vodiče se nazývají TX a RX.
- TX = transmit
- RX = receive
- GND = referenční zem

<br>

Dvě zařízení se pak tedy mezi sebou zapojují takto:
<img src="img/USART_chip_block_diagram.png" width="80%">

Pro speciální případy lze použít i další dva vodiče tzv. CTS a RTS, pro "bezpečnější" ovládání sběrnice:
- RTS = request to send
- CTS = clear to send

<br>

Obecněji je možné USART sběrnici zapojit mezi dvěmi zařízeními takto:
<img src="img/USART_block_diagram.png" width="80%">


Sběrnice nepoužívá žadné hodiny pro určení v jakých intervalech jsou jednotlivé bity odesílány a v jakých intervalech mají být tedy přijímány. Z tohoto důvodu se pro komunikaci musí určit vždy předem komunikační rychlost tzv. **baudrate**, který udává kolik bitů bude přeneseno za jednotku času.

Mezi typické hodnoty baudrate patří například:
- 9600
- 19200
- 115200

Na sběrnici USART lze komunikovat bez rušení pouze s jedním zařízením, například s jednou komunikační kartou nebo s jedním senzorem.

<br>

Přenášené zprávy nazývame **pakety** a surová data v nich obsažená nazýváme **datové rámce**.
<img src="img/USART_packet.png" width="80%">

Každý paket obsahuje:
- **Start bit** = začátek komunikace - pro start se logická jednnička na sběrnici změní na logickou nulu a tím se odstartuje komunikace
- **Data frame** = datový rámec (data) - počet přenášených bitů může být nastaven pro 5 až 9 bitů podle potřeby
- **Parity bit** = bit parity - bit pro pasivní kontrolu případné poruchy cestě komunikace. Může bát nastaveno na **sudou** nebo **lichou** paritu. Pokud je sběrnice nastavena na sudou paritu, tak když počet jedniček v datovém rámci při přenosu bude sudé číslo, parity bit bude nastaven na logickou 0, v opačném případě bude nastaven na logickou 1. To samé platí pro lichou paritu v opačné smyslu.

<br>

Sběrnice USART se může používat ve dvou módech:
- Asynchronní mód (UART) - data mohou být v jedu chvíli jak odesílána, tak i přijímána
- Synchronní mód - jednodušší na používání a umožnňuje nám data buď pouze vysílat anebo pouze přijímat v jedné dané chvíli

<br>

Přenos dat na USART sběrnici vypadá nasledovně:

- Datový vodič je v klidovém stavu připojen na logickou hodnotu 1.
- V případě začátku komunikace přepne vysílací zařízení datový vodič z logické hodnoty 1 na logickou hodnotu 0 po dobu přenosu jednoho bitu na sběrnici.
- Datový objem je volitelný od 5 bitů do 9 bitů, obě strany (přijímač, vysílač) musí být nastaveny stejně.
- Parity bit je volitelný a může být nastaven do dvou módu
- Stop bit ukončuje přenos jednoho packetu po sběrnici (volitelný 1 až 2 stop bity) nastavením vodiče na logickou hodnotu 1.


<img src="img/USART_packet_and_graph.png" width="100%">


<br>

# Příklady projektů

Jsou zde projekty peripheral, které pracují s jednoduchou knihovnou a také projekty se sofistikovanější harmonycore knihovnou.


## USART_peripheral_EDBG_example

**Doporučuje se.**
Jednoduchý projekt využívající peripheral USART knihovnu. USART je nastavený na EDBG programátor, který se reprezentuje v počítači jako comport. V uživatelském programu je vidět jak odesílat data po sběrnici a také jak data správně efektivně přijímat a uchovávat. Více informací k projektu neleznete [zde](USART_peripheral_EDBG_example).

## USART_peripheral_CLICK_example

Projekt využívající peripheral USART knihovnu. USART je nastavený na CLICK header piny. V uživatelském programu je pro čtení dat použita polled metoda (čekání dokud nejsou všechna data přijata). Více informací k projektu naleznete [zde](USART_peripheral_CLICK_example).

## USART_peripheral_STDIO_EDBG_example

Projekt využívající peripheral USART knihovnu a zároveň standardní vstupně výstupní knihovnu STDIO (stdio.h). USART je nastavený na EDBG programátor, který se reprezentuje v počítači jako comport. V uživatelském programu je vidět periodické odesílání dat každých 500 ms. Více informace k projektu naleznete [zde](USART_peripheral_STDIO_EDBG_example).


## USART_peripheral_STDIO_CLICK_example

Projekt využívající peripheral USART knihovnu a zároveň standardní vstupně výstupní knihovnu STDIO (stdio.h). USART je nastavený na CLICK header piny. V uživatelském programu je vidět periodické odesílání dat každých 500 ms. Více informace k projektu naleznete [zde](USART_peripheral_STDIO_CLICK_example).


## USART_harmonycore_EDBG_example

Projekt využívá Harmony v3 core USART knihovnu. USART je nastavený jako asynchronní a je nastavený na EDBG programátor, který se reprezentuje v počítači jako comport. V uživatelském programu je vidět periodické odesílání dat každých 500 ms. Více informace k projektu naleznete [zde](USART_harmonycore_EDBG_example).


## USART_harmonycore_CLICK_example

Projekt využívá Harmony v3 core USART knihovnu. USART je nastavený jako synchronní a je nastavený na CLICK header piny. V uživatelském programu je vidět periodické odesílání dat každých 500 ms. Více informace k projektu naleznete [zde](USART_harmonycore_CLICK_example).