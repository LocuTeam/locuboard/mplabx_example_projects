CZ / [EN](README_EN.md)

# [USART sekce](/USART) -> USART_harmonycore_EDBG_example

Nacházíš se ve složce projektu USART_peripheral_EDBG_example, kde se pracuje s USART sběrnicí a zároveň se standardní vstupně výstupní knihovnou (stdio.h). Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

**Při vkládání harmony core driveru vybereme v první dialogovém okně možnost ANO pro importování harmony core, ale v druhém dialogovém okně vybereme možnost NE pro nenaimportování FREE RTOS**

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">


### Nastavení komponenty sercom4

Zde vybereme možnost USART with Internal Clock, dále Non-blocking mód, USART frame with parity, baudrate nastavíme na 19200 bauds, paritu na No Parity, Character size nastavíme na 8 bitovou velikost dat, Stop bit nastavíme na One Stop Bit.

Nejdůležitější částí je Receive Pinout a Transmit Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, a proto se musíme podívat do datasheetu, který pad odpovídá jakému pinu anebo se musíme podívat do harmony v3 Pin Table.

<img src="img/sercom4_usart_config.png" width="500">


### Nastavení komponenty Harmony Core

Při vložení této komponenty do projektu necháme vše na výchozích hodnotách.

<img src="img/harmony_core_config.png" width="500">


### Nastavení komponenty Harmony USART driver

V nastavení Harmony USART driveru nastavíme Driver mód na Asynchronous.

<img src="img/harmony_usart_driver_config.png" width="500">


### Nastavení komponenty Harmony USART driver instance 0

V nastavení Harmony USART driver instance 0 necháme výchozí hodnoty. Pokud bychom chtěli můžeme nastavit více Clients, kdybychom chtěli "najednou" posílat více dat (zároveň posílat a číst). Dále můžeme zvětšit Queue size, kdybychom posílali nějaká velká data a věděli bychom, že by se výstupních USART registrů nevešli.

<img src="img/harmony_usart_driver_instance_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro komunikaci na USART sběrnici:

<img src="img/pin_usart_config.png" width="800">

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro led diody a tlačítka:

<img src="img/pin_button_config.png" width="800">