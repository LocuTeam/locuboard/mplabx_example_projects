EN / [CZ](README.md)

# [USART section](/USART) -> USART_peripheral_EDBG_example

Your are at USART_peripheral_EDBG_example project, that use USART bus and also standard input output library (stdio.h). Project source file you can find [here](firmware/src/main.c).


## Project settings

**When the Harmony core usart driver is imported in project the promt windows will appear. In the first one choose Yes as an option for import harmony core, but in the second one choose No as an oprion for not import FREE RTOS**

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### Sercom4 component settings

Here we choose USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 19200 bauds, parity set to No Parity, Character size set to 8 bit width, Stop bit set to One Stop Bit.

The most important part are the Receive Pinout and the Transmit Pinout, these two we set to pin on development board where we want to send our data or where we want to read them. Notice there, that we have no pins to choose here, only pads, so we look for the datasheet or we look for the harmony v3 Pin Table, where the pins show their pads.

<img src="img/sercom4_usart_config.png" width="500">


### Harmony Core component settings

Let all settings of this component on default value.

<img src="img/harmony_core_config.png" width="500">


### Harmony USART driver component settings

In Harmony USART driver setting set Driver mode to Asynchronous.

<img src="img/harmony_usart_driver_config.png" width="500">


### Harmony USART driver instance 0 component settings

In Harmony USART driver instance 0  let values as defaults. If you want to set more Clients for sending more data "at the same time" ("at the same time" send and read data). The next you can expand Queue size for sending "big" data that may not fit in USART registers.

<img src="img/harmony_usart_driver_instance_config.png" width="500">


### Pin settings

Pin settings for USART bus communication:

<img src="img/pin_usart_config.png" width="800">

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">