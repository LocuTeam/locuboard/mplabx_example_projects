CZ / [EN](README_EN.md)

# [USART sekce](/USART) -> USART_peripheral_STDIO_CLICK_example

Nacházíš se ve složce projektu USART_peripheral_STDIO_CLICK_example, kde se pracuje s USART sběrnicí a zároveň se standardní vstupně výstupní knihovnou (stdio.h). Zdrojový soubor můžete nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">


### Nastavení komponenty sercom2

Zde vybereme možnost USART with Internal Clock, dále blocking mód, USART frame with parity, baudrate nastavíme na 19200 bauds, paritu na No Parity, Character size nastavíme na 8 bitovou velikost dat, Stop bit nastavíme na One Stop Bit.

Nejdůležitější částí je Receive Pinout a Transmit Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, a proto se musíme podívat do datasheetu, který pad odpovídá jakému pinu anebo se podívat do harmony v3 Pin Table.

<img src="img/sercom2_usart_config.png" width="500">


### Nastavení komponenty stdio

V komponentě STDIO povolíme možnost buffered mod. Data se budou uchovávat ve větším bufferu je zde menší šance, že bychom o nějaká data přišla.

<img src="img/stdio_config.png" width="400">


### Nastavení pinů

Nastavení pinů pro komunikaci na USART sběrnici:

<img src="img/pin_usart_config.png" width="800">

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro tlačítka:

<img src="img/pin_button_config.png" width="800">

