#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


volatile uint8_t adc_callback_flag = 0;
void adc_callback(ADC_STATUS status, uintptr_t context){
    if(status == ADC_INTENSET_RESRDY_Msk) adc_callback_flag = 1;
}

volatile uint8_t eic_button1_flag = 0;
void eic_button1_callback(uintptr_t context){
    eic_button1_flag = 1;
}


int main(void){
    
    SYS_Initialize ( NULL ); // Initialize all modules
    
    
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_callback, (uintptr_t) NULL); // Register EIC button 1 callback
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)

    
    
    ADC_CallbackRegister(adc_callback, (uintptr_t) NULL);           // Register ADC callback
    ADC_WindowModeSet(ADC_WINMODE_DISABLED);                        // Set window mode
    ADC_InterruptsEnable(ADC_INTENSET_RESRDY(true));                // Enable interrupt
    ADC_ChannelSelect(ADC_INPUTCTRL_MUXPOS_AIN3, ADC_NEGINPUT_GND); // Select ADC channel
    ADC_Enable();                                                   // Enable ADC
    
    
    printf("LocuBoard is ready.\r\n");
    
    
    while(true){
        
        SYS_Tasks(); // Maintain state machines of all polled MPLAB Harmony modules.
        
        if(eic_button1_flag){
            eic_button1_flag = 0;
            ADC_ConversionStart(); // If button 1 is pressed then start conversion
        }
        
        if(adc_callback_flag){ // If data is ready
            adc_callback_flag = 0;
            
            uint16_t adc_value = ADC_ConversionResultGet();
            adc_value = (adc_value > 3300) ? 3300 : adc_value;
            
            printf("ADC result: %d mV\r\n", adc_value); // Print out ADC data
        }
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

