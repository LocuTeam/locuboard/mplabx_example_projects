EN / [CZ](README.md)

# Examples projects for MPLAB X IDE for IoT development board LocuBoard.

You're now in repository of projects for IoT development board LocuBoard. You can find here an inspiration for you project and also you can find here better explanation of sensors, modules and basics of project setup.


# LocuBoard pinout

<img src="img/LocuBoard_sheet.png" style="display: block; margin-left: auto; margin-right: auto; width: 100%;">

## Basic projects ([Basic](Basic))

For better explanation and better understanding  are there created examples of projects that show how project can be configured. There is created example project for delay function usage and function for writing digital value to he pin of microcontroller [here](Basic/LED_AND_DELAY_example). There is also created example project for reading digital value from pin usage [here](Basic/LED_AND_BUTTON_example).


## USART periphery ([USART](USART))

Some of sensors use for their communication USART bus, therefore is this bus important. For beginning it can be used for printing out control data for better understanding our program. For easy usage is the USART bus created on hardware side and for usage you only have to control some registers. For more easy usage of USART periphery are created functions, that for us control all registers. Harmony v3 framework has two layers USART libraries the peripheral and the harmony core.


## EIC periphery ([EIC](EIC))

In some cases we cannot readout all button clicks or we cannot readout state signals in user code and therefore there is some kind of hardware that can take care of it. Extern interrupt is used mainly when the system clock is turned off and the extern interrupt is clock by another clock source, the most often the clock source for EIC is low power 32 kHz crystal.


## Analog-digital convertor ([ADC](ADC))

For the most basic senzor data achieving we use so-called analog-digital convertor. This convertor get informations analog world and convert them to digital world.


## Timer ([TC](TC))

For avoiding delay function usage we can use timer for the same effect. The timer simply count down when is the right time to execute an instruction and in the meantime do what ever we want. We can wake up microcontroller from sleep by timer, because clock source is independent on clock source of main program.


## Timer controller ([TCC](TCC))

The timer it is not necessary used for counting down, but we can use it as PWM signal source.


## Real time counter ([RTC](RTC))

Real time counter can be used for periodic instruction call, similarly as timer. Advantage is, that in whole program run we know exact time.


## I2C bus ([I2C](I2C))

For easy sensor communication is the I2C bus very useful. It allow to transmit data with a relatively low power consumption. The basics of usage the I2C bus you can find in [I2C](I2C) bus section. Usage the I2C bus with sensors you can find in [Sensor](Sensor) section.


## SPI bus ([SPI](SPI))

For more complex sensor and card communication there is teh SPI bus. It allow to transmit a lot of data data with a relatively low power consumption. The basics of usage the SPI bus you can find in [SPI](SPI) bus section. Usage of the SPI bus with sensors and modules you can find in [Sensor](Sensor) and [Card](Card) section.


## Sensors ([Sensor](Sensor))

This section is about project examples of sensors, how to config them and how to use them corectly.  
#### Examples of sensors:
  - Analog sensor
    - Firmware for sensor (Attiny45) [here](Sensor/Analog_sensor_NTC_AND_PHOTO_I2C_SLAVE_firmware/)
    - Reading data using ADC [here](Sensor/Analog_sensor_NTC_AND_PHOTO_ADC_example/)
    - Reading data using I2C [here](Sensor/Analog_sensor_NTC_AND_PHOTO_I2C_example/)
  - MCP9844
    - Periodical data reading using I2C [here](Sensor/MCP9844_example/)


## Modules ([Module](Module))

In section Modules are created projects examples for modules for knowledge how to config them and how to control them.  
#### Example of modules:
  - WS2812B programmable LED strip
    - SPI (MOSI) controls WS2812B [here](Module/WS2812B_LED_STRIP_MODULE_SPI_example/)
  - SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example
    - Segment LED display controlled via gpio functions [here](Module/SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example/)