/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


#define MOSI_NUM_BYTES 2 // Number of bytes for Master Output Slave Input buffer
#define MISO_NUM_BYTES 4 // Number of bytes for Master Input Slave Output buffer

uint8_t MOSI_buffer[MOSI_NUM_BYTES]; // Buffer for Master Output Slave Input data (TX)
uint8_t MISO_buffer[MISO_NUM_BYTES]; // Buffer for Master Input Slave Output data (RX)

volatile uint8_t spi_read_flag = 0;

volatile uint8_t spi_callback_flag = 0; // Flag for SPI callback function (volatile - no optimizations for that variable)

// SPI callback function
void spi_callback(uintptr_t context)
{
    spi_callback_flag = 1; // Set SPI callback flag to 1
}

volatile uint8_t button1_flag = 0; // Flag for EIC button 1 extern interrupt
volatile uint8_t button2_flag = 0; // Flag for EIC button 2 extern interrupt

// Callback function for button1 extern interrupt
void eic_button1_callback(uintptr_t context)
{
    button1_flag = 1;
}

// Callback function for button2 extern interrupt
void eic_button2_callback(uintptr_t context)
{
    button2_flag = 1;
}

// Function prototypes
void USER_BUTTON1_Tasks(void);
void USER_BUTTON2_Tasks(void);
void SPI_CALLBACK_MESSAGE_Tasks(void);

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    
    SYS_Initialize ( NULL ); // Initialize all modules
    
    
    SYSTICK_TimerStart(); // Start system timer for delay function
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 9600;              // Baudrate set to 9600 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default system clock frequency)
    
    
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_callback, (uintptr_t) NULL); // Register extern interrupt callback function for button 1
    EIC_CallbackRegister(EIC_PIN_7, eic_button2_callback, (uintptr_t) NULL); // Register extern interrupt callback function for button 2
    
    
    SERCOM1_SPI_CallbackRegister(spi_callback, (uintptr_t) NULL); // Register callback function for SPI bus
    
    SPI_TRANSFER_SETUP spi_setup; // Variable for storing setup information for spi
    
    spi_setup.clockFrequency = 1000000;                      // SPI clock frequency set to 1MHz
    spi_setup.clockPhase     = SPI_CLOCK_PHASE_LEADING_EDGE; // SPI clock phase set to Data is sampled on clock leading edge and changed on trailing edge
    spi_setup.clockPolarity  = SPI_CLOCK_POLARITY_IDLE_LOW;  // SPI clock polarity set to Inactive state value of clock is logic level zero
    spi_setup.dataBits       = SPI_DATA_BITS_8;              // SPI data bits set to 8 bit width
    
    SERCOM1_SPI_TransferSetup(&spi_setup, 0); // Apply SPI configuration (0 - default system clock frequency)

    
    printf("LocuBoard is ready.\r\n");
    
    
    while ( true )
    {
        
        SYS_Tasks ( ); // Maintain state machines of all polled MPLAB Harmony modules.
        
        USER_BUTTON1_Tasks(); // User button 1 tasks
        
        USER_BUTTON2_Tasks(); // User button 2 tasks
        
        SPI_CALLBACK_MESSAGE_Tasks(); // SPI callback message tasks
        
    }
    
    // Execution should not come here during normal operation

    return ( EXIT_FAILURE );
}

// Function sends data through SPI bus if the bus is not busy
void USER_BUTTON1_Tasks(void){
    if(button1_flag){
        
        if(!SERCOM1_SPI_IsBusy()){
            printf("USER_BUTTON1: Sending data");
            for(int i = 0; i < sizeof(MOSI_buffer); i++) printf(" 0x%02X", MOSI_buffer[i]);
            printf(".\r\n");
            SERCOM1_SPI_Write(&MOSI_buffer, MOSI_NUM_BYTES);
        }
        else{
            printf("USER_BUTTON1: Unable to send data.\r\n");
        }
        
        button1_flag = 0; // Clear button flag
    }
}

// Function sends data through SPI bus if the bus is not busy and start waiting for received data
void USER_BUTTON2_Tasks(void){
    if(button2_flag){
        
        if(!SERCOM1_SPI_IsBusy()){
            printf("USER_BUTTON2: Sending data");
            for(int i = 0; i < sizeof(MOSI_buffer); i++) printf(" 0x%02X", MOSI_buffer[i]);
            printf(" and waiting for response.\r\n");
            SERCOM1_SPI_WriteRead(&MOSI_buffer, MOSI_NUM_BYTES, &MISO_buffer, MISO_NUM_BYTES);
            
            spi_read_flag = 1;
            
        }
        else{
            printf("USER_BUTTON2: Unable to send data.\r\n");
        }
        
        button2_flag = 0; // Clear button flag
    }
}

// Function print received data to stdout
void SPI_CALLBACK_MESSAGE_Tasks(void){
    if(spi_callback_flag){
        
        if(spi_read_flag){
            printf("SPI_CALLBACK_MESSAGE: Data received is:");
            for(int i = 0; i < sizeof(MISO_buffer); i++) printf(" 0x%02X", MISO_buffer[i]);
            printf(".\r\n");
            spi_read_flag = 0;
        } 
        
        spi_callback_flag = 0; // Clear spi flag
    }
}


/*******************************************************************************
 End of File
*/

