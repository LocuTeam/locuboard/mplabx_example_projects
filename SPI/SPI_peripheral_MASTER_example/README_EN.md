EN / [CZ](/SPI/SPI_peripheral_MASTER_example)

# [SPI bus](/SPI) -> SPI_peripheral_MASTER_example

Your are at SPI_peripheral_MASTER_example project, that use SPI bus. The user program sends data through SPI bus if the button 1 is pressed, but if the button 2 is pressed it sends data and start waiting for received data. If SPI transfer is completed, received data are printed out to terminal. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### Sercom4 as EDBG USART component settings 

Here we choose USART with Internal Clock, next blocking mode, USART frame with parity, baudrate set to 9600 bauds, parity set to No Parity, Character size set to 8 bit width, Stop bit set to One Stop Bit.

The most important part are the Receive Pinout and the Transmit Pinout, these two we set to pin on development board where we want to send our data or where we want to read them. Notice there, that we have no pins to choose here, only pads, so we look for the datasheet or we look for the harmony v3 Pin Table, where the pins show their pads.

<img src="img/sercom4_edbg_usart_config.png" width="500">


### Stdio component settings

Choose option buffered mode.

<img src="img/stdio_config.png" width="500">


### Sercom1 as SPI component settings

Choose SPI Master option, next click on Enable Interrupts, set SPI Data Out Pad configration due to datasheet and also SPI Data In Pad set due to datasheet, next set Data order to MSB is transferred first, set Speed in Hz to 1MHz, SPI Data size set to 8bits, next set clock phase to Data sampled on a leading SCK edge, set Clock Polarity to SCK is low when idle, enable SPI Master Hardware Slave Select and at te end enable SPI Receiver.

<img src="img/sercom1_click_spi_config.png" width="500">


### EIC component settings

First choose as clock input Clocked by ULP32K. The next choose chanel 6, 7 and 15. These channels correspond to button pins. In every chanel choose option Enable Interrupt, next choose Edge detection is clock asynchronously operated, next choose Falling edge detection and at the end choose Enable filter.

<img src="img/eic_config.png" width="500">


### Pin settings

Pin settings for USART bus communication:

<img src="img/pin_edbg_usart_config.png" width="800">

Pin settings for SPI bus communication:

<img src="img/pin_click_spi_config.png" width="800">

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">
