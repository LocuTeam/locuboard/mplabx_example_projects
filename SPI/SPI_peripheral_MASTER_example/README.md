CZ / [EN](README_EN.md)

# [SPI sběrnice](/SPI) -> SPI_peripheral_MASTER_example

Nacházíš se ve složce projektu SPI_peripheral_MASTER_example, kde se pracuje s SPI sběrnicí. Data se odesílají pokud je stlačeno tlačítko číslo 1. Pokud je stlačeno tlačítko číslo 2, data jsou také poslána a je zaájeno čekání na přijatá data. Když doběhne transakce, přijatá data jsou zobrazena na terminál. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c).


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Zde povolíme SysTick a zapneme interrupt pro používání delay funkce v projektu.

<img src="img/system_config.png" width="500">


### Nastavení komponenty sercom4 jako EDBG USART

Zde vybereme možnost USART with Internal Clock, dále blocking mód, USART frame with parity, baudrate nastavíme na 9600 bauds, paritu na No Parity, Character size nastavíme na 8 bitovou velikost dat, Stop bit nastavíme na One Stop Bit.

Nejdůležitější částí je Receive Pinout a Transmit Pinout, ty nastavíme podle toho, na jakém pinu vývojové desky chceme přijímat data a na kterém pinu chceme data vysílat. Všimněte si, že zde nemáme na výběr přímo piny ale pady, a proto se musíme podívat buď do datasheetu, který pad odpovídá jakému pinu a nebo se podívat do harmony v3 Pin Table.

<img src="img/sercom4_edbg_usart_config.png" width="500">


### Nastavení komponenty stdio

Zde vybereme možnost buffered mód.

<img src="img/stdio_config.png" width="500">


### Nastavení komponenty sercom1 jako SPI

Zde vybereme možnost SPI Master, dále povolíme Enable Interrupts, nastavíme SPI Data Out Pad configration podle datasheetu a také SPI Data In Pad nastavíme podle datasheetu, dále nastavíme Data order na MSB is transferred first, nastavíme Speed in Hz na 1MHz, SPI Data size nasavíme na 8bits, dále clock phase nastavíme na Data sampled on a leading SCK edge, nastavíme Clock Polarity na SCK is low when idle, povolíme SPI Master Hardware Slave Select a povolíme SPI Receiver.

<img src="img/sercom1_click_spi_config.png" width="500">


### Nastavení komponenty EIC

Zaprvé vybereme zdroj hodin Clocked by ULP32K. Vybereme kanál 6, 7 a 15, které odpovídají pinům, kde jsou připojena tlačítka. V každém kanálu vybereme možnost Enable Interrupt, dále vybereme Edge detection is clock asynchronously operated, dále vybereme Falling edge detection a vybereme Enable filter.

<img src="img/eic_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro komunikaci na USART sběrnici:

<img src="img/pin_edbg_usart_config.png" width="800">

Nastavení pinů pro komunikaci na SPI sběrnici:

<img src="img/pin_click_spi_config.png" width="800">

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro tlačítka:

<img src="img/pin_button_config.png" width="800">