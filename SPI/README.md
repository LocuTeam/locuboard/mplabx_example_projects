CZ / [EN](README_EN.md)

# SPI sběrnice
Je jedna z nejrychlejších embedded sběrnic, a proto se používá pro přenos většího objemu dat. Ke své komunikaci používá dva datové vodiče, hodinový signálový vodič, vodič pro výběr zařízení + referenční zem. Datové vodiče se značí jako MOSI a MISO. MOSI je datový vodič, který slouží pro odesílání dat směrem k přijímači. MISO je datový vodič, který slouží pro přijímání dat z přijímače.

- MOSI - Master Output Slave Input
- MISO - Master Input Slave Output
- SCLK - Signal Clock
- CS/SS - Chip select / Slave select

<br>

SPI sběrnice může být nakonfigurována do dvou komunikačních módů:

- **master** - zařízení ovládající ostatní zařízení na sběrnici
- **slave**  - zařízení, které poslouchá na sběrnici a v případě potřeby přijímá a odesílá data.

<br>
Základní zapojení sběrnice SPI vypadá takto:
<img src="img/SPI_chip_block_diagram.png" width="70%">

<br>

SPI sběrnice umožňuje nastavit dva rozsahy odesílaných dat:

- 8bit - data mají velikost 8 bitů a nabývají hodnot od 0 do 255
- 9bit - data mají velikost 9 bitů a nabývají hodnot od 0 do 512

<br>

Další důležitou konfigurcí je tzv. Pořadí dat při odesílání:

- MSB - Most significant bit is transfered first - první se odesílá bit s nejvyšší váhou
- LSB - Low significant bit is transfered first - první se odesílá bit s nejnižší váhou

<br>

Pro vzorkování je důležitou konfigurací tzv. Fáze hodin:

- The data is sampled on a leading SCK edge and changed on atrailing SCK edge - data jsou vzorkována na náběžné hraně hodinového signálu
- The data is sampled on a trailing SCK edge and changed on a leading SCK edge - data jsou vzorkována na sestupné hraně hodinového signálu

<br>

Pro vzorkování je dále také důležitou konfigurací tzv. Polarita hodin:

- SCK is low when idle - při nečinnosti je hodinový signál nastaven na log0
- SCK is high when idle - při nečinnosti je hodinový signál nastaven na log1

<br>

Pro správnou komunikaci je důležité aby měla všechna zařízení na sběrnici nastavena shodně pořadí dat při odesílání, bitovou velikost přenášených dat, fázi hodin a polaritu hodin.

<br>

Pro komunikaci na SPI sběrnici zaprvé master zvolí slave zařízení, se kterým bude komunikovat, za pomoci SS vodiče (SS nastaví na log0). Dále master zahají generování hodinového signálu a začne probíhat přenos dat. Data mohou být odesílána z master zařízení a zároveň ve stejném čase mohou být jiná data přijímáná ze slave zařízení, přičemž jednotlivé bity jsou synchronizovány hranou hodi nového signálu. Pro konec komunikace master nastaví SS vodič na log1 a žádna další data nejsou přijmuta ani odeslána.

<img src="img/SPI_communication.png" width="100%">

<br>

# Příklady projektů

Jsou zde projekty peripheral, které pracují s jednoduchou knihovnou a také projekty se sofistikovanější harmonycore knihovnou.

## SPI_peripheral_MASTER_example

**Doporučuje se.**
Jednoduchý projekt využívající peripheral SPI knihovnu. Uživatelský program odesílá data při stisknutí tlačítka 1 a při stisknutí tlačítka 2 odešle data a čeká na příchozí data. Více informací najdete [zde](SPI_peripheral_MASTER_example).