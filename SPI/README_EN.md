EN / [CZ](README.md)

# SPI bus
It is one of the fastest embedded buses, therefore it is used for transmission of bigger amount of data. For communication the SPI bus use two data wires, clock signal wire, chip select wire + reference ground. Data wires are called MOSI and MISO. MOSI is data wire used for sending data from trasnmitter to receiver. MISO is data wire used for data receive from receiver to transmitter.

- MOSI - Master Output Slave Input
- MISO - Master Input Slave Output
- SCLK - Signal Clock
- CS/SS - Chip select / Slave select

<br>

SPI bus can be configured to two communication modes:

- **master** - device controlling other devices on the bus
- **slave**  - device that is controlled by master device

<br>
Basic connection of SPI bus looks like that:
<br>
<img src="img/SPI_chip_block_diagram.png" width="70%">

<br>

SPI bus allow to set two ranges of data send amount:

- 8bit - data has 8 bit width for value from 0 to 255
- 9bit - data has 9 bit width for value from 0 to 512

<br>

The next important configuration is so-called Data order for sending:

- MSB - Most significant bit is transfered first
- LSB - Low significant bit is transfered first

<br>

For sampling is important configuration of so-called Clock phase:

- The data is sampled on a leading SCK edge and changed on atrailing SCK edge
- The data is sampled on a trailing SCK edge and changed on a leading SCK edge

<br>

For sampling is important also configuration of so-called Clock polarity:

- SCK is low when idle
- SCK is high when idle

<br>

For correct function of SPI bus must be set equally Data order for sending, Bit width, Clock phase and Clock polarity.

<br>

For communication on the SPI bus in the first place master chouse slave device with whitch want to communication using SS wire (SS sets to log0). The next master starts clock signal generation and the data transmission begin. Data can be send from master and in one time can be other data received from slave. Each bit is synchronized by edge of clock signal. For the end of the transmission master sets SS wire to log1 and no other data will  be received or send.

<img src="img/SPI_communication.png" width="100%">

<br>

# Project examples

There are peripheral projects, that work with basic library and also projects with more complex harmonycore library.

## SPI_peripheral_MASTER_example

**Recommended.**
Basic project for peripheral SPI library. The user code sends data through SPI bus if the button 1 is pushed, but if the button 2 is pressed it sends data and start waiting for received data. For more information about project click [here](SPI_peripheral_SEARCH_BUS_example).