/*******************************************************************************
  TRNG Peripheral Library

  Company:
    Microchip Technology Inc.

  File Name:
    plib_trng.c

  Summary:
    TRNG Source File

  Description:
    None

*******************************************************************************/

/*******************************************************************************
* Copyright (C) 2018 Microchip Technology Inc. and its subsidiaries.
*
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your
* responsibility to comply with third party license terms applicable to your
* use of third party software (including open source software) that may
* accompany Microchip software.
*
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
* EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
* WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
* PARTICULAR PURPOSE.
*
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
* FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
* ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
* THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*******************************************************************************/


#include "device.h"
#include "plib_trng.h"
#include "interrupts.h"

TRNG_OBJECT trng;

void TRNG_Initialize( void )
{}
	
void TRNG_RandomNumberGenerate( void )
{
	TRNG_REGS->TRNG_CTRLA |= TRNG_CTRLA_ENABLE_Msk;
	TRNG_REGS->TRNG_INTENSET = TRNG_INTENSET_DATARDY_Msk;
}

void TRNG_CallbackRegister( TRNG_CALLBACK callback, uintptr_t context )
{
	trng.callback = callback;
	trng.context = context;
}


void TRNG_InterruptHandler( void )
{
   TRNG_REGS->TRNG_CTRLA &= ~(TRNG_CTRLA_ENABLE_Msk);
   TRNG_REGS->TRNG_INTENCLR = TRNG_INTENCLR_DATARDY_Msk;
   trng.data = TRNG_REGS->TRNG_DATA;
   if(trng.callback != NULL)
   {
       trng.callback(trng.data, trng.context);
   }
}

