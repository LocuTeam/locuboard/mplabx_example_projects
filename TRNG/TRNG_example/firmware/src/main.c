/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


volatile uint8_t eic_button1_flag = 0;

void eic_button1_callback(uintptr_t context){
    eic_button1_flag = 1;
}


volatile uint8_t trng_flag = 0;

void trng_callback(uint32_t random, uintptr_t context){
    uint32_t *pointer = (uint32_t *) context;
    *pointer = random;
    
    trng_flag = 1;
}


int main(void){
    
    SYS_Initialize(NULL); // Initialize all modules
    
    
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_callback, (uintptr_t) NULL);
    
    
    USART_SERIAL_SETUP usart_setup; // Variable for storing setup information for usart
    
    usart_setup.baudRate  = 115200;            // Baudrate set to 115200 bauds
    usart_setup.dataWidth = USART_DATA_8_BIT;  // DataWidth set to 8 bit
    usart_setup.parity    = USART_PARITY_NONE; // Parity set to NONE
    usart_setup.stopBits  = USART_STOP_1_BIT;  // Stop bit set to 1
    
    SERCOM4_USART_SerialSetup(&usart_setup, 0); // Apply USART configuration (0 - default clock frequency)
    
    
    uint32_t number = 0;
    
    TRNG_CallbackRegister(trng_callback, (uintptr_t) &number);
    
    
    printf("LocuBoard is ready.\r\n");
    
    
    while(true){
        
        SYS_Tasks ( ); // Maintain state machines of all polled MPLAB Harmony modules.
        
        if(eic_button1_flag){
            eic_button1_flag = 0;
            
            TRNG_RandomNumberGenerate();
        }
        
        if(trng_flag){
            trng_flag = 0;
            
            printf("Random generated number is: %lu\r\n", number & 0xff);
        }
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/
