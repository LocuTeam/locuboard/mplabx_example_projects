CZ / [EN](README_EN.md)

# Časovač příklady projektů

Zde se nacházejí projekty používající externí přerušení.


## TC_TIMER_AND_LED_example

Jednoduchý projekt, který využívá timeru pro zpoždění příkazů. Na základě přerušení timeru jsou ovládány LED diody. Více informací o projektu naleznete [zde](TC_TIMER_AND_LED_example).