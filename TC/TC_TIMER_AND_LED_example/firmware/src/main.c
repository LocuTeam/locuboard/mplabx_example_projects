/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


typedef enum{
    LED_STATE_1 = 0,
    LED_STATE_2 = 1,
    LED_STATE_3 = 2,
    LED_STATE_4 = 3
}LED_STATE_e;

LED_STATE_e led_State = LED_STATE_1;


volatile uint8_t tc0_flag = 0;

void tc0_callback(TC_TIMER_STATUS status, uintptr_t context){
    tc0_flag = 1;
}

void LED_STATE_Tasks(void);

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    USER_LED1_Clear();
    USER_LED2_Clear();
    USER_LED3_Clear();
    USER_LED4_Clear();
    
    TC0_TimerCallbackRegister(tc0_callback, (uintptr_t) NULL);
    
    TC0_TimerStart();

    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
        
        LED_STATE_Tasks();
        
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


void LED_STATE_Tasks(void){
    switch(led_State){
        case LED_STATE_1:
            if(tc0_flag == 1){
                USER_LED1_Set();
                USER_LED4_Clear();
                led_State = LED_STATE_2;
                tc0_flag = 0;
            }
        break;
           
        case LED_STATE_2:
            if(tc0_flag == 1){
                USER_LED2_Set();
                USER_LED1_Clear();
                led_State = LED_STATE_3;
                tc0_flag = 0;
            }
        break;
            
        case LED_STATE_3:
            if(tc0_flag == 1){
                USER_LED3_Set();
                USER_LED2_Clear();
                led_State = LED_STATE_4;
                tc0_flag = 0;
            }
        break;
            
        case LED_STATE_4:
            if(tc0_flag == 1){
                USER_LED4_Set();
                USER_LED3_Clear();
                led_State = LED_STATE_1;
                tc0_flag = 0;
            }
        break;
            
        default:
        break;
    }
}


/*******************************************************************************
 End of File
*/

