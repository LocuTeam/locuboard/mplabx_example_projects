EN / [CZ](README.md)

# [Timer interrupt projects](/EIC) -> EIC_BUTTON_AND_LED_example

Your are at EIC_BUTTON_AND_LED_example project. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### TC component settings

First choose 16-bit mode, next choose GCLK_TC/1024, next choose timer period unit to milliseconds, time set to 500 milliseconds and the next allow timer period interrupt.

<img src="img/tc_config.png" width="500">


### Pin settings

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">