CZ / [EN](README_EN.md)

# [Časovač projekty](/TC) -> TC_TIMER_AND_LED_example

Nacházíš se ve složce projektu TC_TIMER_AND_LED_example. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Obrázek níže popisuje jak je nastavená komponenta system.

<img src="img/system_config.png" width="500">


### Nastavení komponenty TC

Zaprvé vybereme 16-bit mód, dále vybereme GCLK_TC/1024, dále nastavíme timer period unit na milliseconds, time nastavíme na 500 millisecond a povolíme timer period interrupt.

<img src="img/tc_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro tlačítka:

<img src="img/pin_button_config.png" width="800">