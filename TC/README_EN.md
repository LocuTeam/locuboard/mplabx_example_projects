EN / [CZ](README.md)

# Timer project examples

Here you can find project examples that use extern interrupt.


## TC_TIMER_AND_LED_example

Easy project, that uses timer for instruction delay. Based on timer interrupt are LEDs controlled. More informations about project you can find [here](TC_TIMER_AND_LED_example).