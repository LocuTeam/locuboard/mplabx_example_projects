EN / [CZ](README.md)

# [Extern interrupt projects](/EIC) -> EIC_BUTTON_AND_LED_example

Your are at EIC_BUTTON_AND_LED_example project. Project source file you can find [here](firmware/src/main.c).


## Project settings

Picture below describes layout of each component needed in project.

<img src="img/project_config.png" width="500">


### System component settings

Here we allow SysTick timer and turn on interrupt for delay function usage.

<img src="img/system_config.png" width="500">


### Nastavení komponenty EIC

First choose as clock input Clocked by ULP32K. The next choose chanel 6, 7 and 15. These channels correspond to button pins. In every chanel choose option Enable Interrupt, next choose Edge detection is clock asynchronously operated, next choose Falling edge detection and at the end choose Enable filter.

<img src="img/eic_config.png" width="500">


### Pin settings

Pin settings for leds:

<img src="img/pin_led_config.png" width="800">

Pin settings for buttons:

<img src="img/pin_button_config.png" width="800">