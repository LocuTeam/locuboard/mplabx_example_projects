CZ / [EN](README_EN.md)

# [Externí interrupt projekty](/EIC) -> EIC_BUTTON_AND_LED_example

Nacházíš se ve složce projektu EIC_BUTTON_AND_LED_example. Zdrojový soubor můžeš nalézt [zde](firmware/src/main.c)


## Nastavení projektu

Obrázek níže popisuje rozvržení jednotlivých komponent potřebných pro projekt.

<img src="img/project_config.png" width="500">


### Nastavení komponenty system

Obrázek níže popisuje jak je nastavená komponenta system.

<img src="img/system_config.png" width="500">


### Nastavení komponenty EIC

Zaprvé vybereme zdroj hodin Clocked by ULP32K. Vybereme kanál 6, 7 a 15, které odpovídají pinům, kde jsou připojena tlačítka. V každém kanálu vybereme možnost Enable Interrupt, dále vybereme Edge detection is clock asynchronously operated, dále vybereme Falling edge detection a vybereme Enable filter.

<img src="img/eic_config.png" width="500">


### Nastavení pinů

Nastavení pinů pro led diody:

<img src="img/pin_led_config.png" width="800">

Nastavení pinů pro tlačítka:

<img src="img/pin_button_config.png" width="800">