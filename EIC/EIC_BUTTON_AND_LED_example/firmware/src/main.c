/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

// Callback function for button 1 extern interrupt
void eic_button1_callback(uintptr_t context){
    USER_LED1_Toggle(); // Toggle LED1 when the interrupt is called
}

// Callback function for button 2 extern interrupt
void eic_button2_callback(uintptr_t context){
    USER_LED2_Toggle(); // Toggle LED2 when the interrupt is called
}

// Callback function for button 3 extern interrupt
void eic_button3_callback(uintptr_t context){
    USER_LED3_Toggle(); // Toggle LED3 when the interrupt is called
}

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    EIC_CallbackRegister(EIC_PIN_6, eic_button1_callback, (uintptr_t) NULL);  // Register extern interrupt callback function for button 1
    EIC_CallbackRegister(EIC_PIN_7, eic_button2_callback, (uintptr_t) NULL);  // Register extern interrupt callback function for button 2
    EIC_CallbackRegister(EIC_PIN_15, eic_button3_callback, (uintptr_t) NULL); // Register extern interrupt callback function for button 3
    
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

