EN / [CZ](README.md)

# Extern interrupt project examples

Here you can find project examples that use extern interrupt.


## EIC_BUTTON_AND_LED_example

Easy project, that uses extern interrupt for buttons. Based on extern interrupt are LEDs controlled. More informations about project you can find [here](EIC_BUTTON_AND_LED_example).