CZ / [EN](README_EN.md)

# Externí přerušení příklady projektů

Zde se nacházejí projekty používající externí přerušení.


## EIC_BUTTON_AND_LED_example

Jednoduchý projekt, který využívá externí přerušení pro tlačítka. Na základě externího přerušení jsou ovládány LED diody. Více informací o projektu naleznete [zde](EIC_BUTTON_AND_LED_example).