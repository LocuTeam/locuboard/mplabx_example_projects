CZ / [EN](README_EN.md)

# Příklady projektů v programu MPLAB X IDE pro vývojovou desku LocuBoard.

Nacházíš se v repozitáři příkladů projektů pro IoT vývojovou desku LocuBoard. Můžeš zde najít inspiraci pro svůj projekt, dále můžeš nalézt lepší vysvětlení a pochopení jednotlivých senzorů, modulů a základů používání frameworku.


# Pinout desky LocuBoard

<img src="img/LocuBoard_sheet.png" style="display: block; margin-left: auto; margin-right: auto; width: 100%;">


## Základní projekty ([Basic](Basic))

Pro lepší vysvětlení a pochopení základních funkcí jsou zde vytvořeny příklady projektů, které ukazují, jak lze projekty nastavit, abychom mohli zapisovat logickou jedničku nebo nulu na určitý mikrokontroleru anebo jak z jednotlivých pinů číst digitální hodnotu. Je zde vytvořený příklad pro používání delay funkce a funkce pro zapisování logických hodnot na piny [zde](Basic/LED_AND_DELAY_example). Pro pochopení nastavení projektu pro čtení logické hodnoty z pinu je zde vytvořený příklad projektu [zde](Basic/LED_AND_BUTTON_example).


## USART sběrnice ([USART](USART))

Některé senzory používají pro svoji komunikaci USART sběrnici a proto je tato sběrnice důležitá. Pro začátek se nám může hodit pro vypisování kontrolních dat pro lepší pochopení práce našeho programu. Proto, aby byla sběrnice jednoduchá pro používání je tato sběrnice vytvořena již na straně hardwaru a stačí tety jen ovládat určité registry pro její obsluhu. Pro to, abychom nemuseli do jednotlivých registrů složitě přistupovat, jsou vytvořeny funkce, které za nás registry obsluhují. Harmony v3 framework má dvě vrstvy USART knihoven, peripheral a harmony core.


## Externí přerušení ([EIC](EIC))

V některých případech nemůžeme obstarávat kliknutí tlačítek anebo nějaké stavové signály přímo v uživatelském programu a budeme potřebovat nějaký hardware, který by dokázal obstarat tyto základní periferie za nás. Externí přerušení nalezne hlavní využití při uspání hlavního programu, kde externí interrupt je krokován podle jiných hodin nejčastěji low power 32 kHz krystalem.


## Analogově-digitální převodník ([ADC](ADC))

Pro nejzákladnější získávání hodnot ze senzorů používáme tzv. analogově-digitální převodník. Tento převodník nám umožňuje získávat informace z analogového světa a převádí je nějakým způsobem do světa digitálního, se kterým již umíme pracovat.


## Časovač ([TC](TC))

Pro to, abychom nemuseli program zpožďovat funkcí delay, můžeme využít časovače, který nám jednoduše spočte kdy nadešel čas na spuštění nějakého příkazu a mezi tím můžeme provádět cokoliv jiného. Můžeme za pomoci časovače také probouzet mikrokontroler ze spánku, protože hodinový zdroj časovače je nezávislý na hodinovém zdroji hlavního programu.


## Časovač s ovladačem ([TCC](TCC))

Časovač se nemusí používat pouze na odpočet, můžeme ho použít například také jako zdroj PWM signálu.


## Hodiny reálného času ([RTC](RTC))

Hodiny reálného času se mohou požít pro periodické volání příkazů, podobně jako tomu bylo u časovače. Výhoda je v tom, že v programu víme přesný čas v každý moment běhu programu.


## I2C sběrnice ([I2C](I2C))

Pro jednoduchou komunikaci se senzory je I2C sběrnice velice užitečná. Umožňuje s relativně malou spotřebou přenášet dostatečný objem dat z nejrůznějších senzorů. Základy používání I2C sběrnice jsou v sekci [I2C](I2C). Použití I2C sběrnice se senzory je možné vidět v sekci [Sensor](Sensor).


## SPI sběrnice ([SPI](SPI))

Pro senzory nebo mokunikační karty, kde je potřeba přenášet větší objemy dat je SPI sběrnice velice užitečná. Základy používání SPI sběrnice najdeš v sekci [SPI](SPI) sběrnice. Použití of the SPI sběrnice se senzory a moduly můžeš najít v sekci [Sensor](Sensor) nebo v sekci [Card](Card).


## Senzory ([Sensor](Sensor))

Tato sekce se zabívá příklady projektů jednotlivých senzorů, jak je nastavit a správně používat.  
#### Příklady senzorů:
  - Analogový senzor
    - Firmware pro senzor (Attiny45) [zde](Sensor/Analog_sensor_NTC_AND_PHOTO_I2C_SLAVE_firmware/)
    - Čtení dat pomocí ADC [zde](Sensor/Analog_sensor_NTC_AND_PHOTO_ADC_example/)
    - Čtení dat pomocí I2C [zde](Sensor/Analog_sensor_NTC_AND_PHOTO_I2C_example/)
  - MCP9844
    - Periodické čtení dat pomocí I2C [zde](Sensor/MCP9844_example/)


## Moduly ([Module](Module))

V sekci Moduly jsou připraveny příklady projektů k jednotlivým modulům, jak je správně nastavit a jak jse správně ovládat.  
#### Příklady modulů:
  - WS2812B programovatelný LED pásek
    - Ovládádní WS2812B pomocí SPI (MOSI) [zde](Module/WS2812B_LED_STRIP_MODULE_SPI_example/)
  - SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example
    - Segmentový LED display ovládaný gpio funkcí [zde](Module/SEGMENT_LED_DISPLAY_MODULE_CLICK_PERIPHERAL_example/)